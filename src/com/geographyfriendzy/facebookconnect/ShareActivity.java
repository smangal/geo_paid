package com.geographyfriendzy.facebookconnect;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.geographyfriendzypaid.R;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

public class ShareActivity extends AdBaseActivity 
{
	private final String PENDING_ACTION_BUNDLE_KEY 	= "com.spellfriendzy:PendingAction";
	/**
	 * Register your here app https://dev.twitter.com/apps/new and get your
	 * consumer key and secret
	 * */
	static String TWITTER_CONSUMER_KEY 			= "JOucKPyyImymVh3nlFwFQ";
	static String TWITTER_CONSUMER_SECRET 		= "GZEghJniw4ZJLPtgkNfOweATpOWkKikaqncejsH5YD4";

	// Preference Constants
	static String PREFERENCE_NAME 				= "twitter_oauth";
	static final String PREF_KEY_OAUTH_TOKEN 	= "oauth_token";
	static final String PREF_KEY_OAUTH_SECRET 	= "oauth_token_secret";
	static final String PREF_KEY_TWITTER_LOGIN 	= "isTwitterLogedIn";

	static final String TWITTER_CALLBACK_URL 	= "oauth://t4jsample";

	// Twitter oauth urls
	static final String URL_TWITTER_AUTH 			= "https://api.twitter.com/oauth/authorize";
	static final String URL_TWITTER_OAUTH_VERIFIER 	= "oauth_verifier";
	static final String URL_TWITTER_OAUTH_TOKEN 	= "oauth_token";

	ProgressDialog pDialog;
	private static Twitter twitter;
	private static RequestToken requestToken;

	// Shared Preferences
	private static SharedPreferences mSharedPreferences;

	//DialogGenerator generator;
	//Translation translate;	

	private Button postStatusUpdateButton		= null;
	private ImageView imgScreen					= null;
	private TextView txtMsg						= null;
	private TextView txtTitleScreen				= null;

	private static String message;
	private static Bitmap bitmap;
	private static int    id;
	private static boolean flag;
	static ByteArrayInputStream bs ;

	private String alertMessage = null;

	private PendingAction pendingAction = PendingAction.NONE;

	private ProgressDialog pd = null;

	private enum PendingAction 
	{
		NONE,
		POST_PHOTO,
		POST_STATUS_UPDATE
	}

	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};



	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share);		

		flag = getIntent().getBooleanExtra("flag", true);
		if(!flag)
		{			
			message 	= getIntent().getStringExtra("message");			
			id			= getIntent().getIntExtra("id", 0);
			
			bitmap 		= Top100Activity.b;		
			
			if(bitmap != null)
			{
				ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
				bitmap.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos); 
				byte[] bitmapdata = bos.toByteArray();
				bs = new ByteArrayInputStream(bitmapdata);
			}


		}
		
		if(!CommonUtils.isInternetConnectionAvailable(this))
		{
			CommonUtils.showInternetDialog(this);			
		}
		
		/*SharedPreferences sf = this.getSharedPreferences("share", Context.MODE_PRIVATE);
		sf*/
		getAndSetWidget();

		if(id == R.id.btnFbSahre)
		{
			if (Session.getActiveSession() == null || Session.getActiveSession().isClosed()) 
			{			
				Session.openActiveSession(this, true, new Session.StatusCallback() {

					@Override
					public void call(Session session, SessionState state, Exception exception)
					{					

					}
				});
			}

			uiHelper = new UiLifecycleHelper(this, callback);
			uiHelper.onCreate(savedInstanceState);

			if (savedInstanceState != null) {
				String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
				pendingAction = PendingAction.valueOf(name);
			}      
		}//BtnFBPostId
		else
		{
			mSharedPreferences = getSharedPreferences("MyPref", 0);
			if (!CommonUtils.isInternetConnectionAvailable(this))
			{					
				CommonUtils.showInternetDialog(this);
			}
			else
			{
				new LoginTwitter().execute(null,null,null);
			}
		}//END TwitterPost


	}//END onCreate Method



	private void checkConnection() 
	{
		/** This if conditions is tested once is
		 * redirected from twitter page. Parse the uri to get oAuth
		 * Verifier
		 * */
		if (!isTwitterLoggedInAlready()) {
			Uri uri = getIntent().getData();
			if (uri != null && uri.toString().startsWith(TWITTER_CALLBACK_URL))
			{
				// oAuth verifier
				String verifier = uri
						.getQueryParameter(URL_TWITTER_OAUTH_VERIFIER);
				Log.e("","aceess : "+verifier);

				try {
					// Get the access token
					AccessToken accessToken = twitter.getOAuthAccessToken(
							requestToken, verifier);
					Log.e("","aceess : "+accessToken);
					// Shared Preferences
					Editor e = mSharedPreferences.edit();

					// After getting access token, access token secret
					// store them in application preferences
					e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
					e.putString(PREF_KEY_OAUTH_SECRET,
							accessToken.getTokenSecret());
					// Store login status - true
					e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
					e.commit(); // save changes

					Log.e("Twitter OAuth Token", "> " + accessToken.getToken());

				} catch (Exception e) 
				{					
					Log.e("Twitter Login Error", "> " + e.getMessage());
					e.printStackTrace();
				}//End try catch block
			}//End Inner if
		}//ENd outer if condition

	}//END checkConnection Method


	/**
	 * Function to login twitter
	 * */
	private void loginToTwitter() 
	{
		// Check if already logged in
		if (!isTwitterLoggedInAlready()) {
			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setUseSSL(true);
			builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
			builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);
			Configuration configuration = builder.build();

			TwitterFactory factory = new TwitterFactory(configuration);
			twitter = factory.getInstance();

			try {
				requestToken = twitter
						.getOAuthRequestToken(TWITTER_CALLBACK_URL);
				this.startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse(requestToken.getAuthenticationURL())));
			} catch (TwitterException e) {
				e.printStackTrace();
			}
		} else {
			// user already logged into twitter
			/*Toast.makeText(getApplicationContext(),
					"Already Logged into twitter", Toast.LENGTH_LONG).show();*/
		}
	}


	/**
	 * Check user already logged in your application using twitter Login flag is
	 * fetched from Shared Preferences
	 * */
	private boolean isTwitterLoggedInAlready() 
	{
		// return twitter login status from Shared Preferences
		return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
	}


	/**
	 * Function to update status
	 * */
	class updateTwitterStatus extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ShareActivity.this);
			pDialog.setMessage(" ");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			//Log.d("Tweet Text", "> " + args[0]);
			String status = args[0];
			try {
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(TWITTER_CONSUMER_KEY);
				builder.setOAuthConsumerSecret(TWITTER_CONSUMER_SECRET);

				// Access Token 
				String access_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");
				// Access Token Secret
				String access_token_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");

				AccessToken accessToken = new AccessToken(access_token, access_token_secret);
				Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

				// Update status
				/*twitter4j.Status response = twitter.updateStatus(status);                 
				Log.d("Status", "> " + response.getText());*/

				// Update status
				if(status.length() >= 144)
				{
					status = " ";
				}
				StatusUpdate ad = new StatusUpdate(status);
				// The InputStream opens the resourceId and sends it to the buffer
				InputStream is = getResources().openRawResource(R.raw.ic_launcher);

				ad.setMedia(status,bs);

				twitter4j.Status response2 = twitter.updateStatus(ad);
				//Log.d("Status", "> " + response2.getText());

			} catch (TwitterException e) {
				// Error in updating status
				Log.d("Twitter Update Error", e.getMessage());
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog and show
		 * the data in UI Always use runOnUiThread(new Runnable()) to update UI
		 * from background thread, otherwise you will get error
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			/*Toast.makeText(getApplicationContext(),
					"Status tweeted successfully", Toast.LENGTH_SHORT)
					.show();*/
			logoutFromTwitter();
			DialogGenerator dg = new DialogGenerator(ShareActivity.this);
			dg.generateDateWarningDialog(alertMessage);
		}
	}

	/**
	 * Function to logout from twitter
	 * It will just clear the application shared preferences
	 * */
	private void logoutFromTwitter() 
	{
		// Clear the shared preferences
		Editor e = mSharedPreferences.edit();
		e.remove(PREF_KEY_OAUTH_TOKEN);
		e.remove(PREF_KEY_OAUTH_SECRET);
		e.remove(PREF_KEY_TWITTER_LOGIN);
		e.commit();	
	}



	private void getAndSetWidget() 
	{
		imgScreen 		= (ImageView) findViewById(R.id.imgScreen);
		txtMsg			= (TextView) findViewById(R.id.txtMsg);
		txtTitleScreen	= (TextView) findViewById(R.id.txtTitleScreen);
		postStatusUpdateButton = (Button) findViewById(R.id.postStatusUpdateButton);

		Translation translate = new Translation(this);
		translate.openConnection();
		String text		= translate.getTranselationTextByTextIdentifier("btnTitleShare");		
		txtTitleScreen.setText(text);
		text		= translate.getTranselationTextByTextIdentifier("btnTitlePostOnWall");
		postStatusUpdateButton.setText(text);

		txtMsg.setText(message);
		imgScreen.setBackgroundResource(0);
		imgScreen.setImageBitmap(bitmap);

		alertMessage = translate.getTranselationTextByTextIdentifier("alertMsgPostedSuccessfully");
		translate.closeConnection();
		postStatusUpdateButton.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View view)
			{
				if(R.id.btnFbSahre == id)
				{
					onClickPostStatusUpdate();
				}
				else
				{
					//Log.e("","Click");
					new updateTwitterStatus().execute(message);
				}
			}
		});	

	}//END getAndSetWidget method




	@Override
	protected void onResume() {
		super.onResume();
		if(R.id.btnFbSahre == id)
			uiHelper.onResume();			
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(R.id.btnFbSahre == id)
		{
			uiHelper.onSaveInstanceState(outState);
			outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(R.id.btnFbSahre == id)
		{
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
			uiHelper.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if(R.id.btnFbSahre == id)
			uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if(R.id.btnFbSahre == id)
		{
			uiHelper.onDestroy();
			/*
			if(Session.getActiveSession() != null)
				Session.getActiveSession().closeAndClearTokenInformation();*/
		}
		else
		{
			logoutFromTwitter();
		}
	}

	private void onClickPostStatusUpdate() 
	{		
		//postStatusUpdate();
		//Log.e("", "on click");
		performPublish(PendingAction.POST_STATUS_UPDATE);
	}


	private void performPublish(PendingAction action)
	{
		//Log.e("", "perform Publish");
		Session session = Session.getActiveSession();
		if (session != null)
		{
			Log.e("", "Valid session");
			pendingAction = action;
			/*if (hasPublishPermission())
			{*/
			Log.e("", "Has Permission");
			// We can do the action right away.
			handlePendingAction();

			/*} else 
			{
				Session.NewPermissionsRequest newPermissionsRequest = new Session
						.NewPermissionsRequest(this, Arrays.asList("publish_actions"));
				session.requestNewPublishPermissions(newPermissionsRequest);
				final String[] PERMISSION_ARRAY_PUBLISH = {"publish_stream"};
				final List<String> permissionList = Arrays.asList(PERMISSION_ARRAY_PUBLISH);

				// If all required permissions are available...
				if(session.getPermissions().containsAll(permissionList))
				{
					// Handle success case here.
					handlePendingAction();
				}

			}*/
		}
	}





	private void onSessionStateChange(Session session, SessionState state, Exception exception) 
	{
		if (state == SessionState.OPENED_TOKEN_UPDATED) 
		{
			handlePendingAction();
		}
	}


	@SuppressWarnings("incomplete-switch")
	private void handlePendingAction() {
		//Log.e("", "HandlingAction");
		PendingAction previouslyPendingAction = pendingAction;
		// These actions may re-set pendingAction if they are still pending, but we assume they
		// will succeed.
		pendingAction = PendingAction.NONE;

		switch (previouslyPendingAction)
		{
		case POST_STATUS_UPDATE:	
			//Log.e("", "POst update");
			postStatusUpdate();
			break;
		}
	}



	private void postStatusUpdate()
	{
		//Log.e("", "POsting on wall");
		/*if (hasPublishPermission()) 
		{*/
		pd = CommonUtils.getProgressDialog(this);
		pd.show();
		Log.e("", "user valid");
		View view = getWindow().getDecorView().getRootView();
		view.setDrawingCacheEnabled(true);

		Request photoRequest = Request.newUploadPhotoRequest(Session.getActiveSession(), bitmap, 
				new Request.Callback() {

			@Override
			public void onCompleted(Response response)
			{
				showPublishResult(message, response.getGraphObject(), response.getError());

			}
		});


		//	final String message = getString(R.string.hello_world, (new Date().toString()));
		Bundle parameters = photoRequest.getParameters();

		parameters.putString("message", message);
		photoRequest.executeAsync();
		/*} else {
			pendingAction = PendingAction.POST_STATUS_UPDATE;
		}*/
	}



	private boolean hasPublishPermission() {
		Session session = Session.getActiveSession();
		return session != null && session.getPermissions().contains("publish_actions");
	}


	private void showPublishResult(String message, GraphObject result, FacebookRequestError error) 
	{	
		if (error != null) 
		{
			alertMessage = error.getErrorMessage();
		} 

		if(pd != null)
			pd.cancel();

		DialogGenerator dg = new DialogGenerator(this);
		dg.generateDateWarningDialog(alertMessage);
	}

	private interface GraphObjectWithId extends GraphObject {
		String getId();
	}



	class LoginTwitter extends AsyncTask<Void, Void, Void>
	{
		ProgressDialog pd;

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(ShareActivity.this);
			pd.show();
			super.onPreExecute();
		}
		@Override
		protected Void doInBackground(Void... params)
		{
			checkConnection();
			loginToTwitter();
			return null;
		}
		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();
			//logoutFromTwitter();
			super.onPostExecute(result);
		}

	}

}
