package com.mathfriendzy.model.registration;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mathfriendzy.database.Database;
import com.mathfriendzy.model.friendzy.FriendzyDTO;

public class UserPlayerOperation
{
	private final String USER_PLAYER_TABLE_NAME     = "userPlayer";
	private final String PARENT_USER_ID				= "parentUserId";
	private final String PLAYER_ID					= "playerid";
	private final String FIRST_NAME					= "firstname";
	private final String LAST_NAME					= "lastname";
	private final String USER_NAME					= "username";
	private final String CITY						= "city";
	private final String COIN						= "coin";
	private final String COMPLETE_LEVEL				= "completelavel";
	private final String GRADE						= "grade";
	private final String POINTS						= "points";
	private final String PROFILE_IMAGE				= "profileImage";
	private final String SCHOOL_ID					= "schoolId";
	private final String SCHOOL_NAME				= "schoolName";
	private final String TEACHER_FIRST_NAME			= "teacherFirstName";
	private final String TEACHER_LAST_NAME			= "teacheLastName";
	private final String TEACHER_USER_ID			= "teacherUserId";
	private final String STATE_NAME					= "stateName";
	private final String INDEX_OF_APPEARANCE        = "indexOfAppearance";
	private final String PROFILE_IMAGE_NAME         = "imageName";

	private final String IS_TUTOR = "isTutor";
	private final String CHAT_ID = "chatId";
	private final String CHAT_USER_NAME = "chatUserName";
	private final String STUDENT_ID_BY_TEACHER = "studentIdByTeacher";

	//for professional tutor changes
	private final String ALLOW_PAID_TUTOR = "allowPaidTutor";
	private final String PURCHASED_TIME = "purchasedTime";


	private SQLiteDatabase dbConn 					= null;
	private Context        context					= null;

	/**
	 * Constructor
	 * @param context
	 */
	public UserPlayerOperation(Context context)
	{
		this.context = context;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
	}

	/**
	 * This method create the user player table if it is not exist
	 * @param playerList
	 */
	public void createUserPlayerTable(ArrayList<UserPlayerDto> playerList)
	{
		/*String query = " create table " + USER_PLAYER_TABLE_NAME + " (" + PARENT_USER_ID + " text, " + PLAYER_ID + " text , "
						+ FIRST_NAME + " text , " + LAST_NAME + "  text , " +  USER_NAME + "  text , " + CITY + "  text , " 
						+ COIN + " text , " + COMPLETE_LEVEL + " text , " + GRADE + " text , " + POINTS + "  text , "
						+ PROFILE_IMAGE + " BLOB , " + SCHOOL_ID + " text , " + SCHOOL_NAME + " text , " + TEACHER_FIRST_NAME 
						+ " text , " + TEACHER_LAST_NAME + " text , " + TEACHER_USER_ID + " text , " + STATE_NAME + " text," 
						+ INDEX_OF_APPEARANCE + "  text ) ";//, " + IMAGE_NAME + " text )";
		 */

		String query = " create table " + USER_PLAYER_TABLE_NAME + " (" + PARENT_USER_ID + " text, " + PLAYER_ID + " text , "
				+ FIRST_NAME + " text , " + LAST_NAME + "  text , " +  USER_NAME + "  text , " + CITY + "  text , "
				+ COIN + " text , " + COMPLETE_LEVEL + " text , " + GRADE + " text , " + POINTS + "  text , "
				+ PROFILE_IMAGE + " BLOB , " + SCHOOL_ID + " text , " + SCHOOL_NAME + " text , " + TEACHER_FIRST_NAME
				+ " text , " + TEACHER_LAST_NAME + " text , " + TEACHER_USER_ID + " text , " + STATE_NAME + " text,"
				+ INDEX_OF_APPEARANCE + "  text  , " + PROFILE_IMAGE_NAME + " text , " + IS_TUTOR + " text , "
				+ CHAT_ID + " text , " + CHAT_USER_NAME + " text , " + STUDENT_ID_BY_TEACHER + " text , "
				+ ALLOW_PAID_TUTOR + " text , " + PURCHASED_TIME + " text )";

		dbConn.execSQL(query);

		this.deleteFromUserPlayer();
		this.insertUserPlayerData(playerList);
		if(dbConn != null)
			dbConn.close();
	}

	/**
	 * Check for if User Player teble exist
	 * @return
	 */
	public boolean isUserPlayerTableExist()
	{
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?",
				new String[] {"table", USER_PLAYER_TABLE_NAME});
		if(cursor.moveToFirst())
		{
			cursor.close();
			return true;
		}
		else
		{
			cursor.close();
			return false;
		}
	}

	/**
	 * This method delete the data from the table userPlayer
	 */
	public void deleteFromUserPlayer()
	{
		dbConn.delete(USER_PLAYER_TABLE_NAME , null , null);
	}

	/**
	 * This method insert the data into the UserPlaye Table
	 * @param playerList
	 */
	public void insertUserPlayerData(ArrayList<UserPlayerDto> playerList)
	{
		//this.deleteFromUserPlayer();

		UserPlayerDto userPlayerObj = null;
		for(int i = 0 ; i < playerList.size() ; i++)
		{
			userPlayerObj = playerList.get(i);
			ContentValues contentValues = new ContentValues();
			contentValues.put(PARENT_USER_ID, userPlayerObj.getParentUserId());
			contentValues.put(PLAYER_ID, userPlayerObj.getPlayerid());
			contentValues.put(FIRST_NAME, userPlayerObj.getFirstname());
			contentValues.put(LAST_NAME, userPlayerObj.getLastname());
			contentValues.put(USER_NAME, userPlayerObj.getUsername());
			contentValues.put(CITY, userPlayerObj.getCity());
			contentValues.put(COIN, userPlayerObj.getCoin());
			contentValues.put(COMPLETE_LEVEL, userPlayerObj.getCompletelavel());
			contentValues.put(GRADE, userPlayerObj.getGrade());
			contentValues.put(POINTS, userPlayerObj.getPoints());
			contentValues.put(PROFILE_IMAGE, userPlayerObj.getProfileImage());
			contentValues.put(SCHOOL_ID, userPlayerObj.getSchoolId());
			contentValues.put(SCHOOL_NAME, userPlayerObj.getSchoolName());
			contentValues.put(TEACHER_FIRST_NAME, userPlayerObj.getTeacherFirstName());
			contentValues.put(TEACHER_LAST_NAME, userPlayerObj.getTeacheLastName());
			contentValues.put(TEACHER_USER_ID, userPlayerObj.getTeacherUserId());
			contentValues.put(STATE_NAME, userPlayerObj.getStateName());
			contentValues.put(INDEX_OF_APPEARANCE, userPlayerObj.getIndexOfAppearance());
			contentValues.put(PROFILE_IMAGE_NAME, userPlayerObj.getImageName());
			contentValues.put(STUDENT_ID_BY_TEACHER, userPlayerObj.getStudentIdByTeacher());

			//            contentValues.put(IS_TUTOR, userPlayerObj.getIsTutor());
			//            contentValues.put(CHAT_ID, userPlayerObj.getChatId());
			//            contentValues.put(CHAT_USER_NAME, userPlayerObj.getChatUserName());

			//
			//            contentValues.put(ALLOW_PAID_TUTOR, userPlayerObj.getAllowPaidTutor());
			//            contentValues.put(PURCHASED_TIME, userPlayerObj.getPurchasedTime());
			dbConn.insert(USER_PLAYER_TABLE_NAME, null, contentValues);
		}
		if(dbConn != null)
			dbConn.close();
	}

	/**
	 * This method return the UserPlayer Record
	 * @return
	 */
	public ArrayList<UserPlayerDto> getUserPlayerData()
	{
		ArrayList<UserPlayerDto> userPlayerList = new ArrayList<UserPlayerDto>();

		String query = " select * from " + USER_PLAYER_TABLE_NAME;
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			UserPlayerDto userPlayerObj = new UserPlayerDto();
			userPlayerObj.setParentUserId(cursor.getString(cursor.getColumnIndex(PARENT_USER_ID)));
			userPlayerObj.setPlayerid(cursor.getString(cursor.getColumnIndex(PLAYER_ID)));
			userPlayerObj.setFirstname(cursor.getString(cursor.getColumnIndex(FIRST_NAME)));
			userPlayerObj.setLastname(cursor.getString(cursor.getColumnIndex(LAST_NAME)));
			userPlayerObj.setUsername(cursor.getString(cursor.getColumnIndex(USER_NAME)));
			userPlayerObj.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
			userPlayerObj.setCoin(cursor.getString(cursor.getColumnIndex(COIN)));
			userPlayerObj.setCompletelavel(cursor.getString(cursor.getColumnIndex(COMPLETE_LEVEL)));
			userPlayerObj.setGrade(cursor.getString(cursor.getColumnIndex(GRADE)));
			userPlayerObj.setPoints(cursor.getString(cursor.getColumnIndex(POINTS)));
			userPlayerObj.setProfileImage(cursor.getBlob(cursor.getColumnIndex(PROFILE_IMAGE)));
			userPlayerObj.setSchoolId(cursor.getString(cursor.getColumnIndex(SCHOOL_ID)));
			userPlayerObj.setSchoolName(cursor.getString(cursor.getColumnIndex(SCHOOL_NAME)));
			userPlayerObj.setTeacherFirstName(cursor.getString(cursor.getColumnIndex(TEACHER_FIRST_NAME)));
			userPlayerObj.setTeacheLastName(cursor.getString(cursor.getColumnIndex(TEACHER_LAST_NAME)));
			userPlayerObj.setTeacherUserId(cursor.getString(cursor.getColumnIndex(TEACHER_USER_ID)));
			userPlayerObj.setStateName(cursor.getString(cursor.getColumnIndex(STATE_NAME)));
			userPlayerObj.setIndexOfAppearance(cursor.getString(cursor.getColumnIndex(INDEX_OF_APPEARANCE)));
			userPlayerObj.setImageName(cursor.getString(cursor.getColumnIndex(PROFILE_IMAGE_NAME)));

			//            userPlayerObj.setIsTutor(cursor.getString(cursor.getColumnIndex(IS_TUTOR)));
			//            userPlayerObj.setChatId(cursor.getString(cursor.getColumnIndex(CHAT_ID)));
			//            userPlayerObj.setChatUserName(cursor.getString(cursor.getColumnIndex(CHAT_USER_NAME)));
			userPlayerObj.setStudentIdByTeacher(cursor.getString(cursor.getColumnIndex(STUDENT_ID_BY_TEACHER)));
			//
			//            userPlayerObj.setAllowPaidTutor(cursor.getString(cursor.getColumnIndex(ALLOW_PAID_TUTOR)));
			//            userPlayerObj.setPurchasedTime(cursor.getString(cursor.getColumnIndex(PURCHASED_TIME)));
			userPlayerList.add(userPlayerObj);
		}
		if(cursor != null)
			cursor.close();
		if(dbConn != null)
			dbConn.close();

		return userPlayerList;
	}

	/**
	 * Check for is UserPlayer exist or not
	 * @return
	 */
	public boolean isUserPlayersExist()
	{
		String query = " select * from " + USER_PLAYER_TABLE_NAME;
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToFirst())
		{
			cursor.close();
			dbConn.close();
			return false;
		}
		else
		{
			cursor.close();
			dbConn.close();
			return true;
		}
	}

	/**
	 * Thic method close the connection with the database
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}

	/**
	 * This method return the userPlayer Data by its ID
	 * @param playerId
	 * @return
	 */
	public UserPlayerDto getUserPlayerDataById(String playerId)
	{
		UserPlayerDto userPlayerObj = null;
		String query = " select * from " + USER_PLAYER_TABLE_NAME + " where "  + PLAYER_ID + " = '" + playerId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
		{
			userPlayerObj = new UserPlayerDto();
			userPlayerObj.setParentUserId(cursor.getString(cursor.getColumnIndex(PARENT_USER_ID)));
			userPlayerObj.setPlayerid(cursor.getString(cursor.getColumnIndex(PLAYER_ID)));
			userPlayerObj.setFirstname(cursor.getString(cursor.getColumnIndex(FIRST_NAME)));
			userPlayerObj.setLastname(cursor.getString(cursor.getColumnIndex(LAST_NAME)));
			userPlayerObj.setUsername(cursor.getString(cursor.getColumnIndex(USER_NAME)));
			userPlayerObj.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
			userPlayerObj.setCoin(cursor.getString(cursor.getColumnIndex(COIN)));
			userPlayerObj.setCompletelavel(cursor.getString(cursor.getColumnIndex(COMPLETE_LEVEL)));
			userPlayerObj.setGrade(cursor.getString(cursor.getColumnIndex(GRADE)));
			userPlayerObj.setPoints(cursor.getString(cursor.getColumnIndex(POINTS)));
			userPlayerObj.setProfileImage(cursor.getBlob(cursor.getColumnIndex(PROFILE_IMAGE)));
			userPlayerObj.setSchoolId(cursor.getString(cursor.getColumnIndex(SCHOOL_ID)));
			userPlayerObj.setSchoolName(cursor.getString(cursor.getColumnIndex(SCHOOL_NAME)));
			//userPlayerObj.setTeacherFirstName(cursor.getString(cursor.getColumnIndex(TEACHER_FIRST_NAME)));
			//userPlayerObj.setTeacheLastName(cursor.getString(cursor.getColumnIndex(TEACHER_LAST_NAME)));
			userPlayerObj.setTeacherUserId(cursor.getString(cursor.getColumnIndex(TEACHER_USER_ID)));
			userPlayerObj.setStateName(cursor.getString(cursor.getColumnIndex(STATE_NAME)));
			userPlayerObj.setIndexOfAppearance(cursor.getString(cursor.getColumnIndex(INDEX_OF_APPEARANCE)));
			userPlayerObj.setImageName(cursor.getString(cursor.getColumnIndex(PROFILE_IMAGE_NAME)));

			//            userPlayerObj.setIsTutor(cursor.getString(cursor.getColumnIndex(IS_TUTOR)));
			//            userPlayerObj.setChatId(cursor.getString(cursor.getColumnIndex(CHAT_ID)));
			//            userPlayerObj.setChatUserName(cursor.getString(cursor.getColumnIndex(CHAT_USER_NAME)));
			//            //userPlayerObj.setStudentIdByTeacher(cursor.getString(cursor.getColumnIndex(STUDENT_ID_BY_TEACHER)));
			//            userPlayerObj.setTeacherFirstName(this.getCursorValue(cursor , TEACHER_FIRST_NAME));
			//            userPlayerObj.setTeacheLastName(this.getCursorValue(cursor , TEACHER_LAST_NAME));
			userPlayerObj.setStudentIdByTeacher(this.getCursorValue(cursor , STUDENT_ID_BY_TEACHER));
			//
			//            userPlayerObj.setAllowPaidTutor(cursor.getString(cursor.getColumnIndex(ALLOW_PAID_TUTOR)));
			//            userPlayerObj.setPurchasedTime(cursor.getString(cursor.getColumnIndex(PURCHASED_TIME)));
		}
		if(cursor != null)
			cursor.close();
		if(dbConn != null)
			dbConn.close();

		return userPlayerObj;
	}

	private String getCursorValue(Cursor cursor , String columnKey){
		String value = "";
		try{
			if (!cursor.isNull(cursor.getColumnIndex(columnKey)))
				value = cursor.getString(cursor.getColumnIndex(columnKey));
		}catch(Exception e){
			e.printStackTrace();
		}
		return value;
	}

	/**
	 * This method delete the record from the user player data by id
	 * @param playerId
	 */
	public void deleteFromUserPlayerByPlayerId(String playerId)
	{
		dbConn.delete(USER_PLAYER_TABLE_NAME , PLAYER_ID + " = ?", new String[]{playerId});
	}


	/**
	 * This method return the player id correspondent to the user name
	 * @param userName
	 * @return
	 */
	public String getPlayerIdbyUserName(String userName)
	{
		String playerId = "";
		String query = " select " + PLAYER_ID + " from " + USER_PLAYER_TABLE_NAME + " where "  + USER_NAME + " = '" + userName + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
		{
			playerId = cursor.getString(cursor.getColumnIndex(PLAYER_ID));
		}

		if(cursor != null)
			cursor.close();

		return playerId;
	}

	/**
	 * This method return the player id correspondent to the user name
	 * @param userName
	 * @return
	 */
	public String getUserIdbyUserName(String userName)
	{
		String userId = "";
		String query = " select " + PARENT_USER_ID + " from " + USER_PLAYER_TABLE_NAME + " where "  + USER_NAME + " = '" + userName + "'";
		Cursor cursor = dbConn.rawQuery(query, null);
		if(cursor.moveToNext())
		{
			userId = cursor.getString(cursor.getColumnIndex(PARENT_USER_ID));
		}

		if(cursor != null)
			cursor.close();

		return userId;
	}

	/**
	 * This method update the player coins
	 * @param coins
	 * @param playerId
	 */
	public void updataPlayerCoins(int coins , String playerId)
	{
		String where = PLAYER_ID + " = '" + playerId + "'";
		ContentValues cv = new ContentValues();
		cv.put(COIN, coins + "");

		dbConn.update(USER_PLAYER_TABLE_NAME, cv, where, null);
	}

	/**
	 * Update player grade and avtar
	 * @param playerId
	 * @param grade
	 * @param avatar
	 */
	public void updatePlayerGradeAvatar(String playerId ,
			String grade , String avatar , String fName , String lName){
		String where = PLAYER_ID + " = '" + playerId + "'";
		ContentValues cv = new ContentValues();
		cv.put(PROFILE_IMAGE_NAME, avatar);
		cv.put(GRADE, grade);
		cv.put(FIRST_NAME, fName);
		cv.put(LAST_NAME, lName);
		dbConn.update(USER_PLAYER_TABLE_NAME, cv, where, null);
	}

	/*
	 * Edit code by shilpi
	 */
	/**
	 * Use to fetch teacher record for create challenge
	 * @return
	 */
	public FriendzyDTO getTeacherDetails()
	{
		FriendzyDTO dto = new FriendzyDTO();
		String query = "select userid, fname, lname, schoolid, schoolname from user";

		Cursor cursor = dbConn.rawQuery(query, null);
		while(cursor.moveToNext())
		{
			dto.setTeacherId(cursor.getString(0));
			dto.setTeacherName(cursor.getString(1)+" "+cursor.getString(2));
			dto.setSchoolId(cursor.getString(3));
			dto.setSchoolName(cursor.getString(4));
		}

		if(cursor != null)
			cursor.close();
		if(dbConn != null)
			dbConn.close();
		return dto;
	}


	/* public void updateChatPlayerDetail(UserPlayerDto selectedPlayer , QBUser qbUser){
        String where = PLAYER_ID + " = '" + selectedPlayer.getPlayerid() + "'";
        ContentValues cv = new ContentValues();
        cv.put(CHAT_USER_NAME, qbUser.getLogin());
        cv.put(CHAT_ID, qbUser.getId());
        dbConn.update(USER_PLAYER_TABLE_NAME, cv, where, null);
    }*/

	/**
	 * Update the isTutor value
	 * @param selectedPlayer
	 * @param isTutor
	 */
	public void updateIsTutorValue(UserPlayerDto selectedPlayer , int isTutor){
		String where = PLAYER_ID + " = '" + selectedPlayer.getPlayerid() + "'";
		ContentValues cv = new ContentValues();
		cv.put(IS_TUTOR, isTutor);
		dbConn.update(USER_PLAYER_TABLE_NAME, cv, where, null);
	}
}
