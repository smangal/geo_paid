package com.mathfriendzy.model.registration;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mathfriendzy.database.Database;

/**
 * This class for the Registered User Operation
 * @author Yashwant Singh
 *
 */
public class UserRegistrationOperation 
{
	private final String USER_TABLE_NAME = "user";
	private final String FIRST_NAME = "fname";
	private final String USER_ID  	= "userId";
	private final String VOLUME 	= "volume";
	private final String SCHOOL_ID	= "schoolId";
	private final String SCHOOL_NAME= "schoolName";
	private final String PREFERED_LANGUAGE_ID = "preferredLanguageId";
	private final String LAST_NAME 	= "lname";
	private final String EMAIL		= "email";
	private final String PASSWORD 	= "password";
	private final String IS_PARENT 	= "isParent";
	private final String IS_STUDENT = "isStudent";
	private final String IS_TEACHER	= "isTeacher";
	private final String COUNTRY_ID	= "countryId";
	private final String STATE_ID 	= "stateId";
	private final String STATE 		= "state";
	private final String CITY		= "city";
	private final String COINS 		= "coins";
	private final String ZIP 		= "zip";
	
	private SQLiteDatabase dbConn 					= null;
	private Context        context					= null;
	
	/**
	 * Constructor
	 * @param context
	 */
	public UserRegistrationOperation(Context context)
	{
		this.context = context;
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
	}
	
	/**
	 * This method create the UserTable
	 * @param regObj
	 */
	public void createUserTable(RegistereUserDto regObj)
	{		
		String query = " create table " + USER_TABLE_NAME + " (" + USER_ID + " text, " + FIRST_NAME + " text , "
						+ LAST_NAME + " text , " + VOLUME + "  text , " +  SCHOOL_ID + "  text , " + SCHOOL_NAME + "  text , " 
						+ PREFERED_LANGUAGE_ID + " text , " + EMAIL + " text , " + PASSWORD + " text , " + IS_PARENT + "  text , "
						+ IS_STUDENT + " text , " + IS_TEACHER + " text , " + COUNTRY_ID + " text , " + STATE_ID 
						+ " text , " + STATE + " text , " + CITY + " text , " + COINS + " text," 
						+ ZIP + "  text  )";
		
		dbConn.execSQL(query);
		this.insertUserData(regObj);
		if(dbConn != null)
			dbConn.close();
	}
	
	/**
	 * Check for is User Table exist
	 * @return
	 */
	public boolean isUserTableExist()
	{		
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
	    											new String[] {"table", USER_TABLE_NAME});
	    if(cursor.moveToFirst())
	    {
	    	cursor.close();
	    	return true;
	    }
	    else
	    {
	    	cursor.close();
	    	return false;
	    }
	}
	
	/**
	 * This method delete the user data from the User Table
	 */
	public void deleteFromUser()
	{
		dbConn.delete(USER_TABLE_NAME , null , null);
	}
	
	/**
	 * This method insert the data into database
	 * @param regUserObj
	 */
	public void insertUserData(RegistereUserDto regUserObj)
	{	
		this.deleteFromUser();
		
		ContentValues contentValues = new ContentValues();
		contentValues.put(USER_ID, regUserObj.getUserId());
		contentValues.put(FIRST_NAME, regUserObj.getFirstName());
		contentValues.put(LAST_NAME, regUserObj.getLastName());
		contentValues.put(VOLUME, regUserObj.getVolume());
		contentValues.put(SCHOOL_ID, regUserObj.getSchoolId());
		contentValues.put(SCHOOL_NAME, regUserObj.getSchoolName());
		contentValues.put(PREFERED_LANGUAGE_ID, regUserObj.getPreferedLanguageId());
		contentValues.put(EMAIL, regUserObj.getEmail());
		contentValues.put(PASSWORD, regUserObj.getPass());
		contentValues.put(IS_PARENT, regUserObj.getIsParent());//changes
		contentValues.put(IS_STUDENT, "0");
		contentValues.put(IS_TEACHER, "0");
		contentValues.put(COUNTRY_ID, regUserObj.getCountryId());
		contentValues.put(STATE_ID, regUserObj.getStateId());
		contentValues.put(STATE, regUserObj.getState());
		contentValues.put(CITY, regUserObj.getCity());
		
		if(regUserObj.getCoins().equals("")){
			contentValues.put(COINS, "");
		}else{
		contentValues.put(COINS, regUserObj.getCoins());
		}
		contentValues.put(ZIP, regUserObj.getZip());
		
		dbConn.insert(USER_TABLE_NAME, null, contentValues);
		if(dbConn != null)
			dbConn.close();
	}
	
	/**
	 * This method return the User Data
	 * @return
	 */
	public RegistereUserDto getUserData()
	{
		RegistereUserDto regUserObj = new RegistereUserDto();
		String query = " select * from " + USER_TABLE_NAME;
		Cursor cursor = dbConn.rawQuery(query, null);
				
		if(cursor.moveToNext())
		{
			regUserObj.setUserId(cursor.getString(cursor.getColumnIndex(USER_ID)));
			regUserObj.setFirstName(cursor.getString(cursor.getColumnIndex(FIRST_NAME)));
			regUserObj.setLastName(cursor.getString(cursor.getColumnIndex(LAST_NAME)));
			regUserObj.setVolume(cursor.getString(cursor.getColumnIndex(VOLUME)));
			regUserObj.setSchoolId(cursor.getString(cursor.getColumnIndex(SCHOOL_ID)));
			regUserObj.setSchoolName(cursor.getString(cursor.getColumnIndex(SCHOOL_NAME)));
			regUserObj.setPreferedLanguageId(cursor.getString(cursor.getColumnIndex(PREFERED_LANGUAGE_ID)));
			regUserObj.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
			regUserObj.setPass(cursor.getString(cursor.getColumnIndex(PASSWORD)));
			regUserObj.setIsParent(cursor.getString(cursor.getColumnIndex(IS_PARENT)));
			regUserObj.setIsStudent(cursor.getString(cursor.getColumnIndex(IS_STUDENT)));
			regUserObj.setIsTeacher(cursor.getString(cursor.getColumnIndex(IS_TEACHER)));
			regUserObj.setCountryId(cursor.getString(cursor.getColumnIndex(COUNTRY_ID)));
			regUserObj.setStateId(cursor.getString(cursor.getColumnIndex(STATE_ID)));
			regUserObj.setState(cursor.getString(cursor.getColumnIndex(STATE)));
			regUserObj.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
			regUserObj.setCoins(cursor.getString(cursor.getColumnIndex(COINS)));
			regUserObj.setZip(cursor.getString(cursor.getColumnIndex(ZIP)));
		}
		
		if(cursor != null)
			cursor.close();
		if(dbConn != null)
			dbConn.close();
		
		return regUserObj;
	}
	
	
	/**
	 * This method update the coins of the user
	 * @param coins
	 */
	public void updaUserCoins(int coins , String userId)
	{
		String where = USER_ID + " = '" + userId + "'";
		ContentValues cv = new ContentValues();
		cv.put(COINS, coins + "");
				
		dbConn.update(USER_TABLE_NAME, cv, where, null);
	}
	/**
	 * This method close the connection with tha database
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();	
	}
	
	
	/**
	 * This method return the user id 
	 * @return
	 */
	public String getUserId()
	{
		String userId = "";
		String query = " select userId from " + USER_TABLE_NAME;
		Cursor cursor = dbConn.rawQuery(query, null);
		
		if(cursor.moveToNext())
			userId =  cursor.getString(0);
		else
			userId =  "";
		
		if(cursor != null)
			cursor.close();
		this.closeConn();
		
		return userId;
	}


}
