package com.mathfriendzy.model.top100;

import static com.mathfriendzy.utils.ITextIds.TOP_100_URL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.mathfriendzy.controller.top100.Top100ListActivity;
import com.mathfriendzy.utils.CommonUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

public class JsonAsynTask extends AsyncTask<Void, Integer, String> 
{
	Context context;
	int id;
	ProgressDialog progressDialog;
	
	public JsonAsynTask(Context context, int id)
	{
		this.id = id;
		this.context = context;
		
		progressDialog = CommonUtils.getProgressDialog(context);
		progressDialog.show();
	}
	
	
	private String getJsonFile(int id)
	{
		String str = "";
		StringBuilder stringBuilder = new StringBuilder("");
		URL url = null;
		try {
			if(id == 1)
				url = new URL(TOP_100_URL+"StudentsList"+"&appId=7");
			if(id == 2)
				url = new URL(TOP_100_URL+"TeachersList"+"&appId=7");			
			if(id == 3)
				url = new URL(TOP_100_URL+"SchoolsList"+"&appId=7");
						
			BufferedReader ins = new BufferedReader(new InputStreamReader(url.openStream()));				
			try 
			{     
				while((str = ins.readLine()) != null)
				{				
					stringBuilder.append(str);
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}
	
		
	@Override
	protected String doInBackground(Void... params)
	{	
		return getJsonFile(id);
	}

	
	@Override
	protected void onPostExecute(String jsonFile)
	{
		Intent intent = new Intent(context, Top100ListActivity.class);
		TopListDatabase db			= new TopListDatabase();		
		TopBeans topObj	= new TopBeans();
		db.deleteFileFromTopTable(context, id);		
		topObj.setId(String.valueOf(id));
		topObj.setFile(jsonFile);		
		db.insertFileIntoTop100(topObj, context);
		intent.putExtra("id", id);
		progressDialog.cancel();
		context.startActivity(intent);
		((Activity)context).finish();
		super.onPostExecute(jsonFile);
	}
}
