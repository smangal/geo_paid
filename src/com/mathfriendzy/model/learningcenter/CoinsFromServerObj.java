package com.mathfriendzy.model.learningcenter;

public class CoinsFromServerObj 
{
	private int coinsRequired;
	private int coinsEarned;
	private int coinsPurchase;
	private int monthlyCoins;
	private int yearlyCoins;
	private int appStatus;
	
	
	
	public int getAppStatus() {
		return appStatus;
	}
	public void setAppStatus(int appstatus) {
		this.appStatus = appstatus;
	}
	public int getCoinsRequired() {
		return coinsRequired;
	}
	public void setCoinsRequired(int coinsRequired) {
		this.coinsRequired = coinsRequired;
	}
	public int getCoinsEarned() {
		return coinsEarned;
	}
	public void setCoinsEarned(int coinsEarned) {
		this.coinsEarned = coinsEarned;
	}
	public int getCoinsPurchase() {
		return coinsPurchase;
	}
	public void setCoinsPurchase(int coinsPurchase) {
		this.coinsPurchase = coinsPurchase;
	}
	public int getMonthlyCoins() {
		return monthlyCoins;
	}
	public void setMonthlyCoins(int monthlyCoins) {
		this.monthlyCoins = monthlyCoins;
	}
	public int getYearlyCoins() {
		return yearlyCoins;
	}
	public void setYearlyCoins(int yearlyCoins) {
		this.yearlyCoins = yearlyCoins;
	}
	
	
	
}

