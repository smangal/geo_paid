package com.mathfriendzy.model.learningcenter;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

import com.mathfriendzy.utils.CommonUtils;

public class LearningCenteServerOperation 
{
	/**
	 * This method getDetail from server
	 * @param userId
	 * @param playerId
	 */
	public PlayerDataFromServerObj getPlayerDetailFromServer(String userId,String playerId)
	{
		String action = "getPlayerDetails";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "user_id="   + userId  + "&"
				+ "player_id=" + playerId+"&appId="+CommonUtils.APP_ID;
		//Log.e("", ""+strUrl);
		return this.parseJsonForPlayerData(CommonUtils.readFromURL(strUrl));
	}
	
	/**
	 * This method parse json String which contain player detail
	 * @param jsonString
	 */
	private PlayerDataFromServerObj parseJsonForPlayerData(String jsonString)
	{		
		PlayerDataFromServerObj playerData = null;
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONObject jsonObject2 = jObject.getJSONObject("data");
			playerData = new PlayerDataFromServerObj();
			playerData.setPoints(jsonObject2.getInt("point"));
			playerData.setItems(jsonObject2.getString("items"));
			
			ArrayList<PlayerEquationLevelObj> playerList = new ArrayList<PlayerEquationLevelObj>();
			JSONArray jsonArray = jsonObject2.getJSONArray("level");
					
			for( int i = 0 ; i < jsonArray.length() ; i ++ )
			{
				PlayerEquationLevelObj playerEquationLevel = new PlayerEquationLevelObj();
				
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				playerEquationLevel.setUserId(jsonObject.getString("uid"));
				playerEquationLevel.setPlayerId(jsonObject.getString("pid"));
				playerEquationLevel.setEquationType(jsonObject.getInt("eqnId"));
				playerEquationLevel.setLevel(jsonObject.getInt("level"));
				playerEquationLevel.setStars(jsonObject.getInt("stars"));
				
				playerList.add(playerEquationLevel);
			}
			
			playerData.setPlayerLevelList(playerList);
			playerData.setLockStatus(jsonObject2.getInt("appsUnlock"));
			
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		return playerData;
	}
	
	/**
	 * This method return the requaired coins and also the earned and purchase coins
	 * @param apiValue
	 * @return
	 */
	public CoinsFromServerObj getRequiredCoisForPurchase(String apiValue)
	{
		String action = "getNewRequiredCoinsForPurchaseItem";
		//String action = "getRequiredCoinsForPurchaseItem";
		String strUrl = COMPLETE_URL  + "action=" + action + "&" + apiValue+"&appId="+CommonUtils.APP_ID;
		//Log.e("LearningCenterServer", ""+strUrl);		
		return this.parseCoindPurchasedJson(CommonUtils.readFromURL(strUrl));	
	}

	private CoinsFromServerObj parseCoindPurchasedJson(String jsonString) 
	{
		//Log.e("Sever operation for learnign center", "inside parsecoinsPurchase " + jsonString );		
		CoinsFromServerObj coinsFromServerobj = new CoinsFromServerObj();
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONObject jsonObject2 = jObject.getJSONObject("data");
			
			
			coinsFromServerobj.setCoinsRequired(jsonObject2.getInt("coinsRequired"));						
			coinsFromServerobj.setMonthlyCoins(jsonObject2.getInt("monthPrice"));
			coinsFromServerobj.setYearlyCoins(jsonObject2.getInt("yearPrice"));
			try{
				coinsFromServerobj.setCoinsEarned(jsonObject2.getInt("coinsEarned"));
			}
			catch (Exception e) {
				coinsFromServerobj.setCoinsEarned(-1);
			}
			try{
				coinsFromServerobj.setCoinsPurchase(jsonObject2.getInt("coinsPurchsed"));
			}
			catch (Exception e) {

				coinsFromServerobj.setCoinsPurchase(0);
			}
			
		}
		catch (JSONException e) 
		{
			coinsFromServerobj.setCoinsEarned(-1);
			coinsFromServerobj.setCoinsPurchase(0);
			coinsFromServerobj.setMonthlyCoins(0);
			coinsFromServerobj.setYearlyCoins(0);
			//Log.e("LearningCenteServerOperation", "Do not getting data for temp player");
			//e.printStackTrace();
		}
		
		return coinsFromServerobj;
	}
	
	public void addAllLevel(String levelXml)
	{
		String action = "addAllLevels";
		String strUrl = COMPLETE_URL  + "action=" + action + "&" 
						+ "allLevels=" +  levelXml
						+"&appId="+CommonUtils.APP_ID;
		
		//Log.e("LearningCenteServerOperation", "strUrl " + strUrl);
		
		this.parseJson(CommonUtils.readFromURL(strUrl));
	}
	
	private void parseJson(String jsonString)
	{
		//Log.e("LearningCenteServerOperation", "inside parseJson " + jsonString);
	}
	
	/**
	 * This method return the requaired coins and also the earned and purchase coins
	 * @param apiValue
	 * @return
	 */
	public CoinsFromServerObj getSubscriptionInfoForUser(String apiValue)
	{
		String action = "getSubscriptionInfoForUser";		
		String strUrl = COMPLETE_URL  + "action=" + action + "&" + apiValue+"&appId="+CommonUtils.APP_ID;
		//Log.e("LearningCenterServer", ""+strUrl);		
		return this.parseCoinsSubscription(CommonUtils.readFromURL(strUrl));	
	}
	
	private CoinsFromServerObj parseCoinsSubscription(String jsonString) 
	{
		//Log.e("Sever operation for learnign center", "inside parsecoinsPurchase " + jsonString );		
		
		CoinsFromServerObj coinsFromServerobj = new CoinsFromServerObj();
		
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);			
			int appStatus		= jObject.getInt("appsUnlock");
			coinsFromServerobj.setAppStatus(appStatus);
			if(appStatus == 1)
			{
				return coinsFromServerobj;
			}
			//coinsFromServerobj.setCoinsRequired(jsonObject2.getInt("coinsRequired"));						
			coinsFromServerobj.setMonthlyCoins(jObject.getInt("monthPrice"));
			coinsFromServerobj.setYearlyCoins(jObject.getInt("yearPrice"));
			try{
				coinsFromServerobj.setCoinsEarned(jObject.getInt("coinsEarned"));
			}
			catch (Exception e) {
				coinsFromServerobj.setCoinsEarned(-1);
			}
			try{
				coinsFromServerobj.setCoinsPurchase(jObject.getInt("coinsPurchsed"));
			}
			catch (Exception e) {

				coinsFromServerobj.setCoinsPurchase(0);
			}
			
		}
		catch (JSONException e) 
		{
			coinsFromServerobj.setCoinsEarned(-1);
			coinsFromServerobj.setCoinsPurchase(0);
			coinsFromServerobj.setMonthlyCoins(0);
			coinsFromServerobj.setYearlyCoins(0);
			//Log.e("LearningCenteServerOperation", "Do not getting data for temp player");
			//e.printStackTrace();
		}
		
		return coinsFromServerobj;
	}
}
