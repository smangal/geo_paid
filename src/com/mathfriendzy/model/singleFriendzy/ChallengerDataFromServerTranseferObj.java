package com.mathfriendzy.model.singleFriendzy;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class hold the challenger data from server
 * @author Yashwant Singh
 *
 */
@SuppressWarnings("serial")
public class ChallengerDataFromServerTranseferObj 
{
	private ArrayList<ChallengerTransferObj> challengerPlayerList;
	private String pushNotificationDevice;
	
	public ArrayList<ChallengerTransferObj> getChallengerPlayerList() {
		return challengerPlayerList;
	}
	public void setChallengerPlayerList(
			ArrayList<ChallengerTransferObj> challengerPlayerList) {
		this.challengerPlayerList = challengerPlayerList;
	}
	public String getPushNotificationDevice() {
		return pushNotificationDevice;
	}
	public void setPushNotificationDevice(String pushNotificationDevice) {
		this.pushNotificationDevice = pushNotificationDevice;
	}
	
	
}
