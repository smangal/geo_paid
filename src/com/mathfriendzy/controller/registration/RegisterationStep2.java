package com.mathfriendzy.controller.registration;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;
import static com.mathfriendzy.utils.ICommonUtils.REGISTRATION_STEP_2_FALG;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.geographyfriendzypaid.R;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.player.BasePlayer;
import com.mathfriendzy.controller.player.EditRegisteredUserPlayer;
import com.mathfriendzy.controller.school.SearchYourSchoolActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.chooseAvtar.AvtarServerOperation;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.Language;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.login.Login;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.notification.AddUserWithAndroidDevice;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

/**
 * This is Activity is which open after when user fill the information in step 1 for registration
 * @author Yashwant Singh
 *
 */
public class RegisterationStep2 extends BasePlayer implements OnClickListener
{
	private TextView regTitleRegistration 	= null;
	private TextView lblPleaseEnterYourLocationToSaveYourTimeAndPoints = null;
	private TextView lblRegLanguage 		= null;
	private TextView lblRegVolume 			= null;
	
	private Button    btnTitleSave          = null;
	private SeekBar   seekbarVolume 		= null;
	private Spinner spinnerLanguage 		= null;
	
	private ArrayList<String> languageList  = null;
	
	private String firstName = null;
	private String lastName  = null;
	private String email     = null;
	private String pass      = null;
	
	public static String schoolName				  = null;
	public static boolean IS_REGISTER_SCHOOL	  = false;
	
	String stateId 		= null;
	String stateCode 	= null;
	String countryId    = null;
	String countryIso   = null;
	String languageId   = null;
	/*String schoolId     = null;*/
	String isParentValue= "0";
	String isStudentValue="0";
	String isTeacherValue="0";
	String volume       = null;
	String coins		= null;
	String zip          = null;
	String city			= null;
	String players      = null;
		
	String languageCode = null;
	
	private boolean isTeacher= false;
	private boolean isParent = false;
	private boolean isStudent= false;
	
	private String schoolIdFromFind   = null;
	private String schoolNameFromFind = null;
	
	ProgressDialog progressDialog 	= null;
	private String state = "";
	private boolean isTEmpPlayerExist = false;
	private String tempPlayerUserName = "";
	private final String TAG = this.getClass().getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registeration_step2);
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside onCreate()");
		
		this.setWidgetsReferences();
		this.getValuesFromStep1();
		this.setWidgetsTextValues();
		
		TempPlayerOperation tempPlayerOperationObj = new TempPlayerOperation(this);
		if(tempPlayerOperationObj.isTemparyPlayerExist())
		{
			if(!tempPlayerOperationObj.isTempPlayerDeleted())
			{
				isTEmpPlayerExist = true;
				TempPlayerOperation tempPlayerOperationObj1 = new TempPlayerOperation(this);
				ArrayList<TempPlayer> tempPlayer = tempPlayerOperationObj1.getTempPlayerData();
				/*tempPlayerOperationObj1.closeConn();//changes*/
				this.getCountries(this,tempPlayer.get(0).getCountry());
				state = tempPlayer.get(0).getState();
				edtCity.setText(tempPlayer.get(0).getCity());
				edtZipCode.setText(tempPlayer.get(0).getZipCode());
			}
			else
			{
				this.getCountries(this,"United States");
			}
		}
		else
		{
			tempPlayerOperationObj.createTempPlayerTable();
			tempPlayerOperationObj.closeConn();
			this.getCountries(this,"United States");
		}
		
		this.getLanguages();
		this.setListenerOnWidgets();
				
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside onCreate()");
	}

	/**
	 * This method get the data from the intent which is set in the step 1
	 */
	private void getValuesFromStep1() 
	{
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside getValuesFromStep1()");
		
		firstName 	= this.getIntent().getStringExtra("fName");
		lastName  	= this.getIntent().getStringExtra("lName");
		email  		= this.getIntent().getStringExtra("email");
		pass  		= this.getIntent().getStringExtra("pass");
		isTeacher 	= this.getIntent().getBooleanExtra("isTeacher", false);
		isParent 	= this.getIntent().getBooleanExtra("isParent",  false);
		isStudent   = this.getIntent().getBooleanExtra("isStudent", false);
			
		if(isTeacher)
		{
			pickerTitleSchool.setVisibility(TextView.VISIBLE);
			edtSchool.setVisibility(Spinner.VISIBLE);
		}
		else
		{
			pickerTitleSchool.setVisibility(TextView.GONE);
			edtSchool.setVisibility(Spinner.GONE);
		}
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside getValuesFromStep1()");
	}

	/**
	 * this method set the widgets references from the layout to the widgets objects
	 */
	private void setWidgetsReferences() 
	{
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside setWidgetsReferences()");
		
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		regTitleRegistration 	= (TextView) findViewById(R.id.regTitleRegistration);
		lblPleaseEnterYourLocationToSaveYourTimeAndPoints = (TextView) findViewById(
				R.id.lblPleaseEnterYourLocationToSaveYourTimeAndPoints);
		lblRegCountry 			= (TextView) findViewById(R.id.lblRegCountry);
		lblRegState 			= (TextView) findViewById(R.id.lblRegState);
		lblRegCity 				= (TextView) findViewById(R.id.lblRegCity);
		lblRegZip 				= (TextView) findViewById(R.id.lblRegZip);
		lblRegLanguage 			= (TextView) findViewById(R.id.lblRegLanguage);
		lblRegVolume 			= (TextView) findViewById(R.id.lblRegVolume);
		
		btnTitleSave 			= (Button) findViewById(R.id.btnTitleSave);
		seekbarVolume 			= (SeekBar) findViewById(R.id.seekbarVolume);
		edtCity 				= (EditText) findViewById(R.id.edtCity);
		edtZipCode 				= (EditText) findViewById(R.id.edtZip);
		spinnerCountry 			= (Spinner) findViewById(R.id.spinnerCountry);
		spinnerState 			= (Spinner) findViewById(R.id.spinnerState);
		spinnerLanguage 		= (Spinner) findViewById(R.id.spinnerLanguage);
		
		pickerTitleSchool       = (TextView) findViewById(R.id.lblRegSchool);
		edtSchool 			= (EditText) findViewById(R.id.spinnerSchool);
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}
	
	/**
	 * This method set the widgets text values from the Translation
	 */
	private void setWidgetsTextValues() 
	{	
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside setWidgetsTextValues()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(MathFriendzyHelper.getAppName(transeletion));
		regTitleRegistration.setText(transeletion.getTranselationTextByTextIdentifier("regTitleRegistration"));
		lblPleaseEnterYourLocationToSaveYourTimeAndPoints.setText(transeletion.getTranselationTextByTextIdentifier
				("lblPleaseEnterYourLocationToSaveYourTimeAndPoints"));
		lblRegCountry.setText(transeletion.getTranselationTextByTextIdentifier("lblRegCountry") + ": " );
		lblRegState.setText(transeletion.getTranselationTextByTextIdentifier("lblRegState") + ": ");
		lblRegCity.setText(transeletion.getTranselationTextByTextIdentifier("lblRegCity") + ": " );
		lblRegZip.setText(transeletion.getTranselationTextByTextIdentifier("lblRegZip") + ": " );
		lblRegLanguage.setText(transeletion.getTranselationTextByTextIdentifier("lblRegLanguage") + ": " );
		lblRegVolume.setText(transeletion.getTranselationTextByTextIdentifier("lblRegVolume") + ": " );
		btnTitleSave.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSave"));
		pickerTitleSchool.setText(transeletion.getTranselationTextByTextIdentifier("lblRegSchool"));
		transeletion.closeConnection();
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside setWidgetsTextValues()");
	}
	
	/**
	 * This methos get languages from the database
	 */
	private void getLanguages() 
	{
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside getLanguages()");
		
		Language languageObj = new Language(this);
		languageList = languageObj.getLanguages();
		this.setLanguageAdapter();
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside getLanguages()");
	}
	
	/**
	 * This method set the languages to the spinner
	 */
	private void setLanguageAdapter() 
	{
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside setLanguageAdapter()");
		
		ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,languageList);
		languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerLanguage.setAdapter(languageAdapter);
		spinnerLanguage.setSelection(languageAdapter.getPosition("English"));
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside setLanguageAdapter()");
	}

	/**
	 * This method set the listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		spinnerCountry.setOnItemSelectedListener(new OnItemSelectedListener() 
		{
			@Override
			public void onItemSelected(AdapterView<?> arg0, View view,int arg2, long arg3) 
			{				
				if(spinnerCountry.getSelectedItem().toString().equals("United States"))
				{
					spinnerState.setVisibility(Spinner.VISIBLE);
					lblRegState.setVisibility(TextView.VISIBLE);
					setStates("United States" , state);
					lblRegZip.setTextColor(Color.BLACK);
					edtZipCode.setEnabled(true);
				}
				else if(spinnerCountry.getSelectedItem().toString().equals("Canada"))
				{
					spinnerState.setVisibility(Spinner.VISIBLE);
					lblRegState.setVisibility(TextView.VISIBLE);
					setStates("Canada" , state);
					lblRegZip.setTextColor(Color.BLACK);
					edtZipCode.setEnabled(true);
				}
				else
				{
					spinnerState.setVisibility(Spinner.GONE);
					lblRegState.setVisibility(TextView.GONE);
					edtZipCode.setText("");
					lblRegZip.setTextColor(Color.GRAY);
					edtZipCode.setEnabled(false);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) 
			{				
			}
		});
		
				
		edtSchool.setOnClickListener(this);//changes
		btnTitleSave.setOnClickListener(this);
	}
	
	@Override
	protected void onRestart() 
	{
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside onRestart()");
		
		this.setTextFromFacebook();
		/*if(IS_REGISTER_SCHOOL)
		{
			if(schoolName != null)
			{
				IS_REGISTER_SCHOOL = false;
				schoolNameList.add(schoolName);
				schoolAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, schoolNameList);
				schoolAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinnerSchool.setAdapter(schoolAdapter);
				spinnerSchool.setSelection(schoolAdapter.getPosition(schoolName));
			}
		}*/
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside onRestart()");
		
		super.onRestart();
	}
	
	/**
	 * This method set the states when user select country as United State or Canada
	 * @param countryName
	 * @param stateName
	 */
	private void setStates(String countryName,String stateName)
	{
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside setStates()");
		
		ArrayList<String> satateList = new ArrayList<String>();
		States stateObj = new States();
		if(countryName.equals("United States"))
			satateList = stateObj.getUSStates(this);
		else if(countryName.equals("Canada"))
			satateList = stateObj.getCanadaStates(this);
		
		ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,satateList);
		stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerState.setAdapter(stateAdapter);
		spinnerState.setSelection(stateAdapter.getPosition(stateName));
		stateAdapter.notifyDataSetChanged();
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside setStates()");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnTitleSave :
			this.checkEmptyValidateField();
			break;
			
		case R.id.spinnerSchool:
			this.setListenerOnEdtSchool();
			break;
		}
	}
	
	/**
	 * This methos call when user click on the school Edit text
	 */
	private void setListenerOnEdtSchool()
	{
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside setListenerOnEdtSchool()");
		
		Intent schoolIntent = new Intent(this,SearchYourSchoolActivity.class);
		
		if(spinnerCountry.getSelectedItem().toString().equals("United States") || 
				spinnerCountry.getSelectedItem().toString().equals("Canada"))
		{	
			if(edtCity.getText().toString().equals("") ||  edtZipCode.getText().toString().equals(""))
			{
				DialogGenerator dg1 = new DialogGenerator(this);
				Translation transeletion1 = new Translation(this);
				transeletion1.openConnection();
				dg1.generateWarningDialog(transeletion1.getTranselationTextByTextIdentifier("alertMsgEnterYourLocationToContinue"));
				transeletion1.closeConnection();
			}
			else
			{
				schoolIntent.putExtra("country", spinnerCountry.getSelectedItem().toString());
				schoolIntent.putExtra("state", spinnerState.getSelectedItem().toString());
				schoolIntent.putExtra("city", edtCity.getText().toString());
				schoolIntent.putExtra("zip", edtZipCode.getText().toString());
				
				startActivityForResult(schoolIntent, 1);
			}
		}
		else
		{	
			schoolIntent.putExtra("country", spinnerCountry.getSelectedItem().toString());
			schoolIntent.putExtra("state", "");
			schoolIntent.putExtra("city", edtCity.getText().toString());
			schoolIntent.putExtra("zip", edtZipCode.getText().toString());
			
			startActivityForResult(schoolIntent, 1);
		}
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside setListenerOnEdtSchool()");
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside onActivityResult()");
		
		if (requestCode == 1) 
		{
		     if(resultCode == RESULT_OK)
		     {      
		         ArrayList<String> schoolInfo = data.getStringArrayListExtra("schoolInfo");
		         edtSchool.setText(schoolInfo.get(0));
		         schoolNameFromFind = schoolInfo.get(0);
		         schoolIdFromFind   = schoolInfo.get(1);
		     }
		    
		 }
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside onActivityResult()");
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	 * This method check for empty validation before submiting information on server
	 */
	private void checkEmptyValidateField() 
	{
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside checkEmptyValidateField()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		DialogGenerator dg = new DialogGenerator(this);
		
		if(spinnerCountry.getSelectedItem().toString().equals("United States") || spinnerCountry.getSelectedItem().toString().equals("Canada"))
		{
			if(edtCity.getText().toString().equals("") ||  edtZipCode.getText().toString().equals(""))
			{
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
			}
			else
			{
				this.registerUser();
			}
		}
		else
		{
			if(edtCity.getText().toString().equals("") /*||  edtZip.getText().toString().equals("")*/)
			{
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
			}
			else
			{
				this.registerUser();
			}
		}
		transeletion.closeConnection();
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside checkEmptyValidateField()");
	}

	/**
	 * This method registered user on server
	 */
	private void registerUser()
	{	
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside registerUser()");
		
		Country countryObj = new Country();
		countryId = countryObj.getCountryIdByCountryName(spinnerCountry.getSelectedItem().toString(), this);
		countryIso = countryObj.getCountryIsoByCountryName(spinnerCountry.getSelectedItem().toString(), this);
		
		if(spinnerState.getSelectedItem() != null)
		{
			States stateobj = new States();
			if(spinnerCountry.getSelectedItem().toString().equals("United States"))
			{
				stateId = stateobj.getUSStateIdByName(spinnerState.getSelectedItem().toString(), this);
				stateCode = stateobj.getStateCodeNameByStateNameFromUS(spinnerState.getSelectedItem().toString(), this);
			}
			else if(spinnerCountry.getSelectedItem().toString().equals("Canada"))
			{
				stateId = stateobj.getCanadaStateIdByName(spinnerState.getSelectedItem().toString(), this);
				stateCode = stateobj.getStateCodeNameByStateNameFromCanada(spinnerState.getSelectedItem().toString(), this);
			}
		}
		else
		{
			stateId = "0";
			stateCode = "";
		}
		
		Language languageObj = new Language(this);
		languageId = languageObj.getLanguageIdByName(spinnerLanguage.getSelectedItem().toString());
		languageCode = languageObj.getLanguageCodeByName(spinnerLanguage.getSelectedItem().toString());
		
		/*schoolId      		= "0";*/
		if(isParent)
		{
			isParentValue 	   = "1";
			schoolIdFromFind   = "0";
			schoolNameFromFind = "";
			
		}
		else if(isStudent)
		{
			isParentValue 		= "2";
			schoolIdFromFind	= "0";
			schoolNameFromFind 	= "";
		}
		else if(isTeacher)
		{			
			isParentValue 	= "0";
		}
		
		volume = seekbarVolume.getProgress()+"";
		coins  = "0";
		city   = edtCity.getText().toString();
		zip    = edtZipCode.getText().toString();
		
		
		players = "<AllPlayers></AllPlayers>";
		
		TempPlayerOperation tempObj = new TempPlayerOperation(this);
		if(tempObj.isTempPlayerDeleted())
		{
			players="<AllPlayers></AllPlayers>";
		}
		else
		{			
			TempPlayerOperation tempObj1 = new TempPlayerOperation(this);
			ArrayList<TempPlayer> tempPlayerObj = tempObj1.getTempPlayerData();
			
			tempPlayerUserName = tempPlayerObj.get(0).getUserName();
			
			players =  "<AllPlayers>"+this.getXmlPlayer(tempPlayerObj) + "</AllPlayers>"; 
		}
		
		RegistereUserDto regObj = new RegistereUserDto();
		regObj.setFirstName(firstName);
		regObj.setLastName(lastName);
		regObj.setEmail(email);
		regObj.setPass(pass);
		regObj.setCountryId(countryId);
		regObj.setCountryIso(countryIso);
		regObj.setStateId(stateId);
		regObj.setStateCode(stateCode);
		regObj.setCity(city);
		regObj.setPreferedLanguageId(languageId);
		regObj.setSchoolId(schoolIdFromFind);
		regObj.setSchoolName(schoolNameFromFind);
		regObj.setIsParent(isParentValue);
		regObj.setIsStudent(isStudentValue);
		regObj.setIsTeacher(isTeacherValue);
		regObj.setVolume(volume);
		regObj.setZip(zip);
		regObj.setPlayers(players);
		regObj.setCoins(coins);
		
		//changes for Internet Connection
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new RegisteredAsynTask(regObj).execute(null,null,null);
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside registerUser()");
	}
	
	/**
	 * This method return the xml format of the temp player data
	 * @param tempPlayerObj
	 * @return
	 */
	private String getXmlPlayer(ArrayList<TempPlayer> tempPlayerObj)
	{
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "inside getXmlPlayer()");
		
		//String xml = "";
		StringBuilder xml = new StringBuilder("");
		//Log.e(TAG, "tempplayerData size " + tempPlayerObj.size());
		
		for( int i = 0 ;  i < tempPlayerObj.size() ; i++)
		{		 
			PlayerTotalPointsObj playerObj = null;
			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(tempPlayerObj.get(i).getPlayerId() + "");
			learningCenterimpl.closeConn();
			
			xml.append("<player>" +
		  			"<playerId>"+tempPlayerObj.get(i).getPlayerId()+"</playerId>"+
				    "<fName>"+tempPlayerObj.get(i).getFirstName()+"</fName>"+
		  			"<lName>"+tempPlayerObj.get(i).getLastName()+"</lName>"+
		 			"<grade>"+tempPlayerObj.get(i).getGrade()+"</grade>"+
		  			"<schoolId>"+tempPlayerObj.get(i).getSchoolId()+"</schoolId>"+
		 			"<teacherId>"+tempPlayerObj.get(i).getTeacherUserId()+"</teacherId>"+
		  			"<indexOfAppearance>-1</indexOfAppearance>"+
		 			"<profileImageId>"+tempPlayerObj.get(i).getProfileImageName()+"</profileImageId>"+
		  			"<coins>"+playerObj.getCoins()+"</coins>"+
		 			"<points>"+playerObj.getTotalPoints()+"</points>"+
		  			"<userName>"+tempPlayerObj.get(i).getUserName()+"</userName>"+
		 			"<competeLevel>"+playerObj.getCompleteLevel()+"</competeLevel>"+
		  			"</player>");
		}
		
		if(REGISTRATION_STEP_2_FALG)
			Log.e(TAG, "outside getXmlPlayer()");
		
		return xml.toString();
	}
	
	
	/**
	 * This method call after succesfull registration for notification
	 */
	private void addUserOnServerWithAndroidDevice()
	{
		UserRegistrationOperation userObj = new UserRegistrationOperation(this);
		new AddUserWithAndroidDevice(userObj.getUserId() , this).execute(null,null,null);
	}
	
	/**
	 * This AsynckTask Registered user on server
	 * @author Yashwant Singh
	 *
	 */
	class RegisteredAsynTask extends AsyncTask<Void, Void, Void>
	{
		private RegistereUserDto regObj = null;
		private int registreationResult = 0;
		/*ProgressDialog progressDialog 	= null;*/
		
		RegisteredAsynTask(RegistereUserDto regObj)
		{
			this.regObj = regObj;
		}
		
		@Override
		protected void onPreExecute() 
		{
			if(REGISTRATION_STEP_2_FALG)
				Log.e(TAG, "inside RegisteredAsynTask onPreExecute()");
			
			progressDialog = CommonUtils.getProgressDialog(RegisterationStep2.this);
			progressDialog.show();
			
			if(REGISTRATION_STEP_2_FALG)
				Log.e(TAG, "outside RegisteredAsynTask onPreExecute()");
			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			if(REGISTRATION_STEP_2_FALG)
				Log.e(TAG, "inside RegisteredAsynTask doInBackground()");
			
			Register registerObj = new Register(RegisterationStep2.this);
			registreationResult = registerObj.registerUserOnserver(regObj);
				
				if(REGISTRATION_STEP_2_FALG)
					Log.e(TAG, "outside RegisteredAsynTask doInBackground()");
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) 
		{
			if(REGISTRATION_STEP_2_FALG)
				Log.e(TAG, "inside RegisteredAsynTask onPostExecute()");
			
			/*progressDialog.cancel();*/
			
			Translation transeletion = new Translation(RegisterationStep2.this);
			transeletion.openConnection();
			DialogGenerator dg = new DialogGenerator(RegisterationStep2.this);
			
			if(registreationResult == Register.SUCCESS)
			{
				//lblTheLastStepOfRegistrationIsToConfirm
				
				SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0); 
				SharedPreferences.Editor editor = sheredPreference.edit();
				editor.putBoolean(IS_LOGIN, true);
				editor.commit();
					
				//add user on server for notification
				addUserOnServerWithAndroidDevice();
				
				//update userid and playerid from the local databse table
				UserPlayerOperation userOpr = new UserPlayerOperation(RegisterationStep2.this);
				String playerId = userOpr.getPlayerIdbyUserName(tempPlayerUserName);
				String userId   = userOpr.getUserIdbyUserName(tempPlayerUserName);
				userOpr.closeConn();
				
				LearningCenterimpl learnignCenter = new LearningCenterimpl(RegisterationStep2.this);
				learnignCenter.openConn();
				learnignCenter.updatePlayerTotalPointsForUserIdandPlayerId(userId, playerId);
				learnignCenter.updatePlayerEquationTabelForUserIdAndPlayerId(userId, playerId);
				learnignCenter.updateMathResultForUserIdAndPlayerId(userId, playerId);
				learnignCenter.closeConn();
						
				//end update
				
				//changes for avtar
				ChooseAvtarOpration opr = new ChooseAvtarOpration();
				opr.openConn(RegisterationStep2.this);
				opr.updateplayerAvtarStatusForTempPlayer(userId, playerId);
				ArrayList<String> avtarList = opr.getAvtarIds(userId, playerId);
				opr.closeConn();
								
				if(avtarList.size() > 0)
				{
					if(CommonUtils.isInternetConnectionAvailable(RegisterationStep2.this))
					{
						new SaveAvtar(userId , playerId , avtarList).execute(null,null,null);
					}
				}
				//end changes for avtar
				
				//changes for purchased item
				LearningCenterimpl lrarningImpl = new LearningCenterimpl(RegisterationStep2.this);
				lrarningImpl.openConn();
				lrarningImpl.updatePurchasedItemTable(userId);
				ArrayList<String> purchaseItemList = lrarningImpl.getPurchaseItemIdsByUserId(userId);
				lrarningImpl.closeConn();	
				
				if(purchaseItemList.size() > 0)
				{
					if(CommonUtils.isInternetConnectionAvailable(RegisterationStep2.this))
					{
						new SavePurchasedItems(userId, purchaseItemList).execute(null,null,null);
					}
				}
				//end
				
				if(CommonUtils.isInternetConnectionAvailable(RegisterationStep2.this))
				{
					new GetTranselationFromServer().execute(null,null,null);
				}
				else
				{		
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				}
				
				/*Intent intent = new Intent(RegisterationStep2.this,MainActivity.class);
				startActivity(intent);*/
			}
			else if(registreationResult == Register.INVALID_CITY)
			{
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidCity"));
				progressDialog.cancel();
			}
			else if(registreationResult == Register.INVALID_ZIP)
			{
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidZipCode"));
				progressDialog.cancel();
			}
			
			transeletion.closeConnection();
			
			if(REGISTRATION_STEP_2_FALG)
				Log.e(TAG, "outside RegisteredAsynTask onPostExecute()");
			
			super.onPostExecute(result);
		}
	}
	
	/**
	 * This AsynckTask load all the data from the server based on the user selected language
	 * @author Yashwant Singh
	 *
	 */
	class GetTranselationFromServer extends AsyncTask<Void, Void, Void>
	{	
		@Override
		protected Void doInBackground(Void... params) 
		{
			Translation traneselation = new Translation(RegisterationStep2.this);
			CommonUtils.LANGUAGE_CODE = languageCode;
			CommonUtils.LANGUAGE_ID   = languageId;
			traneselation.getTransalateTextFromServer(languageCode, languageId, CommonUtils.APP_ID);
			traneselation.saveTranslationText();
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) 
		{
			//progressDialog.cancel();
			
			LearningCenterimpl learningCenterObj = new LearningCenterimpl(RegisterationStep2.this);
			learningCenterObj.openConn();
			ArrayList<MathResultTransferObj> mathResultList = learningCenterObj.getMathResultData();
			learningCenterObj.closeConn();
			
			if(mathResultList.size() > 0 )
			{				
				if(CommonUtils.isInternetConnectionAvailable(RegisterationStep2.this))
				{
					new SaveTempPlayerScoreOnServer(mathResultList).execute(null,null,null);
				}
				else
				{	
					progressDialog.cancel();
					Translation transeletion = new Translation(RegisterationStep2.this);
					transeletion.openConnection();
					DialogGenerator dg = new DialogGenerator(RegisterationStep2.this);
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();
				}
			
			}
			else
			{
				progressDialog.cancel();				
			
			if(isTEmpPlayerExist)
			{
				
				UserPlayerOperation userPlayerObj = new UserPlayerOperation(RegisterationStep2.this);
				ArrayList<UserPlayerDto> userPlayerList = userPlayerObj.getUserPlayerData();
				
				SharedPreferences sheredPreferences = getSharedPreferences(IS_CHECKED_PREFF, 0);
				SharedPreferences.Editor editor     = sheredPreferences.edit();
				editor.clear();
				editor.putString(PLAYER_ID, userPlayerList.get(0).getPlayerid());
				editor.commit();
				
				Intent intent = new Intent(RegisterationStep2.this,EditRegisteredUserPlayer.class);
				if(email.equals(""))//do not show registration pop up for student
				      intent.putExtra("isCallFromRegistration", false);
				     else
				      intent.putExtra("isCallFromRegistration", true);
				intent.putExtra("playerId", userPlayerList.get(0).getPlayerid());
				intent.putExtra("userId", userPlayerList.get(0).getParentUserId());
				intent.putExtra("imageName" , userPlayerList.get(0).getImageName());
				intent.putExtra("callingActivity", "RegisterationStep2");
				startActivity(intent);
			}
			else
			{
				Intent intent = new Intent(RegisterationStep2.this,MainActivity.class);
				if(email.equals(""))//do not show registration pop up for student
				      intent.putExtra("isCallFromRegistration", false);
				     else
				      intent.putExtra("isCallFromRegistration", true);
				startActivity(intent);
			}
			}
			super.onPostExecute(result);
		}
	}
	
	
	/**
	 * Save Temp Player result on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveTempPlayerScoreOnServer extends AsyncTask<Void, Void, Void>
	{

		private ArrayList<MathResultTransferObj> mathobj = null;
		private String playerId = null;
		private String UserId   = null;
		
		SaveTempPlayerScoreOnServer(ArrayList<MathResultTransferObj>  mathObj)
		{
			this.mathobj = mathObj;
		}
		
		@Override
		protected void onPreExecute() 
		{
			UserPlayerOperation userPlayerObj = new UserPlayerOperation(RegisterationStep2.this);
			playerId = userPlayerObj.getPlayerIdbyUserName(tempPlayerUserName);
			userPlayerObj.closeConn();
			
			UserRegistrationOperation userOperation = new UserRegistrationOperation(RegisterationStep2.this);
			UserId = userOperation.getUserData().getUserId();
			userOperation.closeConn();
			
			for( int i = 0 ; i < mathobj.size() ; i ++ )
			{
				mathobj.get(i).setPlayerId(playerId);
				mathobj.get(i).setUserId(UserId);
			}
			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(RegisterationStep2.this);
			register.savePlayerScoreOnServer(mathobj);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) 
		{
			super.onPostExecute(result);
			
			progressDialog.cancel();
			
			LearningCenterimpl learnignCenter = new LearningCenterimpl(RegisterationStep2.this);
			learnignCenter.openConn();
			learnignCenter.deleteFromMathResult();
			learnignCenter.closeConn();
						
			LearningCenterimpl learnignCenter1 = new LearningCenterimpl(RegisterationStep2.this);
			learnignCenter1.openConn();
			learnignCenter1.deleteFromMathResult();
			ArrayList<PlayerEquationLevelObj> playerquationData = learnignCenter1.getPlayerEquationLevelDataByPlayerId(playerId);
			learnignCenter1.closeConn();
			
			
			if(playerquationData.size() > 0 )
			{
				new  AddLevelAsynTask(playerquationData).execute(null,null,null);
			}
			else
			{
				if(isTEmpPlayerExist)
				{				
					UserPlayerOperation userPlayerObj = new UserPlayerOperation(RegisterationStep2.this);
					ArrayList<UserPlayerDto> userPlayerList = userPlayerObj.getUserPlayerData();
					
					SharedPreferences sheredPreferences = getSharedPreferences(IS_CHECKED_PREFF, 0);
					SharedPreferences.Editor editor     = sheredPreferences.edit();
					editor.clear();
					editor.putString(PLAYER_ID, userPlayerList.get(0).getPlayerid());
					editor.commit();
					
					Intent intent = new Intent(RegisterationStep2.this,EditRegisteredUserPlayer.class);
					if(email.equals(""))//do not show registration pop up for student
					      intent.putExtra("isCallFromRegistration", false);
					     else
					      intent.putExtra("isCallFromRegistration", true);
					intent.putExtra("playerId", userPlayerList.get(0).getPlayerid());
					intent.putExtra("userId", userPlayerList.get(0).getParentUserId());
					intent.putExtra("imageName" , userPlayerList.get(0).getImageName());
					intent.putExtra("callingActivity", "RegisterationStep2");
					startActivity(intent);
				}
				else
				{
					Intent intent = new Intent(RegisterationStep2.this,MainActivity.class);
					if(email.equals(""))//do not show registration pop up for student
					      intent.putExtra("isCallFromRegistration", false);
					     else
					      intent.putExtra("isCallFromRegistration", true);
					startActivity(intent);
				}
			}
		}
	}
	
	/**
	 * Update Player level data
	 * @author Yashwant Singh
	 *
	 */
	private class AddLevelAsynTask extends AsyncTask<Void, Void, Void>
	{
		private ArrayList<PlayerEquationLevelObj> playerquationData = null;
		
		AddLevelAsynTask(ArrayList<PlayerEquationLevelObj> playerquationData)
		{
			this.playerquationData = playerquationData;
		}
		
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation serverOpr = new LearningCenteServerOperation();
			serverOpr.addAllLevel("<levels>" + getLevelXml(playerquationData) + "</levels>");	
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) 
		{			
			if(isTEmpPlayerExist)
			{				
				UserPlayerOperation userPlayerObj = new UserPlayerOperation(RegisterationStep2.this);
				ArrayList<UserPlayerDto> userPlayerList = userPlayerObj.getUserPlayerData();
				
				SharedPreferences sheredPreferences = getSharedPreferences(IS_CHECKED_PREFF, 0);
				SharedPreferences.Editor editor     = sheredPreferences.edit();
				editor.clear();
				editor.putString(PLAYER_ID, userPlayerList.get(0).getPlayerid());
				editor.commit();
				
				Intent intent = new Intent(RegisterationStep2.this,EditRegisteredUserPlayer.class);
				if(email.equals(""))//do not show registration pop up for student
				      intent.putExtra("isCallFromRegistration", false);
				     else
				      intent.putExtra("isCallFromRegistration", true);
				intent.putExtra("playerId", userPlayerList.get(0).getPlayerid());
				intent.putExtra("userId", userPlayerList.get(0).getParentUserId());
				intent.putExtra("imageName" , userPlayerList.get(0).getImageName());
				intent.putExtra("callingActivity", "RegisterationStep2");
				startActivity(intent);
			}
			else
			{
				Intent intent = new Intent(RegisterationStep2.this,MainActivity.class);
				if(email.equals(""))//do not show registration pop up for student
				      intent.putExtra("isCallFromRegistration", false);
				     else
				      intent.putExtra("isCallFromRegistration", true);
				startActivity(intent);
			}
			super.onPostExecute(result);
		}
	}
	
	private String getLevelXml(ArrayList<PlayerEquationLevelObj> playerquationData)
	{
		StringBuilder xml = new StringBuilder("");
		
		for( int i = 0 ; i < playerquationData.size() ; i ++ )
		{
			xml.append("<userLevel>" +
				    "<uid>"+playerquationData.get(i).getUserId()+"</uid>"+
		  			"<pid>"+playerquationData.get(i).getPlayerId()+"</pid>"+
		 			"<eqnId>"+playerquationData.get(i).getEquationType()+"</eqnId>"+
		  			"<level>"+playerquationData.get(i).getLevel()+"</level>"+
		 			"<stars>"+playerquationData.get(i).getStars()+"</stars>"+
		  			"</userLevel>");
		}
		
		return xml.toString();
	}
	
	/**
	 * This class save temp player avtar on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveAvtar extends AsyncTask<Void, Void, Void>
	{
		private String userId;
		private String playerId;
		private String avtarIds;
		
		SaveAvtar(String userId , String playerId , ArrayList<String> avtarIdList)
		{
			this.userId = userId;
			this.playerId = playerId;
			
			avtarIds = "";
			
			for(int i = 0 ; i < avtarIdList.size() ; i ++ )
			{
				avtarIds = avtarIds + avtarIdList.get(i);
				if(i < avtarIdList.size() - 1)
					avtarIds = avtarIds + ",";
			}
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			AvtarServerOperation serverObj = new AvtarServerOperation();
			serverObj.saveAvtar(userId, playerId, avtarIds);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	}
	
	/**
	 * This method save the temp player purchased items on server
	 * @author Yashwant Singh
	 *
	 */
	class SavePurchasedItems extends AsyncTask<Void, Void, Void>
	{		
		private String userId;
		private ArrayList<String> purchaseItemList;
		private String itemsId;
		
		SavePurchasedItems(String userId , ArrayList<String> purchasedItemList)
		{
			this.userId = userId;
			this.purchaseItemList = purchasedItemList;
			
			itemsId = "";
			for(int i = 0 ; i < purchasedItemList.size() ; i ++ )
			{
				itemsId = itemsId + purchasedItemList.get(i);
				if(i < purchasedItemList.size() - 1)
					itemsId = itemsId + ",";
			}
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
	
		@Override
		protected Void doInBackground(Void... params) {
			Login login = new Login(RegisterationStep2.this);
			login.savePurchasedItem(userId, itemsId);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	}
}
