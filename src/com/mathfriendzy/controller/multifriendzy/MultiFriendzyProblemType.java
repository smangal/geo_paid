package com.mathfriendzy.controller.multifriendzy;

import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_BG_INFO;
import static com.mathfriendzy.utils.ICommonUtils.LEARNING_CENTER_MAIN_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ITextIds.LBL_GEOGRAPHY;
import static com.mathfriendzy.utils.ITextIds.LBL_LANGUAGE;
import static com.mathfriendzy.utils.ITextIds.LBL_MATH;
import static com.mathfriendzy.utils.ITextIds.LBL_MEASUREMENT;
import static com.mathfriendzy.utils.ITextIds.LBL_MONEY;
import static com.mathfriendzy.utils.ITextIds.LBL_PHONICS;
import static com.mathfriendzy.utils.ITextIds.LBL_READING;
import static com.mathfriendzy.utils.ITextIds.LBL_SCIENCE;
import static com.mathfriendzy.utils.ITextIds.LBL_SOCIAL;
import static com.mathfriendzy.utils.ITextIds.LBL_VOCABULARY;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.geographyfriendzypaid.R;
import com.mathfriendzy.controller.learningcenter.CategoriesEfficientAdapter;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.utils.DialogGenerator;
/**
 * This class set problem type with its difficulty
 * @author Yashwant Singh
 *
 */
public class MultiFriendzyProblemType extends AdBaseActivity
{
	private TextView txtLearningCenterTop 				= null;
	private TextView chooseCategory         			= null;
	private ListView lstLearningCenterCategories 		= null;
	private int selectedPossition                   	= 0;

	private String alertForUnlock						= null;
	private PlayerEquationLevelObj playerEquationObj	= null;

	private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList1 = null;

	private final String TAG = this.getClass().getSimpleName();

	private ArrayList<LearningCenterTransferObj> laernignCenterFunctionsList = null;

	SharedPreferences sharedPreferences = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learning_center_main);

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside onCreate()");

		this.setWidgetsReferences();
		this.setWidgetsTextValues();
		this.getFunctionalList();
		this.setListenerOnWidgets();

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside onCreate()");
	}

	/**
	 * This method set the widgets references from the layout to the widgets Objects 
	 */
	private void setWidgetsReferences()
	{
		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside setWidgetsReferences()");

		txtLearningCenterTop    = (TextView) findViewById(R.id.learningCenterTop);
		chooseCategory          = (TextView) findViewById(R.id.labelChooseCategory);
		lstLearningCenterCategories = (ListView) findViewById(R.id.listLearningCenter);

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside setWidgetsReferences()");
	}

	/**
	 * This method set the textValues from the etranslation
	 */
	private void setWidgetsTextValues()
	{
		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside setWidgetsReferences()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		txtLearningCenterTop.setText(transeletion.getTranselationTextByTextIdentifier("lblChooseProblemType"));
		chooseCategory.setText(transeletion.getTranselationTextByTextIdentifier("mfLblSelectEquationsType") + ".");

		alertForUnlock = transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustCompletePreviousRank");
		transeletion.closeConnection();

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside setWidgetsReferences()");
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside setListenerOnWidgets()");	

		lstLearningCenterCategories.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,long id) 
			{				
				sharedPreferences = getSharedPreferences(LEARNING_CENTER_BG_INFO,0);

				SharedPreferences.Editor editor = sharedPreferences.edit();
				editor.clear();
				editor.putString("bgImageName", laernignCenterFunctionsList.get(position)
						.getLearningCenterOperation());
				editor.putString("topBarText", laernignCenterFunctionsList1.get(position)
						.getLearningCenterOperation());
				editor.putString("topBarSign", laernignCenterFunctionsList.get(position)
						.getLearningCenterOperation().toLowerCase().replace(" ","_")+"_sign");
				editor.putInt("operationId", laernignCenterFunctionsList.get(position)
						.getLearningCenterMathOperationId());
				editor.commit();

				selectedPossition = position;
				
				int rank_id = laernignCenterFunctionsList.get(selectedPossition).getLearningCenterMathOperationId();
				if(isUnlock(rank_id - 1) || (rank_id == 1))
				{
					operationSelectShowActivity();
				}
				else
				{
					DialogGenerator dg = new DialogGenerator(MultiFriendzyProblemType.this);
					dg.generateWarningDialog(alertForUnlock);			
				}

			}
		});

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside setListenerOnWidgets()");
	}


	private void operationSelectShowActivity()
	{
		Intent intent = new Intent(this, MultiFriendzyEquationActivity.class);
		int rank_id = laernignCenterFunctionsList.get(selectedPossition).getLearningCenterMathOperationId();
		intent.putExtra("operationId", rank_id);
		startActivity(intent);

	}//END operationSelectShowActivity method



	/**
	 * used to check is previous rank is completed or not
	 * @param eqaution_type takes id for rank
	 * @return
	 */
	boolean isUnlock(int eqaution_type)
	{
		boolean isUnlock = false;

		playerEquationObj.setEquationType(eqaution_type);
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();

		if(learningCenterObj.getHighetsLevel(playerEquationObj) == 10)
		{
			isUnlock = true;
		}
		learningCenterObj.closeConn();

		return isUnlock;
	}


	/**
	 * This method get function categories list from the database and set it to the list view
	 */
	private void getFunctionalList() 
	{
		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " inside getFunctionalList()");	

		laernignCenterFunctionsList = new ArrayList<LearningCenterTransferObj>();
		laernignCenterFunctionsList1 = new ArrayList<LearningCenterTransferObj>();
		LearningCenterimpl laerningCenter = new LearningCenterimpl(this);
		laerningCenter.openConn();
		laernignCenterFunctionsList = laerningCenter.getLearningCenterFunctions();
		laerningCenter.closeConn();

		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		for(int i = 0; i < laernignCenterFunctionsList.size() ; i ++ )
		{
			LearningCenterTransferObj lc = new LearningCenterTransferObj();

			if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Beginner"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_READING));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Novice"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_MATH));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Rookie"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_LANGUAGE));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Intermediate"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_VOCABULARY));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Professional"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_MONEY));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Whiz"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_PHONICS));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Expert"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_MEASUREMENT));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Master"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_GEOGRAPHY));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Prodigy"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_SOCIAL));
			else if(laernignCenterFunctionsList.get(i).getLearningCenterOperation().contains("Brainiac"))
				lc.setLearningCenterOperation(transeletion.getTranselationTextByTextIdentifier(LBL_SCIENCE));

			laernignCenterFunctionsList1.add(lc);
		}

		transeletion.closeConnection();
		/**
		 * For checking is level completed or not till 10th level
		 */
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		playerEquationObj = new PlayerEquationLevelObj();

		playerEquationObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));
		playerEquationObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));

		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		boolean[] isCheck = new boolean[10];
		isCheck = learningCenterObj.getHighestLevelForCheck(playerEquationObj);
		learningCenterObj.closeConn();
		isCheck[0] = true;

		//Do true check for multifriendzy by default
		isCheck[0] = true;
		//set function list to the adapter
		CategoriesEfficientAdapter adapter = new CategoriesEfficientAdapter(this, R.layout.list_choose_categories, 
				laernignCenterFunctionsList, laernignCenterFunctionsList1, isCheck);

		lstLearningCenterCategories.setAdapter(adapter);

		if(LEARNING_CENTER_MAIN_FLAG)
			Log.e(TAG, " outside getFunctionalList()");	
	}

}