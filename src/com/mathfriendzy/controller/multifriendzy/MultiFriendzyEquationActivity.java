package com.mathfriendzy.controller.multifriendzy;

import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geographyfriendzypaid.R;
import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.multifriendzy.findbyuser.SelectedPlayerActivity;
import com.mathfriendzy.gcm.ProcessNotification;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.MathScoreTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.SeeAnswerTransferObj;
import com.mathfriendzy.model.learningcenter.triviaEquation.TriviaQuestion;
import com.mathfriendzy.model.multifriendzy.CreateMultiFriendzyDto;
import com.mathfriendzy.model.multifriendzy.MathFriendzysRoundDTO;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.player.base.Player;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.singleFriendzy.EquationFromServerTransferObj;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.notification.GetAndroidDevicePidForUser;
import com.mathfriendzy.notification.SendNotificationTransferObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ICommonUtils;
import com.mathfriendzy.utils.ITextIds;
import com.mathfriendzy.utils.PlaySound;

public class MultiFriendzyEquationActivity extends AdBaseActivity implements OnClickListener
{
	private final String TAG = this.getClass().getSimpleName();

	private TextView txtPlayer1Name							= null;
	private TextView txtPlayer2Name							= null;
	private TextView txtPlayer1Points						= null;
	private TextView txtPlayer2Points						= null;
	private TextView txtQueNo								= null;
	private TextView txtQuestion							= null;
	private TextView txtOption1								= null;
	private TextView txtOption2								= null;
	private TextView txtOption3								= null;
	private TextView txtOption4								= null;
	private TextView mfTitleHomeScreen						= null;
	private TextView txtTime								= null;

	private LinearLayout progressBarLayout1					= null;
	private LinearLayout progressBarLayout2					= null;
	private RelativeLayout layoutReady						= null;
	private RelativeLayout layoutShow						= null;

	private RelativeLayout progressLayout					= null;
	private RelativeLayout ansLayout1						= null;
	private RelativeLayout ansLayout2						= null;
	private RelativeLayout ansLayout3						= null;
	private RelativeLayout ansLayout4						= null;
	private ImageView imgFlagPlayer1						= null;
	private ImageView imgFlagPlayer2						= null;
	private ImageView imgProgressBar						= null;
	private ImageView getReady								= null;

	private ArrayList<String> optionArray					= null;
	private LearningCenterTransferObj questionObj 			= null;

	private int questionNumber								= -1;
	private int id											= -1;
	private final int MAX_QUESTION							= 10;
	private CountDownTimer getReadyTimer					= null;
	private CountDownTimer timer							= null;
	private long START_TIME									= 2*60*1000;
	private final long INTERVAL								= 1*1000;
	private static long playingTime							= 0;
	private String strPoint									= null;

	//Defining two integer variable for storing play start_time and end_time
	private int start_time									= 0;
	private int end_time									= 0;
	private static boolean timeFlag							= false;
	private long DISPLAY_TIME 								= 0;
	//Max and mininmum points for a correct answer
	private final int MIN_SCORE								= 225;
	private final int MAX_SCORE								= 250;
	private boolean flag_right								= false;

	private int pointsPlayer1 								= 0;
	private int pointsPlayer2 								= 0;
	private int pointsForEachQuestion 						= 0;

	private int numberOfCoins     							= 0;


	private float coinsPerPoint   							= 0.05f;

	private static boolean isClickOnBackPressed 			= true;
	public static SeeAnswerTransferObj seeAnswerDataObj 	= null;
	private String userAnswer 								= "";
	private int isCorrectAnsByUser 							= 0;

	private Date startTime 									= null;
	private Date endTime   									= null;

	private boolean isStopSecondPlayerProgress 				= false;

	private final long START_TIME_FOR_GET_READY 			= 7 * 1000;
	//	private final int EQUAITON_TYPE 						= 11;
	private int equationId 									= 0;
	private int isWin			 							= 0;
	private int operationId     					        = 0;
	private PlaySound	playSound							= null;
	private String gameType									= "Play";

	private Player  opponentData							= null;
	private String friendzyId							 	= "-1";
	private boolean isEquationFromserver 					= false;
	private int turn       									= 0;
	private String winnerId									= "-1";
	private int roundScore 									= 0;
	private int shouldSaveEquation 							= 0;
	private String problems 								= "";
	private String playFriendzyDate 						= null;

	private final String THEIR_FRENDZY_TEXT 				= "their friendzy";
	private final String YOUR_FRIENDZY_TEXT 				= "your friendzy";
	//private final String HISTORY_TEXT       				= "";
	private final String MESSAGE_TEXT_FOR_NOTIFICATION 		= " has sent a "+ITextIds.LBL+" Friendzy to ";
	private boolean isNotificationSend						= false;

	private ArrayList<Integer> equationList	  = null;
	ArrayList<MathFriendzysRoundDTO> updatedRoundList			  = null;

	private int index_challenger							= 0;
	public static int completeLevel 						= 0;//level user play at current which is set in Single FriendzyMain

	private ArrayList<Integer> equationsIdList 									= null;
	private ArrayList<EquationFromServerTransferObj> equListForSecondPlayer 	= null;//hold the equation data from server
	private ArrayList<LearningCenterTransferObj> playEquationList 				= null;
	private ArrayList<String> userAnswerList 									= null;
	private ArrayList<MathScoreTransferObj> playMathlist 						= null;
	private boolean isTabSmall  												= false;


	//Change for time issue
	private final int EXTRA_PLAY_TIME						= 30;
	private final int MIN_PLAY_TIME							= 90;
	private boolean isClick									= false;
	private Date holdingTime								= null;
	private int totalTime									= 0;
	//ENd Change

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multi_friendzy_equation);		

		operationId	 = getIntent().getIntExtra("operationId", 0);
		isTabSmall	 = MainActivity.isTab;

		if(!CommonUtils.isInternetConnectionAvailable(this))
		{
			showDialogForNet();
		}
		else
		{
			userAnswerList   	= new ArrayList<String>();		
			seeAnswerDataObj 	= new SeeAnswerTransferObj();
			equationList		= new ArrayList<Integer>();
			playMathlist      	= new ArrayList<MathScoreTransferObj>();
			seeAnswerDataObj 	= new SeeAnswerTransferObj();
			userAnswerList		= new ArrayList<String>();
			playEquationList	= new ArrayList<LearningCenterTransferObj>();
			questionObj			= new LearningCenterTransferObj();

			isClickOnBackPressed	= true;
			timeFlag				= false;

			getWidgetId();		
			setPlayerDetail();
			getIntentValue();
			setWidgetListener();
			setWidgetText();		
			setPoints();

			startTime	= new Date();
			endTime		= new Date();
			if(ProcessNotification.isFromNotification)
			{
				ProcessNotification.isFromNotification = false;

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder
				.setTitle(ITextIds.LBL+" Friendzy")
				.setMessage(ProcessNotification.sentNotificationData.getShareMessage())
				.setIcon(R.drawable.ic_launcher)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog, int which) 
					{	
						getReadyTimer();
					}
				}).show();
			}
			else
			{
				this.getReadyTimer();
			}
		}
	}


	protected void getWidgetId()
	{
		mfTitleHomeScreen	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		txtOption1			= (TextView) findViewById(R.id.txtOption1);
		txtOption2			= (TextView) findViewById(R.id.txtOption2);
		txtOption3			= (TextView) findViewById(R.id.txtOption3);
		txtOption4			= (TextView) findViewById(R.id.txtOption4);
		txtQuestion			= (TextView) findViewById(R.id.txtQue);
		txtQueNo			= (TextView) findViewById(R.id.txtQueNo);
		txtTime				= (TextView) findViewById(R.id.txtTime);

		txtPlayer1Name		= (TextView) findViewById(R.id.txtPlayer1Name);
		txtPlayer1Points	= (TextView) findViewById(R.id.txtPlayer1Points);
		imgFlagPlayer1		= (ImageView) findViewById(R.id.imgFlagPlayer1);
		txtPlayer2Name		= (TextView) findViewById(R.id.txtPlayer2Name);
		txtPlayer2Points	= (TextView) findViewById(R.id.txtPlayer2Points);
		imgFlagPlayer2		= (ImageView) findViewById(R.id.imgFlagPlayer2);

		progressBarLayout1	= (LinearLayout) findViewById(R.id.progressBarLayout1);
		progressBarLayout2	= (LinearLayout) findViewById(R.id.progressBarLayout2);
		ansLayout1			= (RelativeLayout) findViewById(R.id.ansLayout1);
		ansLayout2			= (RelativeLayout) findViewById(R.id.ansLayout2);
		ansLayout3			= (RelativeLayout) findViewById(R.id.ansLayout3);
		ansLayout4			= (RelativeLayout) findViewById(R.id.ansLayout4);		
		progressLayout		= (RelativeLayout) findViewById(R.id.progressLayout);

		layoutReady			= (RelativeLayout) findViewById(R.id.layoutReady);
		layoutShow			= (RelativeLayout) findViewById(R.id.layoutShow);
		getReady			= (ImageView) findViewById(R.id.getReady);
	}//END getWidgetId method



	private void getIntentValue() 
	{

		if(MultiFriendzyMain.isNewFriendzyStart)
		{
			//Log.e("", "isNewFriendzyStart");
			opponentData = SelectedPlayerActivity.player;

			/*Log.e(TAG, "notification from userv seacrhing ipod" + opponentData.getIponPids() 
						+ " android pids " + opponentData.getAndroidPids());*/

			friendzyId = "-1";

			isNotificationSend = true;
			this.getEquation();
			progressLayout.setVisibility(View.GONE);
		}
		else
		{				
			friendzyId = MultiFriendzyRound.friendzyId;

			/*Log.e(TAG, "notification from userv seacrhing ipod" + MultiFriendzyRound.multiFriendzyServerDto.getNotificationDevices() 
					+ " android pids " + MultiFriendzyRound.multiFriendzyServerDto.getAndroidPids());*/

			int index = MultiFriendzyRound.roundList.size() - 1 ;
			if(MultiFriendzyRound.roundList.get(index).getPlayerScore().length() > 0
					&& MultiFriendzyRound.roundList.get(index).getOppScore().length() > 0)
			{
				//Log.e("", "getEquation");
				isNotificationSend = true;
				this.getEquation();
				progressLayout.setVisibility(View.GONE);
			}
			else
			{
				if(MultiFriendzyRound.roundList.get(index).getPlayerScore().length() == 0 )
				{
					if(CommonUtils.isInternetConnectionAvailable(this))
					{
						//Log.e("", "isEquationFromserver");
						isNotificationSend = false;
						isEquationFromserver = true;
						this.getEquationFromServer();
					}
					else
					{
						CommonUtils.showInternetDialog(this);
					}

				}

			}
		}
	}	


	/**
	 * This method get equations from server
	 */
	private void getEquationFromServer()
	{		
		String userId 		= MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getParentUserId();
		String playerId 	= MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getPlayerId();
		String friendzyId 	= MultiFriendzyRound.friendzyId;

		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new FindMultiFriendzyEquationsForPlayer(userId, playerId, friendzyId).execute(null,null,null);
		}
		else
		{
			if(getReadyTimer != null)
				getReadyTimer.cancel();
			CommonUtils.showInternetDialog(this);
		}
	}

	/**
	 * use to setListener for widget
	 */
	private void setWidgetListener()
	{
		ansLayout1.setOnClickListener(this);
		ansLayout2.setOnClickListener(this);
		ansLayout3.setOnClickListener(this);
		ansLayout4.setOnClickListener(this);

	}//END setWidgetListner method

	/**
	 * disable listener when all questions completed.
	 */
	private void visibleOptions() 
	{
		ansLayout1.setVisibility(View.VISIBLE);
		ansLayout2.setVisibility(View.VISIBLE);
		ansLayout3.setVisibility(View.VISIBLE);
		ansLayout4.setVisibility(View.VISIBLE);
	}//END disableListner method


	private void setOptionBackground()
	{
		if(MainActivity.isTab)
		{
			ansLayout1.setBackgroundResource(R.drawable.option1_ipad);
			ansLayout2.setBackgroundResource(R.drawable.option2_ipad);
			ansLayout3.setBackgroundResource(R.drawable.option3_ipad);
			ansLayout4.setBackgroundResource(R.drawable.option4_ipad);
		}		
		else
		{
			ansLayout1.setBackgroundResource(R.drawable.option1);
			ansLayout2.setBackgroundResource(R.drawable.option2);
			ansLayout3.setBackgroundResource(R.drawable.option3);
			ansLayout4.setBackgroundResource(R.drawable.option4);
		}

	}//END setOptionsBackground method


	private void setClickckableTrue()
	{
		ansLayout1.setEnabled(true);
		ansLayout2.setEnabled(true);
		ansLayout3.setEnabled(true);
		ansLayout4.setEnabled(true);
	}

	private void setClickckableFalse()
	{
		ansLayout1.setEnabled(false);
		ansLayout2.setEnabled(false);
		ansLayout3.setEnabled(false);
		ansLayout4.setEnabled(false);
	}


	/**
	 * use to set Qustion on Screen
	 */
	private void setNextQuestion() 
	{		
		isClick = false;
		startTime = new Date();

		visibleOptions();
		setOptionBackground();
		setClickckableTrue();

		questionNumber++;
		questionObj = playEquationList.get(questionNumber);		
		equationId = questionObj.getEquationId();
		setOptionArray(questionObj);
		setQuestion();	
	}


	/**
	 * This method set the second player progress
	 */
	private void setSecondPlayerProgress()
	{	
		if(equListForSecondPlayer.get(0).getTimeTakenToAnswer() > 0 )
		{				
			setsecondPlayerDataProgress(0, equListForSecondPlayer.get(0).getTimeTakenToAnswer());
		}		


	}


	/**
	 * This method set the second player points data
	 * @param index
	 * @param timeTakenToAnswer
	 */
	private void setsecondPlayerDataProgress(final int index , double timeTakenToAnswer)
	{	
		long sleepTime = (long) (timeTakenToAnswer * 1000);

		Handler handlerSetResult = new Handler();
		handlerSetResult.postDelayed(new Runnable() 
		{
			@Override
			public void run() 
			{			
				imgProgressBar = new ImageView(getApplicationContext());

				if(equListForSecondPlayer.get(index).getIsAnswerCorrect() == 0)
				{
					if(MainActivity.isTab)
					{
						imgProgressBar.setBackgroundResource(R.drawable.red_ipad);
					}
					else
					{
						imgProgressBar.setBackgroundResource(R.drawable.red);					
					}
				}
				else
				{
					pointsPlayer2 = pointsPlayer2 + equListForSecondPlayer.get(index).getPoints();

					txtPlayer2Points.setText(strPoint + " : " + pointsPlayer2);
					if(MainActivity.isTab)
					{
						imgProgressBar.setBackgroundResource(R.drawable.green_ipad);
					}
					else
					{
						imgProgressBar.setBackgroundResource(R.drawable.green);					
					}
					//playsound.playSoundForRightForSingleFriendzyForChallenger(SingleFriendzyEquationSolve.this);
				}
				progressBarLayout2.addView(imgProgressBar);	

				equationIndex(index);

			}
		}, sleepTime);

	}


	/**
	 * Get the data from the equation list for setting progress according to i index 
	 * @param index
	 */
	private void equationIndex(int index)
	{
		index_challenger = index + 1;

		if(index >= MAX_QUESTION - 1)
		{
			isStopSecondPlayerProgress = true;
			//setAnswerForAnswerScreen();
		}
		if(!isStopSecondPlayerProgress)
		{			
			setsecondPlayerDataProgress(index_challenger, equListForSecondPlayer
					.get(index_challenger).getTimeTakenToAnswer());
		}


	}



	/**
	 * Use to set options in a string array
	 * @param categories 
	 */
	private void setOptionArray(LearningCenterTransferObj categories) 
	{
		optionArray	= new ArrayList<String>();
		optionArray.add(categories.getAnswer());
		optionArray.add(categories.getOption1());
		optionArray.add(categories.getOption2());
		optionArray.add(categories.getOption3());		
	}//END setOptionArray method



	/**
	 * use to set Text on UI
	 * @param questionObj 
	 */
	private void setWidgetText() 
	{
		Translation translate = new Translation(this);
		translate.openConnection();	
		strPoint = (translate.getTranselationTextByTextIdentifier("lblPts"));
		if(operationId > 0)
			mfTitleHomeScreen.setText(translate.getTranselationTextByTextIdentifier
					(CommonUtils.setLearningTitle(operationId)));
		else
		{
			mfTitleHomeScreen.setText(MathFriendzyHelper.getAppName(translate));
		}

		txtPlayer2Points.setText(strPoint+" : "+CommonUtils.setNumberString(pointsPlayer2+""));
		translate.closeConnection();

	}//END setWidgetId method



	private void setQuestion()
	{		
		int que = questionNumber + 1;
		txtQueNo.setText(""+que);
		txtQuestion.setText(questionObj.getQuestion());
		id = new Random().nextInt(optionArray.size());
		txtOption1.setText(optionArray.get(id));
		optionArray.remove(id);

		id = new Random().nextInt(optionArray.size());
		txtOption2.setText(optionArray.get(id));
		optionArray.remove(id);

		id = new Random().nextInt(optionArray.size());
		txtOption3.setText(optionArray.get(id));
		optionArray.remove(id);

		id = new Random().nextInt(optionArray.size());
		txtOption4.setText(optionArray.get(id));
		optionArray.remove(id);
	}

	private void setPoints()
	{
		pointsPlayer1 = getPlayer1ScoreForEachQuestion();

		txtPlayer1Points.setText(strPoint+" : "+CommonUtils.setNumberString(pointsPlayer1+""));		
		start_time = (int) (new Date().getTime()/1000);
	}

	private int getPlayer1ScoreForEachQuestion() 
	{
		end_time = (int) (new Date().getTime()/1000);

		if(start_time == 0)
		{
			return 0;
		}
		else
		{			
			pointsForEachQuestion = MAX_SCORE - (5 * (end_time - start_time));
			pointsPlayer1 = pointsPlayer1 + pointsForEachQuestion;

			if(pointsForEachQuestion < MIN_SCORE)
			{				
				pointsPlayer1 = pointsPlayer1 + MIN_SCORE - pointsForEachQuestion;
				pointsForEachQuestion = MIN_SCORE;
			}

			return pointsPlayer1;	
		}

	}

	private void getEquation()
	{
		TriviaQuestion db = new TriviaQuestion(this);
		playEquationList = db.getQuestionList(0, operationId);
		for(int index = 0; index < MAX_QUESTION ; index++)
		{
			equationList.add(playEquationList.get(index).getEquationId());			
		} 
	}


	/**
	 * This method change the image on each seconds for get ready
	 */
	private void getReadyTimer()
	{
		getReadyTimer = new CountDownTimer(START_TIME_FOR_GET_READY, 1) 
		{

			@Override
			public void onTick(long millisUntilFinished) 
			{
				int value = (int) ((millisUntilFinished/1000) % 60) ;
				setBacKGroundForGetReadyImage(value);
			}

			@Override
			public void onFinish() 
			{				
				if(isClickOnBackPressed)
				{
					layoutReady.setVisibility(View.GONE);
					layoutShow.setVisibility(View.VISIBLE);

					timer = new MyCountDownTimer(START_TIME, INTERVAL);
					timer.start();


					if(equListForSecondPlayer != null)
						setSecondPlayerProgress();

					playSound	 = new PlaySound(); 
					playSound.playSoundForBackground(MultiFriendzyEquationActivity.this);
					if(playEquationList.size() != 0)
					{
						setNextQuestion();
					}
					else
					{
						getEquation();
						setNextQuestion();
					}					
				}
			}

		}.start();
	}

	/**
	 * This method set the Get Ready Image back ground
	 * @param value
	 */
	private void setBacKGroundForGetReadyImage(int value)
	{
		if(MainActivity.isTab)
		{
			switch(value)
			{
			case 6 : 
				getReady.setBackgroundResource(R.drawable.mcg_get_ready_ipad);
				break;
			case 5 : 
				getReady.setBackgroundResource(R.drawable.mcg_5_ipad);
				break;
			case 4 : 
				getReady.setBackgroundResource(R.drawable.mcg_4_ipad);
				break;
			case 3 : 
				getReady.setBackgroundResource(R.drawable.mcg_3_ipad);
				break;
			case 2 : 
				getReady.setBackgroundResource(R.drawable.mcg_2_ipad);
				break;
			case 1 : 
				getReady.setBackgroundResource(R.drawable.mcg_1_ipad);
				break;
			case 0 : 
				getReady.setBackgroundResource(R.drawable.mcg_go_ipad);
				break;
			}
		}
		else
		{
			switch(value)
			{
			case 6 : 
				getReady.setBackgroundResource(R.drawable.mcg_get_ready);
				break;
			case 5 : 
				getReady.setBackgroundResource(R.drawable.mcg_5);
				break;
			case 4 : 
				getReady.setBackgroundResource(R.drawable.mcg_4);
				break;
			case 3 : 
				getReady.setBackgroundResource(R.drawable.mcg_3);
				break;
			case 2 : 
				getReady.setBackgroundResource(R.drawable.mcg_2);
				break;
			case 1 : 
				getReady.setBackgroundResource(R.drawable.mcg_1);
				break;
			case 0 : 
				getReady.setBackgroundResource(R.drawable.mcg_go);
				break;
			}
		}

	}



	/**
	 * timer class for playing game
	 * @author Shilpi Mangal
	 *
	 */
	private class MyCountDownTimer extends CountDownTimer
	{
		public MyCountDownTimer(long startTime, long interval)
		{
			super(startTime, interval);
		}

		@Override
		public void onFinish() 
		{
			isStopSecondPlayerProgress = true;
			setAnswerForAnswerScreen();
			//isClickOnBackPressed = false;
			//callOnBackPressed();			
		}

		@Override
		public void onTick(long millisUntilFinished) 
		{
			playingTime	= millisUntilFinished/1000;
			txtTime.setText(DateTimeOperation.setTimeInMinAndSec(playingTime));	
		}
	}//END MyCountDownTimer class


	@SuppressWarnings("deprecation")
	private void setPlayerDetail() 
	{		

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));

		txtPlayer1Name.setText(playerName+".");

		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);

		try 
		{
			if(!(coutryIso.equals("-")))
				imgFlagPlayer1.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.countryImageFolder) +"/" 
						+ coutryIso + ".png"), null));
		} 
		catch (IOException e) 
		{
			Log.e("MultiFriendzy", "Inside set player detail Error No Image Found");

		}

		//set second user detail
		if(MultiFriendzyRound.multiFriendzyServerDto != null)
		{
			txtPlayer2Name.setText(MultiFriendzyRound.oppenentPlayerName+".");

			try 
			{
				if(!(coutryIso.equals("-")))
					imgFlagPlayer2.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
							getString(R.string.countryImageFolder) +"/" 
							+ MultiFriendzyRound.multiFriendzyServerDto.getCountryCode() + ".png"), null));
			} 
			catch (IOException e) 
			{	
				Log.e(TAG, "Inside set player detail Error : No Image Found");
				//e.printStackTrace();
			}//End try catch block
		}


	}




	@Override
	public void onClick(View v) 
	{		
		isClick = true;

		playSound.playSoundForSelectingAnswer(this);
		setClickckableFalse();
		String layoutText	= null;
		imgProgressBar = new ImageView(this);
		ansLayout1.setVisibility(View.INVISIBLE);
		ansLayout2.setVisibility(View.INVISIBLE);
		ansLayout3.setVisibility(View.INVISIBLE);
		ansLayout4.setVisibility(View.INVISIBLE);
		v.setVisibility(View.VISIBLE);
		switch (v.getId()) 
		{
		case R.id.ansLayout1:
			if(isTabSmall)
			{
				ansLayout1.setBackgroundResource(R.drawable.option1right_ipad);		
			}
			else
			{
				ansLayout1.setBackgroundResource(R.drawable.option1right);		
			}			
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption1);
			break;

		case R.id.ansLayout2:
			if(isTabSmall)
			{
				ansLayout2.setBackgroundResource(R.drawable.option2right_ipad);
			}
			/*else if(isTabLarge)
			{
				ansLayout2.setBackgroundResource(R.drawable.option2right_ipad2);
			}*/
			else
			{
				ansLayout2.setBackgroundResource(R.drawable.option2right);
			}	
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption2);
			break;

		case R.id.ansLayout3:
			if(isTabSmall)
			{
				ansLayout3.setBackgroundResource(R.drawable.option3right_ipad);
			}
			/*else if(isTabLarge)
			{
				ansLayout3.setBackgroundResource(R.drawable.option3right_ipad2);
			}*/
			else
			{
				ansLayout3.setBackgroundResource(R.drawable.option3right);
			}	
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption3);
			break;

		case R.id.ansLayout4:
			if(isTabSmall)
			{
				ansLayout4.setBackgroundResource(R.drawable.option4right_ipad);
			}
			/*else if(isTabLarge)
			{
				ansLayout4.setBackgroundResource(R.drawable.option4right_ipad2);
			}*/
			else
			{
				ansLayout4.setBackgroundResource(R.drawable.option4right);
			}
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption4);
			break;		
		}

		checkAnswer(layoutText, v);		

	}



	/**
	 * use to check answer and update UI with new Question
	 * @param layoutText refer to selected text for answer
	 */
	private void checkAnswer(String layoutText, final View v) 
	{	
		userAnswer = layoutText;

		if(layoutText.equals(questionObj.getAnswer()))
		{
			if(isTabSmall)
			{
				imgProgressBar.setBackgroundResource(R.drawable.green_ipad);
			}
			else
			{
				imgProgressBar.setBackgroundResource(R.drawable.green);
			}
			flag_right = true;
			setPoints();
			DISPLAY_TIME = 500;
		}
		else 
		{
			DISPLAY_TIME = 1000;
			flag_right = false;
			if(isTabSmall)
			{
				imgProgressBar.setBackgroundResource(R.drawable.red_ipad);
			}
			/*else if(isTabLarge)
			{
				imgProgressBar.setBackgroundResource(R.drawable.red_ipad2);
			}*/
			else
			{
				imgProgressBar.setBackgroundResource(R.drawable.red);
			}
		}
		progressBarLayout1.addView(imgProgressBar);

		//playSound.playSoundForAnimation(getApplicationContext());
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() 
			{
				if(!flag_right)
				{
					playSound.playSoundForWrong(getApplicationContext());
					if(isTabSmall)
					{
						v.setBackgroundResource(R.drawable.wrong_ipad);
					}
					/*else if(isTabLarge)
					{
						imgProgressBar.setBackgroundResource(R.drawable.red_ipad2);
					}*/
					else
					{
						v.setBackgroundResource(R.drawable.wrong);
					}
					isCorrectAnsByUser = 0;
				}
				else
				{
					playSound.playSoundForRight(getApplicationContext());
					//rightAnsCounter ++ ;
					isCorrectAnsByUser = 1;
				}

				int right_bg_id;
				if(isTabSmall)
				{
					right_bg_id = R.drawable.right_ipad;
				}
				/*else if(isTabLarge)
				{
					imgProgressBar.setBackgroundResource(R.drawable.red_ipad2);
				}*/
				else
				{
					right_bg_id = R.drawable.right;
				}
				checkRightOption(right_bg_id);
			}

		}, DISPLAY_TIME);

		if(questionNumber == MAX_QUESTION-1)
		{
			playSound.playSoundForResultScreen(getApplicationContext());					
		}		

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() 
			{		

				if(questionNumber < MAX_QUESTION - 1)
				{		
					savePlayData();

					setNextQuestion();
				}
				else
				{
					isStopSecondPlayerProgress = true;
					setAnswerForAnswerScreen();					
				}				
			}

		}, 2500);

	}//END checkAnswer method


	/**
	 * use to check which layout contains the right answer
	 * @param backgroungId
	 */
	private void checkRightOption(int backgroungId)
	{
		if(MathFriendzyHelper.returnTextFromTextview(txtOption1).equals(questionObj.getAnswer()))
		{
			ansLayout1.setVisibility(View.VISIBLE);
			ansLayout1.setBackgroundResource(backgroungId);
		}
		else if(MathFriendzyHelper.returnTextFromTextview(txtOption2).equals(questionObj.getAnswer()))
		{
			ansLayout2.setVisibility(View.VISIBLE);
			ansLayout2.setBackgroundResource(backgroungId);
		}
		else if(MathFriendzyHelper.returnTextFromTextview(txtOption3).equals(questionObj.getAnswer()))
		{
			ansLayout3.setVisibility(View.VISIBLE);
			ansLayout3.setBackgroundResource(backgroungId);
		}
		else if(MathFriendzyHelper.returnTextFromTextview(txtOption4).equals(questionObj.getAnswer()))
		{
			ansLayout4.setVisibility(View.VISIBLE);
			ansLayout4.setBackgroundResource(backgroungId);
		}
	}//END checkRightOption method


	@Override
	protected void onStop() 
	{
		if(getReadyTimer != null)
			getReadyTimer.cancel();
		if(playSound != null)
			playSound.stopPlayer();
		if(timer != null)
			timer.cancel();
		super.onStop();
	}

	@Override
	protected void onPause() 
	{	
		if(getReadyTimer != null)
			getReadyTimer.cancel();
		if(playSound != null)
			playSound.stopPlayer();
		timeFlag = true;
		if(timer != null)
			timer.cancel();
		super.onPause();
	}

	@Override
	protected void onRestart() 
	{			
		playSound = new PlaySound();
		playSound.playSoundForBackground(this);
		if(timeFlag)
		{
			START_TIME = playingTime * 1000;
			timer = new MyCountDownTimer(START_TIME, INTERVAL);
			timer.start();			
		}
		super.onRestart();
	}


	@Override
	public void onBackPressed() 
	{		
		if(getReadyTimer != null)
			getReadyTimer.cancel();

		if(timer != null)
			timer.cancel();

		if(playSound != null)
			playSound.stopPlayer();

		isStopSecondPlayerProgress = true;//for stop the challenger player progresss

		isClickOnBackPressed = false;

		this.clickOnBackPressed();

		super.onBackPressed();
	}


	/**
	 * This method call when user click on back button
	 */
	private void clickOnBackPressed()
	{	
		savePlayData();

		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			if(!isClickOnBackPressed)
			{
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
				new AddCoinAndPointsForLoginUser(getPlayerPoints(savePlayDataToDataBase())).execute(null,null,null);
			}
			else
			{
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());
			}

		}
		else
		{
			this.inserPlayDataIntodatabase(savePlayDataToDataBase());

			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}



	/**
	 * This method save play data to the database(Math_Result table)
	 */
	private MathResultTransferObj savePlayDataToDataBase()
	{
		MathResultTransferObj mathResultObj = new MathResultTransferObj();
		mathResultObj.setRoundId(0);
		mathResultObj.setIsFirstRound("0");
		mathResultObj.setGameType(gameType);
		mathResultObj.setIsWin(isWin);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
			mathResultObj.setUserId("0");
		else
			mathResultObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));

		if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
			mathResultObj.setPlayerId("0");
		else
			mathResultObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));

		//mathResultObj.setChallengerId(challengerDataObj.getPlayerId());

		mathResultObj.setIsFakePlayer(0);
		mathResultObj.setProblems(this.getEquationSolveXml(playMathlist));

		mathResultObj.setTotalScore(pointsPlayer1);

		mathResultObj.setCoins(numberOfCoins);

		return mathResultObj;
	}

	/**
	 * This method convert the data for solving the equation into xml format
	 */
	private String getEquationSolveXml(ArrayList<MathScoreTransferObj> playMathlist)
	{		
		StringBuilder xml = new StringBuilder("");

		for( int i = 0 ;  i < playMathlist.size() ; i++)
		{
			xml.append("<equation>" +
					"<equationId>"+playMathlist.get(i).getEquationId()+"</equationId>"+
					"<start_date_time>"+playMathlist.get(i).getStartDateTimeStr()+"</start_date_time>"+
					"<end_date_time>"+playMathlist.get(i).getEndDateTimeStr()+"</end_date_time>"+					
					"<time_taken_to_answer>"+playMathlist.get(i).getTimeTakenToAnswer()+"</time_taken_to_answer>"+
					"<math_operation_id>"+playMathlist.get(i).getMathOperationID()+"</math_operation_id>"+
					"<points>"+playMathlist.get(i).getPoints()+"</points>"+
					"<isAnswerCorrect>" + playMathlist.get(i).getAnswerCorrect() + "</isAnswerCorrect>"+
					"<user_answer>"+playMathlist.get(i).getAnswer()+"</user_answer>"+
					"</equation>");
		}

		return xml.toString();
	}

	/**
	 * This methdod make xml for unsove eqaurions
	 * @return
	 */
	private String getEquationUnSolveXml()
	{
		StringBuilder xml = new StringBuilder("");

		SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
		singleFriendzy.openConn();

		for( int i = playMathlist.size() ;  i < equationList.size(); i++)
		{				
			xml.append("<equation>" +
					"<equationId>"+equationList.get(i)+"</equationId>"+
					"<start_date_time>"+CommonUtils.formateDate(endTime)+"</start_date_time>"+
					"<end_date_time>"+CommonUtils.formateDate(endTime)+"</end_date_time>"+
					"<time_taken_to_answer>"+ 0 +"</time_taken_to_answer>"+
					"<math_operation_id>"+ operationId +"</math_operation_id>"+
					"<points>"+ 0 +"</points>"+
					"<isAnswerCorrect>" + 0 + "</isAnswerCorrect>"+
					"<user_answer>"+" " +"</user_answer>"+
					"</equation>");
		}

		singleFriendzy.closeConn();
		Log.e("MultiFriendzy", ""+xml.toString());

		return xml.toString();
	}



	/**
	 * This method insert the data into total player points table
	 * @param mathResultObj
	 */
	private void insertIntoPlayerTotalPoints(MathResultTransferObj mathResultObj)
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		PlayerTotalPointsObj playerPoints = this.getPlayerPoints(mathResultObj);

		if(learningCenterObj.isPlayerTotalPointsExist(playerPoints.getPlayerId()))
		{
			int points = playerPoints.getTotalPoints();
			int coins  = playerPoints.getCoins();
			playerPoints.setTotalPoints(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getTotalPoints()
					+ points);

			playerPoints.setCoins(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getCoins()
					+ coins);

			learningCenterObj.deleteFromPlayerTotalPoints(playerPoints.getPlayerId());
		}

		learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterObj.closeConn();
	}

	/**
	 * This method return the player points with its user id and player id and also coins
	 * @return
	 */
	private PlayerTotalPointsObj getPlayerPoints(MathResultTransferObj mathResultObj)
	{
		PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
		playerPoints.setUserId(mathResultObj.getUserId());
		playerPoints.setPlayerId(mathResultObj.getPlayerId());

		playerPoints.setTotalPoints(pointsPlayer1);

		//numberOfCoins = (int) (points * 0.05);

		playerPoints.setCoins(numberOfCoins);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		playerPoints.setCompleteLevel(sharedPreffPlayerInfo.getInt("completeLevel", 0));//set complete level

		playerPoints.setPurchaseCoins(0);

		return playerPoints;
	}

	/**
	 * This method add the start and end time to solve equation and all the information related to the equation
	 * to the arrayList
	 */
	@SuppressWarnings("deprecation")
	private void savePlayData()
	{		

		if(endTime == null){
			holdingTime = startTime;			
		}else{
			holdingTime = endTime;
		}

		endTime	 = new Date();		

		MathScoreTransferObj mathObj = new MathScoreTransferObj();
		mathObj.setMathOperationID(operationId);
		mathObj.setEquationId(questionObj.getEquationId());

		mathObj.setStartDateTimeStr(CommonUtils.formateDate(startTime));
		mathObj.setEndDateTimeStr(CommonUtils.formateDate(endTime));
		mathObj.setPoints(pointsForEachQuestion);


		/*Change for save time extra after stop clicking on answer*/

		//Log.e("", "holdingTime"+holdingTime);
		//Log.e("", "holdingTime"+endTime);

		if(totalTime < MIN_PLAY_TIME && !isClick 
				&& DateTimeOperation.isValidForChange(holdingTime, endTime)){

			endTime = startTime;
			String time = DateTimeOperation.getEndTimeAfterAddedExtraTime(CommonUtils.formateDate(endTime),
					EXTRA_PLAY_TIME);
			mathObj.setEndDateTimeStr(time);

			//Log.e("time", ""+time);


		}//End change	

		int startTimeValue = startTime.getSeconds() ;
		int endTimeVlaue   = endTime.getSeconds();

		if( endTime.getSeconds() < startTime.getSeconds())
			endTimeVlaue = endTimeVlaue + 60;		

		totalTime = totalTime + (endTimeVlaue - startTimeValue);	

		if(userAnswer.length() > 0)
			mathObj.setAnswer(userAnswer);
		else
			mathObj.setAnswer(" ");

		mathObj.setAnswerCorrect(isCorrectAnsByUser);


		mathObj.setTimeTakenToAnswer(endTimeVlaue - startTimeValue);
		mathObj.setEquation(questionObj);

		pointsForEachQuestion = 0;
		playMathlist.add(mathObj);

		userAnswerList.add(userAnswer);
	}
	/*@SuppressWarnings("deprecation")
	private void savePlayData()
	{		 
		endTime = new Date();

		MathScoreTransferObj mathObj = new MathScoreTransferObj();
		mathObj.setMathOperationID(operationId);
		mathObj.setEquationId(equationId);
		mathObj.setStartDateTimeStr(CommonUtils.formateDate(startTime));
		mathObj.setEndDateTimeStr(CommonUtils.formateDate(endTime));
		mathObj.setPoints(pointsForEachQuestion);

		if(userAnswer.length() > 0)
			mathObj.setAnswer(userAnswer);
		else
			mathObj.setAnswer(" ");

		mathObj.setAnswerCorrect(isCorrectAnsByUser);

		int startTimeValue = startTime.getSeconds() ;
		int endTimeVlaue   = endTime.getSeconds();

		if( endTime.getSeconds() < startTime.getSeconds())
			endTimeVlaue = endTimeVlaue + 60;


//		totalTimeTaken = endTime.getSeconds() - startTime.getSeconds();
//		mathObj.setTimeTakenToAnswer(totalTimeTaken);
		mathObj.setTimeTakenToAnswer(endTimeVlaue - startTimeValue);

		mathObj.setEquation(questionObj);

		pointsForEachQuestion = 0;
		playMathlist.add(mathObj);

		userAnswerList.add(userAnswer);
	}*/



	/**
	 * This method store data into database for Math_result
	 * @param mathResultObj
	 */
	private void inserPlayDataIntodatabase(MathResultTransferObj mathResultObj )
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		learningCenterObj.insertIntoMathResult(mathResultObj);
		learningCenterObj.closeConn();
		this.insertIntoPlayerTotalPoints(mathResultObj);
	}


	/**
	 * use to setAnswer for next player
	 */
	private void setAnswerForAnswerScreen()
	{
		//Log.e("", "setAnswerForAnswer calling");

		savePlayData();

		if(timer != null)
			timer.cancel();
		txtTime.setBackgroundResource(0);		

		isStopSecondPlayerProgress = true;

		updatedRoundList = new ArrayList<MathFriendzysRoundDTO>();


		String opponentPlayerId = null;
		String opponentUserId   = null;

		if(opponentData != null)//data from searched user
		{
			opponentPlayerId = opponentData.getPlayerId() + "";
			opponentUserId   = opponentData.getParentUserId() + "";
		}
		else //  data from click on your turn
		{
			opponentPlayerId = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getPlayerId();
			opponentUserId   = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getParentUserId();
		}

		//if mathfriendzy start by my player
		if(MultiFriendzyMain.isNewFriendzyStart)
		{
			MathFriendzysRoundDTO roundDto = new MathFriendzysRoundDTO();
			roundDto.setPlayerScore(pointsPlayer1 + "");
			roundDto.setOppScore("");
			roundDto.setRoundNumber("1");

			updatedRoundList.add(roundDto);

			turn = 1 ; // for their turn
		}
		else
		{
			MathFriendzysRoundDTO roundDto = MultiFriendzyRound.roundList.get
					(MultiFriendzyRound.roundList.size() - 1);//get last round record

			if(roundDto.getPlayerScore().equals(""))
			{
				roundDto.setPlayerScore(pointsPlayer1 + "");

				for(int i = 0 ; i < MultiFriendzyRound.roundList.size() - 1 ; i ++ )
				{
					updatedRoundList.add(MultiFriendzyRound.roundList.get(i));
				}

				updatedRoundList.add(roundDto);
			}
			else
			{
				MathFriendzysRoundDTO roundDto1 = new MathFriendzysRoundDTO();
				roundDto1.setPlayerScore(pointsPlayer1 + "");
				roundDto1.setOppScore("");
				roundDto1.setRoundNumber((MultiFriendzyRound.roundList.size() + 1) + "");

				for(int i = 0 ; i < MultiFriendzyRound.roundList.size(); i ++ )
				{
					updatedRoundList.add(MultiFriendzyRound.roundList.get(i));
				}

				updatedRoundList.add(roundDto1);
			}

			/*for(int i = 0 ; i < MultiFriendzyRound.roundList.size() - 1 ; i ++ )
			{
				updatedRoundList.add(MultiFriendzyRound.roundList.get(i));
			}

			updatedRoundList.add(roundDto);*/
		}

		if(updatedRoundList.size() > 1)
		{
			MathFriendzysRoundDTO roundDto = updatedRoundList.get(updatedRoundList.size() - 1);

			if((!roundDto.getPlayerScore().equals("")) && (!roundDto.getOppScore().equals("")))
			{
				turn = 0 ;// for your turn
			} 
			else if((!roundDto.getPlayerScore().equals("")) && (roundDto.getOppScore().equals("")))
			{
				turn = 1 ;// for their turn
			}
		}

		if(MultiFriendzyMain.isClickOnYourTurn && isEquationFromserver)
		{
			//Log.e(TAG, "inside click on your turn for notification");

			if(updatedRoundList.size() == 3)
			{
				int playerTotalPoints   = 0;
				int opponentTotalPoints = 0;

				for ( int i = 0 ; i < updatedRoundList.size() ; i ++ )
				{
					if(!updatedRoundList.get(i).getPlayerScore().equals(""))
						playerTotalPoints = playerTotalPoints + Integer.parseInt(
								updatedRoundList.get(i).getPlayerScore());

					if(!updatedRoundList.get(i).getOppScore().equals(""))
						opponentTotalPoints = opponentTotalPoints + Integer.parseInt
						(updatedRoundList.get(i).getOppScore());
				}

				if(playerTotalPoints > opponentTotalPoints)
				{
					SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
					String playerId = sharedPreffPlayerInfo.getString("playerId", "");

					winnerId = playerId;
					isNotificationSend = true;
					turn = 2;//for history
				}	
				else if(playerTotalPoints == opponentTotalPoints)
				{
					winnerId = opponentPlayerId;
					isNotificationSend = true;
					turn = 2;//for history
				}
				else
				{
					pointsPlayer1 = pointsPlayer1 - 10;
					winnerId = opponentPlayerId;
					isNotificationSend = true;
					turn = 2;//for history
				}
			}

			if(playMathlist.size() > 0)
			{
				roundScore = pointsPlayer1;
				shouldSaveEquation = 0;
				problems = getEquationSolveXml(playMathlist);
			}
			else
			{
				roundScore = 0;
				shouldSaveEquation = 0;
			}
		}
		else
		{
			if(isEquationFromserver == false)
			{
				roundScore = pointsPlayer1;
				problems = getEquationSolveXml(playMathlist); 
				problems = problems +getEquationUnSolveXml();				

				shouldSaveEquation = 1;
			}
		}

		playFriendzyDate = CommonUtils.formateDateIn24Hours(new Date());
		//playFriendzyDate = CommonUtils.formateDate(new Date());

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String userId = sharedPreffPlayerInfo.getString("userId", "");
		String playerId = sharedPreffPlayerInfo.getString("playerId", "");

		updatePlayerTotalPointsTable(playerId , userId);

		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(MultiFriendzyEquationActivity.this);
		learningCenterimpl.openConn();
		PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playerId);
		learningCenterimpl.closeConn();

		CreateMultiFriendzyDto createMultiFrinedzyDto = new CreateMultiFriendzyDto();

		createMultiFrinedzyDto.setProblems(problems);
		createMultiFrinedzyDto.setFriendzyId(friendzyId);
		createMultiFrinedzyDto.setPlayerUserId(userId);
		createMultiFrinedzyDto.setPlayerId(playerId);

		createMultiFrinedzyDto.setOpponentPlayerId(opponentPlayerId);
		createMultiFrinedzyDto.setOpponentUserId(opponentUserId);

		createMultiFrinedzyDto.setRoundScore(roundScore);
		createMultiFrinedzyDto.setPoints(playerPoints.getTotalPoints());
		createMultiFrinedzyDto.setCoins(playerPoints.getCoins());
		createMultiFrinedzyDto.setDate(playFriendzyDate);

		boolean isAddParameter = false;//this is for add below parameter to the url or not for hit api

		if(!winnerId.equals("-1"))
		{
			isAddParameter = true;
			createMultiFrinedzyDto.setModifying(1);
			createMultiFrinedzyDto.setIsCompleted(1);
			createMultiFrinedzyDto.setWinner(winnerId);
		}
		else if(!friendzyId.equals("-1"))
		{
			isAddParameter = true;
			createMultiFrinedzyDto.setModifying(1);
			createMultiFrinedzyDto.setIsCompleted(0);
			createMultiFrinedzyDto.setWinner("-1");
		}

		createMultiFrinedzyDto.setShouldSaveEquations(shouldSaveEquation);

		MultiFriendzyMain.isNewFriendzyStart = false;

		savePlayDataOnServer(createMultiFrinedzyDto , isAddParameter);

	}


	/**
	 * Update player total points table
	 * @param playerId
	 */
	private void updatePlayerTotalPointsTable(String playerId , String userId)
	{
		numberOfCoins = (int) (pointsPlayer1 * coinsPerPoint);

		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(MultiFriendzyEquationActivity.this);
		learningCenterimpl.openConn();
		PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playerId);

		playerPoints.setTotalPoints(playerPoints.getTotalPoints() + pointsPlayer1);
		playerPoints.setCoins(playerPoints.getCoins() + numberOfCoins);
		playerPoints.setCompleteLevel(playerPoints.getCompleteLevel());
		playerPoints.setUserId(userId);
		playerPoints.setPlayerId(playerId);

		learningCenterimpl.deleteFromPlayerTotalPoints(playerId);
		learningCenterimpl.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterimpl.closeConn();
	}




	/**
	 * This method save play multifriendzy data on server
	 */
	private void savePlayDataOnServer(CreateMultiFriendzyDto createMultiFrinedzyDto , boolean isAddParameter)
	{
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new CreateMultiFriendzy(createMultiFrinedzyDto , isAddParameter).execute(null,null,null);
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
	}



	/**
	 * This class getEquation from server
	 * @author Yashwant Singh
	 *
	 */
	class FindMultiFriendzyEquationsForPlayer extends AsyncTask<Void, Void, ArrayList<EquationFromServerTransferObj>>
	{
		private String userId 	= null;
		private String playerId = null;
		private String friendzyId = null;

		FindMultiFriendzyEquationsForPlayer(String userId , String playerId , String friendzyId)
		{
			this.userId 	= userId;
			this.playerId 	= playerId;
			this.friendzyId = friendzyId;
		}

		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
		}

		@Override
		protected ArrayList<EquationFromServerTransferObj> doInBackground(Void... params) 
		{
			ArrayList<EquationFromServerTransferObj> equatiolListFromServer = new ArrayList<EquationFromServerTransferObj>();
			MultiFriendzyServerOperation serverObj = new MultiFriendzyServerOperation();
			equatiolListFromServer = serverObj.findMultiFriendzyEquatiosForPlayer(userId, playerId, friendzyId);

			return equatiolListFromServer;
		}

		@Override
		protected void onPostExecute(ArrayList<EquationFromServerTransferObj> result) 
		{			
			if(result != null)
			{
				equListForSecondPlayer = result;

				equationsIdList = new ArrayList<Integer>();

				for( int i = 0 ; i < equListForSecondPlayer.size() ; i ++ )
				{	
					equationsIdList.add(equListForSecondPlayer.get(i).getMathEquationId());					
				}
				operationId = equListForSecondPlayer.get(0).getOperationId();
				Translation translate = new Translation(getApplicationContext());
				translate.openConnection();
				mfTitleHomeScreen.setText(translate.getTranselationTextByTextIdentifier
						(CommonUtils.setLearningTitle(equListForSecondPlayer.get(0).getOperationId())));
				translate.closeConnection();

				TriviaQuestion db = new TriviaQuestion(getApplicationContext());
				db.openConn();
				playEquationList = db.getQuestionListForEquationId(equationsIdList);
				db.closeConn();
			}
			else if(playEquationList.size() == 0)
			{
				showDialogForNet();
			}
			else
			{				
				showDialogForNet();
			}

			super.onPostExecute(result);
		}
	}


	private void showDialogForNet()
	{
		if(getReadyTimer != null)
			getReadyTimer.cancel();

		if(timer != null)
			timer.cancel();

		if(playSound != null)
			playSound.stopPlayer();

		isStopSecondPlayerProgress = true;
		DialogGenerator dg = new DialogGenerator(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		dg.generateInternetWarningDialog(transeletion
				.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"), "Ok");
		//CommonUtils.showInternetDialog(this);
		transeletion.closeConnection();
	}

	/**
	 * This asyncTask Add points and coins on server
	 * @author Yashwant Singh
	 *
	 */
	class AddCoinAndPointsForLoginUser extends AsyncTask<Void, Void, Void> 
	{		
		private PlayerTotalPointsObj playerObj = null;

		AddCoinAndPointsForLoginUser(PlayerTotalPointsObj playerObj)
		{
			this.playerObj = playerObj;

			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(MultiFriendzyEquationActivity.this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerObj.getPlayerId());
			learningCenterimpl.closeConn();	

			this.playerObj.setTotalPoints(playerObj.getTotalPoints());
			this.playerObj.setCoins(playerObj.getCoins());
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(MultiFriendzyEquationActivity.this);
			register.addPointsAndCoinsOnServerForloginUser(playerObj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			super.onPostExecute(result);
		}
	}

	/**
	 * This class create Multifriedzy on server
	 * @author Yashwant Singh
	 *
	 */
	class CreateMultiFriendzy extends AsyncTask<Void, Void, Void>
	{
		private CreateMultiFriendzyDto createMultiFrinedzyDto = null;
		private boolean isAddParameter;
		private String frendzyId = null;
		private ProgressDialog pd = null;	

		public CreateMultiFriendzy(CreateMultiFriendzyDto createMultiFrinedzyDto, boolean isAddParameter) 
		{
			this.createMultiFrinedzyDto = createMultiFrinedzyDto;
			this.isAddParameter         = isAddParameter;
		}

		@Override
		protected void onPreExecute() 
		{
			pd = CommonUtils.getProgressDialog(MultiFriendzyEquationActivity.this);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			//Log.e("MultiFriendzyActivity", "Call of CreateMultiFriendzy");
			MultiFriendzyServerOperation multiFriendzyServerObj = new MultiFriendzyServerOperation();
			frendzyId = multiFriendzyServerObj.createMultiFriendzy(createMultiFrinedzyDto, isAddParameter);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			pd.cancel();

			//Log.e(TAG, "turn " + turn);

			if(frendzyId != null)
				createMultiFrinedzyDto.setFriendzyId(frendzyId);

			if(isNotificationSend)
			{
				//Log.e("", "IsNotification is true");
				sendNotification(createMultiFrinedzyDto);//send object for getting some data from it
			}

			MultiFriendzyRound.roundList = updatedRoundList;

			Translation transeletion = new Translation(MultiFriendzyEquationActivity.this);
			transeletion.openConnection();

			if(turn != 2)//not for history
			{
				if(turn == 1)//their turn
				{
					//Log.e("", "Their turn :"+turn);
					MultiFriendzyRound.turn = transeletion.getTranselationTextByTextIdentifier("lblTheirTurn"); 
					MultiFriendzyRound.type = THEIR_FRENDZY_TEXT;
				}
				else if(turn == 0)//your turn
				{
					//Log.e("", "your turn :"+turn);
					MultiFriendzyRound.turn = transeletion.getTranselationTextByTextIdentifier("lblYourTurn");
					MultiFriendzyRound.type = YOUR_FRIENDZY_TEXT;
				}
				transeletion.closeConnection();

				if(frendzyId != null)
					MultiFriendzyRound.friendzyId = frendzyId;

				startActivity(new Intent(MultiFriendzyEquationActivity.this , MultiFriendzyRound.class));
			}
			else
			{	
				String opponentName 			= null;
				String opponentProfileImageId   = null;

				if(opponentData != null)//data from searched user
				{
					String lastName = "";
					if(opponentData.getLastName().length() > 0)
						lastName = opponentData.getLastName().charAt(0) + "";

					opponentName = opponentData.getFirstName() + " " + lastName + ".";
					opponentProfileImageId   = opponentData.getProfileImageName();
				}
				else //  data from click on your turn
				{
					String lastName = "";
					if(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().length() > 0)
						lastName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().charAt(0) + "";

					opponentName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getfName() 
							+ " " + lastName + ".";
					opponentProfileImageId   = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData()
							.getProfileImageNameId();
				}

				MultiFriendzyWinnerScreen.oppenentPlayerName = opponentName;
				MultiFriendzyWinnerScreen.opponentImageId    = opponentProfileImageId;
				MultiFriendzyWinnerScreen.roundList          = updatedRoundList;

				int playerTotal 	= 0 ; 
				int opponentTotal 	= 0 ;

				for(int i = 0 ; i < MultiFriendzyWinnerScreen.roundList.size() ; i ++)
				{
					if(!MultiFriendzyWinnerScreen.roundList.get(i).getPlayerScore().equals(""))
					{
						playerTotal = playerTotal + Integer.parseInt(MultiFriendzyWinnerScreen.roundList.get(i).getPlayerScore());
					}

					if(!MultiFriendzyWinnerScreen.roundList.get(i).getOppScore().equals(""))
					{						
						opponentTotal = opponentTotal + Integer.parseInt(MultiFriendzyWinnerScreen.roundList.get(i)
								.getOppScore());
						//Log.e("", "Winner opponent player "+opponentTotal);
					}
				}

				if(playerTotal > opponentTotal)
				{
					//Log.e("", "Winner starting player "+playerTotal);
					MultiFriendzyWinnerScreen.isWinner = true;
				}
				else
				{
					//Log.e("", "Winner opponent player "+opponentTotal);
					MultiFriendzyWinnerScreen.isWinner = false;
				}

				startActivity(new Intent(MultiFriendzyEquationActivity.this , MultiFriendzyWinnerScreen.class));
			}

			super.onPostExecute(result);
		}

	}



	/**
	 * This method send notification to the devices 
	 * This method call after the data save on server aftre completion of 2 minuts
	 */
	private void sendNotification(CreateMultiFriendzyDto createMultiFrinedzyDto)
	{
		String notificationRecoiverDeviceId = "" ;//this variable contain the device ids to which notification is to be send
		String notificationIponDevices = "";

		String opponentName = null;

		if(opponentData != null)//data from searched user
		{
			//Log.e("", "searched");
			opponentName = opponentData.getFirstName() + " " + opponentData.getLastName().charAt(0) + ".";

			notificationRecoiverDeviceId = opponentData.getAndroidPids();
			notificationIponDevices      = opponentData.getIponPids();
		}
		else //  data from click on your turn
		{
			//Log.e("", "on your turn");
			String lastName = "";
			if(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().length() > 0 )
				lastName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName().charAt(0) + "";

			opponentName = MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getfName()
					+ " " + lastName + ".";

			notificationRecoiverDeviceId = MultiFriendzyRound.multiFriendzyServerDto.getAndroidPids();
			notificationIponDevices      = MultiFriendzyRound.multiFriendzyServerDto.getNotificationDevices();
		}

		Log.e("", "device pid "+notificationRecoiverDeviceId);

		SharedPreferences sharedPreff = getSharedPreferences(ICommonUtils.DEVICE_ID_PREFF, 0);
		String deviceId = sharedPreff.getString(ICommonUtils.DEVICE_ID, "");

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);
		String imageName = sharedPreffPlayerInfo.getString("imageName",null);
		//String playerName= sharedPreffPlayerInfo.getString("playerName", "");//changes for first character of last name
		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2)) + ".";

		String message   = playerName + MESSAGE_TEXT_FOR_NOTIFICATION + opponentName + "!";

		SendNotificationTransferObj sendNotificationObj = new SendNotificationTransferObj();
		sendNotificationObj.setDevicePid(notificationRecoiverDeviceId);//Receiver device ids
		sendNotificationObj.setIphoneDeviceIds(notificationIponDevices);//reciever devices
		sendNotificationObj.setMessage(message);//change
		sendNotificationObj.setUserId(createMultiFrinedzyDto.getPlayerUserId());
		sendNotificationObj.setPlayerId(createMultiFrinedzyDto.getPlayerId());
		sendNotificationObj.setCountry(coutryIso);
		sendNotificationObj.setOppUserId(createMultiFrinedzyDto.getOpponentUserId());
		sendNotificationObj.setOppPlayerId(createMultiFrinedzyDto.getOpponentPlayerId());
		sendNotificationObj.setFriendzyId(createMultiFrinedzyDto.getFriendzyId());
		sendNotificationObj.setProfileImageId(imageName);
		sendNotificationObj.setShareMessage(MultiFriendzyRound.notificationMessage);//changes
		sendNotificationObj.setSenderDeviceId(deviceId);//sender device ids (udid set after get from server)

		Log.e("MultiFriendzyActivity", "send Notification ");

		//get android pid from server after that sent notification
		new GetAndroidDevicePidForUser(createMultiFrinedzyDto.getPlayerUserId(), deviceId , sendNotificationObj)
		.execute(null,null,null);

		// the asynckTask send notification to the devices
		//new SendNotification(sendNotificationObj).execute(null,null,null);		
	}


}
