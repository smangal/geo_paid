package com.mathfriendzy.controller.school;

import static com.mathfriendzy.utils.ICommonUtils.SEACRCH_YOUR_TEACHER;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.geographyfriendzypaid.R;
import com.mathfriendzy.controller.player.SimpleListAdapter;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schools.Schools;
import com.mathfriendzy.model.schools.TeacherDTO;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

/**
 * This Acitivity is open for select teacher
 * @author Yashwant Singh
 *
 */
public class SearchTeacherActivity extends AdBaseActivity implements OnClickListener //, OnKeyListener
{
	private TextView mfTitleHomeScreen 				= null;
	private EditText SearchTeacherEdtSearch 		= null;
	private ListView SearchTeacherListViewSchool 	= null;
	private Button SearchTeacherBtnCannotFindTeacher= null;
		
	private ArrayList<TeacherDTO> teacherList 	 	= null;
	ArrayList<String> techerFirstName            	= null;
	private ArrayList<String> newtecherFirstName	= null;
	
	protected final String TEACHAER_TEXT  	  = "Cannot find my teacher";
	ProgressDialog prgressDailog = null;
	
	SimpleListAdapter adapter	 = null;
	int textSize = 0;
	
	private final String TAG = this.getClass().getSimpleName();
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_teacher);
		
		if(getResources().getBoolean(R.bool.isTabSmall))
		{
			textSize = 24;
		}
		else
		{
			textSize = 14;
		}
		
		if(SEACRCH_YOUR_TEACHER)
			Log.e(TAG, "inside onCreate()");
		
		this.setWidgetsReferences();
		this.setTextValues();
		this.getVlaueFromIntent();
		this.setListenerOnWidgets();
		
		if(SEACRCH_YOUR_TEACHER)
			Log.e(TAG, "outside onCreate()");
		
	}
	
	/**
	 * This method get the value from intent and execute getTeacher AsynckTask based on school ID from intent
	 */
	private void getVlaueFromIntent() 
	{
		if(SEACRCH_YOUR_TEACHER)
			Log.e(TAG, "inside getVlaueFromIntent()");
		
		//changes for Internet Connection
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			new GetTeacher(this.getIntent().getStringExtra("schoolId"), "", this).execute(null,null,null);
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
		
		if(SEACRCH_YOUR_TEACHER)
			Log.e(TAG, "outside getVlaueFromIntent()");
	}

	/**
	 * This method set the widgets references fromthe layout to the widgets Objects
	 */
	private void setWidgetsReferences() 
	{
		if(SEACRCH_YOUR_TEACHER)
			Log.e(TAG, "inside setWidgetsReferences()");
		
		mfTitleHomeScreen 				= (TextView) findViewById(R.id.mfTitleHomeScreen);
		SearchTeacherEdtSearch 			= (EditText) findViewById(R.id.SearchSchoolEdtSearch);
		SearchTeacherListViewSchool 	= (ListView) findViewById(R.id.SearchSchoolListViewSchool);
		SearchTeacherBtnCannotFindTeacher= (Button)  findViewById(R.id.SearchSchoolBtnCannotFindSchool);
		
		if(SEACRCH_YOUR_TEACHER)
			Log.e(TAG, "outside setWidgetsReferences()");
	}
	
	/**
	 * This method set the textWidgets Values from the translation
	 */
	private void setTextValues()
	{
		if(SEACRCH_YOUR_TEACHER)
			Log.e(TAG, "inside setTextValues()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("homeTitleFriendzy"));
		SearchTeacherBtnCannotFindTeacher.setText(transeletion.getTranselationTextByTextIdentifier("pickerValTeacherNotFound"));
		transeletion.closeConnection();
		
		if(SEACRCH_YOUR_TEACHER)
			Log.e(TAG, "outside setTextValues()");
	}
	
	/**
	 * This method set the listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(SEACRCH_YOUR_TEACHER)
			Log.e(TAG, "inside setListenerOnWidgets()");
		
		SearchTeacherListViewSchool.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int possition,long id) 
			{	
				if(teacherList.get(possition).getfName().equals(TEACHAER_TEXT))
				{
					TeacherDTO teacherObj = new TeacherDTO();
					teacherObj.setfName(teacherList.get(possition).getfName());
					teacherObj.setlName(teacherList.get(possition).getlName());
					teacherObj.setTeacherUserId(teacherList.get(possition).getTeacherUserId());
					
					Translation transeletion = new Translation(SearchTeacherActivity.this);
					transeletion.openConnection();
					DialogGenerator dg = new DialogGenerator(SearchTeacherActivity.this);
					String text = transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseRequestYourTeacher");
					text = text+ " " + transeletion.getTranselationTextByTextIdentifier("homeTitleFriendzy");
					dg.canNotFindTeacherDialog(text, teacherObj,RESULT_OK);
					transeletion.closeConnection();
				}
				else
				{
					ArrayList<String> teacherIdAndName = new ArrayList<String>();
					teacherIdAndName.add(teacherList.get(possition).getfName());//add teacher fname at 0th position
					teacherIdAndName.add(teacherList.get(possition).getlName());//add teacher lname at 1st position
					teacherIdAndName.add(teacherList.get(possition).getTeacherUserId());//add teacher id at 2nd position
					Intent returnIntent = new Intent();
					returnIntent.putExtra("schoolInfo",teacherIdAndName);
					setResult(RESULT_OK,returnIntent);     
					finish();
				}
			}
		});
		
		//SearchTeacherEdtSearch.setOnKeyListener(this);
		
		SearchTeacherEdtSearch.addTextChangedListener(new TextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				// TODO Auto-generated method stub
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) 
			{
				// TODO Auto-generated method stub
			}
			
			@Override
			public void afterTextChanged(Editable s) 
			{
				newtecherFirstName = new ArrayList<String>();
				
				 /* This logic for the searching according to the given string in search Bar(EditText)
				  and display which match*/
				 
				for(int i = 0 ; i < techerFirstName.size() ; i++)
				{
					if(techerFirstName.get(i).toLowerCase().contains(SearchTeacherEdtSearch.getText().toString()
							.toLowerCase())) 
						newtecherFirstName.add(techerFirstName.get(i));
				}
				adapter = new SimpleListAdapter(SearchTeacherActivity.this, newtecherFirstName, textSize);
				SearchTeacherListViewSchool.setAdapter(adapter);
				
			}
		});
		
		SearchTeacherBtnCannotFindTeacher.setOnClickListener(this);
		if(SEACRCH_YOUR_TEACHER)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}
	
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.SearchSchoolBtnCannotFindSchool:
			this.showTeacherDialog();
			break;
		}
	}
	
	/**
	 * This function shoe the teacher dialog alert message when can not find teacher in the given list from tha server	
	 */
	private void showTeacherDialog()
	{
		/*Translation transeletion = new Translation(this);
		transeletion.openConnection();
			
		DialogGenerator dg = new DialogGenerator(this);
		dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgRequestYourTeacher"));
		transeletion.closeConnection();*/
		
		TeacherDTO teacherObj = new TeacherDTO();
		teacherObj.setfName(TEACHAER_TEXT);
		teacherObj.setlName("");
		teacherObj.setTeacherUserId("-1");
		
		Translation transeletion = new Translation(SearchTeacherActivity.this);
		transeletion.openConnection();
		DialogGenerator dg = new DialogGenerator(SearchTeacherActivity.this);
		dg.canNotFindTeacherDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgRequestYourTeacher")
				,teacherObj,RESULT_OK);
		transeletion.closeConnection();
	}
	
	/**
	 * This asyncTask get Teacher from server url and set it to the teacher spinner(drop dawn list)
	 * @author Yashwant Singh
	 *
	 */
	class GetTeacher extends AsyncTask<Void, Void, ArrayList<TeacherDTO>>
	{
		private Context context 	= null;
		private String schoolId     = null;
		
		public GetTeacher(String schoolid,String teacherName,Context context)
		{
			this.schoolId = schoolid;
			this.context = context;
		}
		
		@Override
		protected void onPreExecute() 
		{
			if(SEACRCH_YOUR_TEACHER)
				Log.e(TAG, "inside GetTeacher onPreExecute()");
			
			prgressDailog = CommonUtils.getProgressDialog(context);
			prgressDailog.show();
			
			if(SEACRCH_YOUR_TEACHER)
				Log.e(TAG, "outside GetTeacher onPreExecute()");
			
			super.onPreExecute();
		}
		
		
		@Override
		protected ArrayList<TeacherDTO> doInBackground(Void... params) 
		{		
			if(SEACRCH_YOUR_TEACHER)
				Log.e(TAG, "inside GetTeacher doInBackground()");
			
			ArrayList<TeacherDTO> techerList = new ArrayList<TeacherDTO>();
			Schools schoolObj = new Schools();
			techerList = schoolObj.getTeacherList(schoolId);
				
			TeacherDTO teacherDto = new TeacherDTO();
			teacherDto.setfName(TEACHAER_TEXT);
			teacherDto.setlName("");
			teacherDto.setTeacherUserId("-1");
			
			techerList.add(teacherDto);
			
			if(SEACRCH_YOUR_TEACHER)
				Log.e(TAG, "outside GetTeacher doInBackground()");
			
		   return techerList;
		}

		@Override
		protected void onPostExecute(ArrayList<TeacherDTO> result) 
		{
			if(SEACRCH_YOUR_TEACHER)
				Log.e(TAG, "inside GetTeacher onPostExecute()");
			
			prgressDailog.cancel();
			
			teacherList = new ArrayList<TeacherDTO>();
			teacherList = result;
			
			techerFirstName = new ArrayList<String>();
			for( int i = 0 ; i < result.size() ; i++)
			{
				techerFirstName.add(result.get(i).getfName() + result.get(i).getlName());
			}
			
			newtecherFirstName = techerFirstName;
			adapter = new SimpleListAdapter(context, newtecherFirstName, textSize);
			SearchTeacherListViewSchool.setAdapter(adapter);
				
			if(SEACRCH_YOUR_TEACHER)
				Log.e(TAG, "outside GetTeacher onPostExecute()");
			
			super.onPostExecute(result);
		}
	}
}
