package com.mathfriendzy.controller.resources;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.geographyfriendzypaid.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.serveroperation.HttpResponseBase;
import com.mathfriendzy.serveroperation.ServerOperationUtil;
import com.mathfriendzy.utils.CommonUtils;

public class ActLessonCategories extends ActBase {

    private final String TAG = this.getClass().getSimpleName();
    private LinearLayout goLayout = null;
    private TextView txtSearchOverLessons = null;
    private EditText edtEnterTopic = null;
    private Button btnGo = null;
    private TextView txtChooseCategory = null;
    private ListView lstLessonCategories = null;
    private String alerMessageSearchTopicRequired = null;
    private TextView txtSearchTopic = null;
    private TextView txtsubject = null;
    private Spinner spinnerSubject = null;
    private String mathSelectedSubject = "Math";
    private RelativeLayout selectGradeLayut = null;
    private boolean isCategoryListOpen = false;
    private final int VIDEO = 1;
    private final int WEBPAGE = 2;
    private final int IMAGES = 3;
    private final int TEXT = 4;
    private CheckBox chk1				= null;
    private CheckBox chk2				= null;
    private CheckBox chk3				= null;
    private CheckBox chk4				= null;
    private CheckBox chk5				= null;
    private CheckBox chk6				= null;
    private CheckBox chk7				= null;
    private CheckBox chk8				= null;
    private CheckBox chk9				= null;
    private CheckBox chk10				= null;
    private CheckBox chk11				= null;
    private CheckBox chk12				= null;
    private CheckBox chkVideo			= null;
    private CheckBox chkWebpage			= null;
    private CheckBox chkImages			= null;
    private CheckBox chkText			= null;
    private String alerMessageGradeRequired = null;

    //new resources change for khan academy
    private CheckBox chkEnglish = null;
    private CheckBox chkSpanish = null;
    private final int ENGLISH = 1;
    private final int SPANISH = 2;
    private int selectedLanguage = ENGLISH;//default selected language

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_lesson_categories);
        CommonUtils.printLog(TAG, "inside onCreate()");

        MathFriendzyHelper.saveResorceSelectedLanguage(this , ENGLISH);

        this.setWidgetsReferences();
        this.setTextFromTranslation();
        this.setListenerOnWidgets();
        this.getCategories();

        CommonUtils.printLog(TAG, "outside onCreate()");
    }

    @Override
    protected void setWidgetsReferences() {
        CommonUtils.printLog(TAG, "inside setWidgetsReferences()");
        txtTopbar = (TextView) findViewById(R.id.txtTopbar);
        goLayout = (LinearLayout) findViewById(R.id.goLayout);
        edtEnterTopic = (EditText) findViewById(R.id.edtEnterTopic);
        btnGo = (Button) findViewById(R.id.btnGo);
        txtChooseCategory = (TextView) findViewById(R.id.txtChooseCategory);
        lstLessonCategories = (ListView) findViewById(R.id.lstLessonCategories);
        txtSearchOverLessons = (TextView) findViewById(R.id.txtSearchOverLessons);
        txtSearchTopic = (TextView) findViewById(R.id.txtSearchTopic);
        txtsubject = (TextView) findViewById(R.id.txtsubject);
        spinnerSubject = (Spinner) findViewById(R.id.spinnerSubject);
        selectGradeLayut = (RelativeLayout) findViewById(R.id.selectGradeLayut);

        chk1 = (CheckBox) findViewById(R.id.chk1);
        chk2 = (CheckBox) findViewById(R.id.chk2);
        chk3 = (CheckBox) findViewById(R.id.chk3);
        chk4 = (CheckBox) findViewById(R.id.chk4);
        chk5 = (CheckBox) findViewById(R.id.chk5);
        chk6 = (CheckBox) findViewById(R.id.chk6);
        chk7 = (CheckBox) findViewById(R.id.chk7);
        chk8 = (CheckBox) findViewById(R.id.chk8);
        chk9 = (CheckBox) findViewById(R.id.chk9);
        chk10 = (CheckBox) findViewById(R.id.chk10);
        chk11 = (CheckBox) findViewById(R.id.chk11);
        chk12 = (CheckBox) findViewById(R.id.chk12);
        chkVideo = (CheckBox) findViewById(R.id.chkvideo);
        chkWebpage = (CheckBox) findViewById(R.id.chkwebpage);
        chkImages = (CheckBox) findViewById(R.id.chkimages);
        chkText = (CheckBox) findViewById(R.id.chktext);

        //new resources change for khan academy
        chkEnglish = (CheckBox) findViewById(R.id.chkEnglish);
        chkSpanish = (CheckBox) findViewById(R.id.chkSpanish);

        CommonUtils.printLog(TAG, "outside setWidgetsReferences()");
    }

    @Override
    protected void setListenerOnWidgets() {
        CommonUtils.printLog(TAG, "inside setListenerOnWidgets()");
        btnGo.setOnClickListener(this);
        chkVideo.setOnClickListener(this);
        chkImages.setOnClickListener(this);
        chkText.setOnClickListener(this);
        chkWebpage.setOnClickListener(this);

        spinnerSubject.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                String currectSelectedSubject =
                        parent.getItemAtPosition(position).toString();
                if(mathSelectedSubject.equalsIgnoreCase(currectSelectedSubject)){
                    setVisibilityOfLayoutForSelectedSubject(true);
                }else{
                    setVisibilityOfLayoutForSelectedSubject(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        chkEnglish.setOnClickListener(this);
        chkSpanish.setOnClickListener(this);

        CommonUtils.printLog(TAG, "outside setListenerOnWidgets()");
    }

    @Override
    protected void setTextFromTranslation() {
        CommonUtils.printLog(TAG, "inside setTextFromTranslation()");
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        txtTopbar.setText(transeletion.getTranselationTextByTextIdentifier("navigationTitleResources"));
        txtSearchOverLessons.setText(transeletion.getTranselationTextByTextIdentifier
                ("lblHeaderBrowseAndAddResources"));
        btnGo.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleGo"));
        alerMessageSearchTopicRequired = transeletion.getTranselationTextByTextIdentifier
                ("alerMessageSearchTopicRequired");
        txtSearchTopic.setText(transeletion.getTranselationTextByTextIdentifier
                ("txtFieldPlaceHolderSearchTopic"));
        txtsubject.setText(transeletion.getTranselationTextByTextIdentifier("lblSubjectHeader"));
        chkVideo.setText(transeletion.getTranselationTextByTextIdentifier("lblLesson"));
        chkWebpage.setText(transeletion.getTranselationTextByTextIdentifier("lblResourceFormatWebPage"));
        chkImages.setText(transeletion.getTranselationTextByTextIdentifier("lblResourceFormatImages"));
        chkText.setText(transeletion.getTranselationTextByTextIdentifier("lblText"));
        alerMessageGradeRequired = transeletion.getTranselationTextByTextIdentifier
                ("alerMessageGradeRequired");
        //chkEnglish.setText(transeletion.getTranselationTextByTextIdentifier("lblText"));
        //chkSpanish.setText(transeletion.getTranselationTextByTextIdentifier("lblText"));
        transeletion.closeConnection();
        CommonUtils.printLog(TAG, "outside setTextFromTranslation()");
    }

    @Override
    public void serverResponse(HttpResponseBase httpResponseBase, int requestCode) {
        if(requestCode == ServerOperationUtil.GET_RESOURCE_CATEGORIES_REQUEST){
            ResourceCategory resourceCategory = (ResourceCategory) httpResponseBase;
            this.setCategoryAdapter(resourceCategory.getCategoriesList());
            this.setSubjects(resourceCategory.getSubjectList());
        }
    }

    private boolean checkForSearchValidate(){
        if(!isAnyGradeHave()){
            MathFriendzyHelper.showWarningDialog(this , alerMessageGradeRequired);
            return false;
        }
        return true;
    }

    /**
     * Check Grade .
     * @return Any one have grade mark then return true other wise false.
     */
    private boolean isAnyGradeHave() {
        if(chk1.isChecked() )
            return true;
        else if(chk2.isChecked() )
            return true;
        else if(chk3.isChecked() )
            return true;
        else if(chk4.isChecked() )
            return true;
        else if(chk5.isChecked() )
            return true;
        else if(chk6.isChecked() )
            return true;
        else if(chk7.isChecked() )
            return true;
        else if(chk8.isChecked() )
            return true;
        else if(chk9.isChecked() )
            return true;
        else if(chk10.isChecked() )
            return true;
        else if(chk11.isChecked() )
            return true;
        else if(chk12.isChecked() )
            return true;
        return false;
    }

    /**
     * This method Create a grade .
     * @return all grades .
     */
    private String getGrade() {
        StringBuffer grade = new StringBuffer();
        if(chk1.isChecked()) grade.append("1,");
        if(chk2.isChecked()) grade.append("2,");
        if(chk3.isChecked()) grade.append("3,");
        if(chk4.isChecked()) grade.append("4,");
        if(chk5.isChecked()) grade.append("5,");
        if(chk6.isChecked()) grade.append("6,");
        if(chk7.isChecked()) grade.append("7,");
        if(chk8.isChecked()) grade.append("8,");
        if(chk9.isChecked()) grade.append("9,");
        if(chk10.isChecked()) grade.append("10,");
        if(chk11.isChecked()) grade.append("11,");
        if(chk12.isChecked()) grade.append("12,");

        if(grade.length() != 0)
            // This line delete last char ( , ) from complete grade ."
            grade.delete(grade.length() - 1, grade.length());
        return grade.toString();
    }

    /**
     * This method check which resources are marked.
     * @return Resource name ( video , text , image , webpage )  or null string.
     */
    private int getResourceFormate() {
        if(chkVideo.isChecked())
            return VIDEO;
        else if(chkText.isChecked())
            return TEXT;
        else if(chkImages.isChecked())
            return IMAGES;
        else if(chkWebpage.isChecked())
            return WEBPAGE;
        return 0;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnGo:
                ResourceSearchTermParam searchTermParam = new ResourceSearchTermParam();
                if(!isCategoryListOpen){
                    /*if(!this.checkForSearchValidate())
                        return ;*/
                    searchTermParam.setDirectSearchFromCategoryScreen(true);
                    searchTermParam.setSelectedGrades(this.getGrade());
                    searchTermParam.setSelectedResourceFormat(this.getResourceFormate());
                    searchTermParam.setSelectedSubject(this.getSelectedSubject());
                }
                searchTermParam.setSelectedLang(selectedLanguage);
                String searchTerm = this.getSearchTerm();
                if(MathFriendzyHelper.isEmpty(searchTerm)){
                    MathFriendzyHelper.showWarningDialog(this , alerMessageSearchTopicRequired);
                    return ;
                }
                searchTermParam.setSearchTerm(searchTerm);
                MathFriendzyHelper.openResourceScreen(this , searchTermParam);
                break;
            case R.id.chkvideo:
                this.setResourcesTypeChecked(VIDEO);
                break;
            case R.id.chkimages:
                this.setResourcesTypeChecked(IMAGES);
                break;
            case R.id.chkwebpage:
                this.setResourcesTypeChecked(WEBPAGE);
                break;
            case R.id.chktext:
                this.setResourcesTypeChecked(TEXT);
                break;
            case R.id.chkEnglish:
                this.selectedLanguage(ENGLISH);
                break;
            case R.id.chkSpanish:
                this.selectedLanguage(SPANISH);
                break;

        }
    }

    /**
     * Set the selected type
     * @param type
     */
    private void setResourcesTypeChecked(int type){
        switch (type) {
            case VIDEO:
                chkVideo.setChecked(true);
                chkImages.setChecked(false);
                chkWebpage.setChecked(false);
                chkText.setChecked(false);
                break;
            case IMAGES:
                chkImages.setChecked(true);
                chkVideo.setChecked(false);
                chkWebpage.setChecked(false);
                chkText.setChecked(false);
                break;
            case WEBPAGE:
                chkWebpage.setChecked(true);
                chkVideo.setChecked(false);
                chkImages.setChecked(false);
                chkText.setChecked(false);
                break;
            case TEXT:
                chkText.setChecked(true);
                chkVideo.setChecked(false);
                chkImages.setChecked(false);
                chkWebpage.setChecked(false);
        }
    }

    private String getSearchTerm(){
        return edtEnterTopic.getText().toString();
    }


    private void setCategoryAdapter(final ArrayList<ResourceCategory> catList) {
        ResourceCategoryAdapter adapter = new ResourceCategoryAdapter
                (this , catList);
        lstLessonCategories.setAdapter(adapter);
        lstLessonCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openResourceSubCatScreen(catList.get(position));
            }
        });
    }

    private void getCategories(){
        MathFriendzyHelper.getResourceCategories(this , this);
    }

    private void openResourceSubCatScreen(ResourceCategory selectedCat){
        Intent intent = new Intent(this , ActLessonSubCategory.class);
        intent.putExtra("selectedCat" , selectedCat);
        intent.putExtra("selectedLanguage" , selectedLanguage);
        startActivity(intent);
    }

    private void setVisibilityOfLayoutForSelectedSubject(boolean isShowList){
        this.isCategoryListOpen = isShowList;
        edtEnterTopic.setText("");
        if(isShowList){
            lstLessonCategories.setVisibility(ListView.VISIBLE);
            txtChooseCategory.setVisibility(TextView.VISIBLE);
            selectGradeLayut.setVisibility(RelativeLayout.GONE);
        }else{
            lstLessonCategories.setVisibility(ListView.GONE);
            txtChooseCategory.setVisibility(TextView.INVISIBLE);
            selectGradeLayut.setVisibility(RelativeLayout.GONE);
        }
    }

    private void setSubjects(String[] subjectArray) {
        if(subjectArray != null && subjectArray.length > 0){
            ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>
                    (this, R.layout.spinner_country_item, subjectArray);
            countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSubject.setAdapter(countryAdapter);
            spinnerSubject.setSelection(countryAdapter.getPosition(mathSelectedSubject));
        }
    }

    private String getSelectedSubject(){
        try{
            return spinnerSubject.getSelectedItem().toString().trim();
        }catch(Exception e){
            return mathSelectedSubject;
        }
    }

    //new resources change for khan academy
    private void selectedLanguage(int selectedLanguage){
        this.selectedLanguage = selectedLanguage;
        if(selectedLanguage == ENGLISH){
            chkEnglish.setChecked(true);
            chkSpanish.setChecked(false);
        }else if(selectedLanguage == SPANISH){
            chkEnglish.setChecked(false);
            chkSpanish.setChecked(true);
        }
        MathFriendzyHelper.saveResorceSelectedLanguage(this , selectedLanguage);
    }
}
