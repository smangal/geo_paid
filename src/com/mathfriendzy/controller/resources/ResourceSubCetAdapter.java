package com.mathfriendzy.controller.resources;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.geographyfriendzypaid.R;

/**
 * Created by root on 12/1/16.
 */
public class ResourceSubCetAdapter extends BaseAdapter{
    private ArrayList<ResourceSubCat> categoryList = null;
    private LayoutInflater mInflator = null;
    private ViewHolder vHolder = null;

    public ResourceSubCetAdapter(Context context ,
                                   ArrayList<ResourceSubCat> categoryList){
        mInflator = LayoutInflater.from(context);
        this.categoryList = categoryList;
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if(view == null){
            view = mInflator.inflate(R.layout.resource_cat_layout_view , null);
            vHolder = new ViewHolder();
            vHolder.txtCategoryName = (TextView) view.findViewById(R.id.txtCategoryName);
            view.setTag(vHolder);
        }else{
            vHolder = (ViewHolder) view.getTag();
        }
        vHolder.txtCategoryName.setText(categoryList.get(position).getSubCatName());
        return view;
    }

    private class ViewHolder{
        private TextView txtCategoryName;
    }
}
