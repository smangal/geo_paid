package com.mathfriendzy.controller.singlefriendzy;

import static com.mathfriendzy.utils.ICommonUtils.CHOOSE_A_CHALLENGER_LIST;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.util.ArrayList;
import java.util.Random;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.geographyfriendzypaid.R;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.singleFriendzy.ChallengerDataFromServerTranseferObj;
import com.mathfriendzy.model.singleFriendzy.ChallengerTransferObj;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyServerOperation;

/**
 * This Acitivity show all the challenger , select one and play with him
 * @author Yashwant Singh
 *
 */
public class ChooseAChallengerListActivity extends AdBaseActivity 
{	
	private TextView mfLblChooseAChallenger 				= null;
	private TextView mfLblWeFoundChallengersAroundTheWorld 	= null;
	private TextView mfLblChooseAChallengerForList 			= null;
	private TextView txtName 								= null;
	private TextView txtpoints 								= null;
	private TextView txtLevel 								= null;
	private ListView lstChallanger                          = null;

	private int highestScoreForPlayer    = 0 ; // this contain the highest score from server for login user
	private final int RANDOM_ADDED_VALUE = 3 ; // This will add to the random number value from 0 to 5 to 
	// make it 3 to 8
	private ChallengerDataFromServerTranseferObj challengerData = null;

	private final int HIGHEST_POINT_RANGE = 3000;//highest point range from highestScoreForPlayer-3000 
	//to highestScoreForPlayer + 3000 
	private final int INITIAL_RANGE       = 1000;//set initial range point
	private final int  MAX_RANGE          = 4000;//max range range point

	private final String TAG = this.getClass().getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_achallenger_list);

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "inside onCreate()");

		this.setWidgetsReferences();
		this.setTextFromTranselation();
		//this.setChallnegerList();
		this.getDataFromPreviousActivitySetValue();
		this.getHighestScore();

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "outside onCreate()");
	}


	/**
	 * This method get data from intent 
	 */
	private void getDataFromPreviousActivitySetValue() 
	{
		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "inside getDataFromIntent()");

		challengerData = ChooseAchallengerActivity.challengerDate;

		/*for(int i = 0 ; i < challengerData.getChallengerPlayerList().size() ; i ++ )
		{
			Log.e(TAG, "first" + challengerData.getChallengerPlayerList().get(i).getFirst()
					+ " last " + challengerData.getChallengerPlayerList().get(i).getLast()
					+ " country " + challengerData.getChallengerPlayerList().get(i).getCountryIso()
					+" player id " + challengerData.getChallengerPlayerList().get(i).getPlayerId()
					+ " usre id " + challengerData.getChallengerPlayerList().get(i).getUserId()
					+ " isFakePlayer " + challengerData.getChallengerPlayerList().get(i).getIsFakePlayer());
		}
		 */
		//Log.e(TAG, "notification device " + challengerData.getPushNotificationDevice());

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "inside getDataFromIntent()");
	}


	/**
	 * This method set the widhets references from the layout
	 */
	private void setWidgetsReferences() 
	{

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "inside setWidgetsReferences()");

		mfLblChooseAChallenger 					= (TextView) findViewById(R.id.mfLblChooseAChallenger);
		mfLblWeFoundChallengersAroundTheWorld 	= (TextView) findViewById(R.id.mfLblWeFoundChallengersAroundTheWorld);
		mfLblChooseAChallengerForList 			= (TextView) findViewById(R.id.mfLblChooseAChallengerForList);
		txtName 								= (TextView) findViewById(R.id.txtName);
		txtpoints 								= (TextView) findViewById(R.id.txtpoints);
		txtLevel 								= (TextView) findViewById(R.id.txtLevel);
		lstChallanger 							= (ListView) findViewById(R.id.lstChallanger);

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "outside setWidgetsReferences()");

	}

	/**
	 * This method set the text value from trnaselation
	 */
	private void setTextFromTranselation() 
	{

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "inside setTextFromTranselation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfLblChooseAChallenger.setText(transeletion.getTranselationTextByTextIdentifier("mfLblChooseAChallenger"));
		mfLblChooseAChallengerForList.setText(transeletion.getTranselationTextByTextIdentifier("mfLblChooseAChallenger") + ":");
		mfLblWeFoundChallengersAroundTheWorld.setText(transeletion.getTranselationTextByTextIdentifier("mfLblWeFoundChallengersAroundTheWorld"));
		txtName.setText(transeletion.getTranselationTextByTextIdentifier("lblRegName") + ":");
		txtLevel.setText(transeletion.getTranselationTextByTextIdentifier("mfLblLevel") + ":");
		txtpoints.setText(transeletion.getTranselationTextByTextIdentifier("btnTitlePoints") + ":");
		transeletion.closeConnection();

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "outside setTextFromTranselation()");
	}

	/**
	 * This method get highest score from server for login user and set 0 for temp player
	 */
	private void getHighestScore()
	{
		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "inside getHighestScore()");

		SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);	 
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{
			highestScoreForPlayer = 0 ; 

			this.createNewChellengerList();
		}
		else
		{			
			new GetHighestScore(sharedPrefPlayerInfo.getString("userId", ""), 
					sharedPrefPlayerInfo.getString("playerId", "")).execute(null,null,null);
		}

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "outside getHighestScore()");
	}

	/**
	 * This method create a challenger list which is to be display
	 */
	private void createNewChellengerList()
	{
		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "inside createNewChellengerList()");

		ArrayList<ChallengerTransferObj> challengerList = new ArrayList<ChallengerTransferObj>();

		Random random = new Random();
		int noOfChallenger = random.nextInt(6);
		noOfChallenger = noOfChallenger + RANDOM_ADDED_VALUE;

		this.setNumberOfChallengerToText(noOfChallenger);

		if(challengerData.getChallengerPlayerList() != null)
		{
			for( int i = 0 ; i < noOfChallenger ; i ++ )
			{
				int randomNuber = random.nextInt(challengerData.getChallengerPlayerList().size());

				ChallengerTransferObj challengerObj = challengerData.getChallengerPlayerList().get(randomNuber);

				if(challengerObj.getSum().length() > 0)
					challengerList.add(challengerObj);
				else
				{
					challengerList.add(this.setChallengerPlayerPoints(challengerObj));
				}

				challengerData.getChallengerPlayerList().remove(randomNuber);//remove from the list
			}
		}
		this.setChallnegerList(challengerList);

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "outside createNewChellengerList()");
	}

	/**
	 * This method set the text value to  the we found noOfChallenger challenger in the world
	 */
	private void setNumberOfChallengerToText(int noOfChallenger)
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfLblWeFoundChallengersAroundTheWorld.setText(transeletion.getTranselationTextByTextIdentifier("mfLblWeFoundChallengersAroundTheWorld"));
		transeletion.closeConnection();
	}

	/**
	 * This method set the challenger list to the list view 
	 */
	private void setChallnegerList(ArrayList<ChallengerTransferObj> challengerList)
	{
		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "inside setChallnegerList()");

		ChooseChallengerAdapter adapter = new ChooseChallengerAdapter(this,R.layout.challanger_layout_for_single_friendzy , 
				this.shortChallengerListByPoints(challengerList));
		lstChallanger.setAdapter(adapter);

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "outside setChallnegerList()");
	}


	/**
	 * This method arrange the points into descending order
	 * @param challengerList
	 */
	private ArrayList<ChallengerTransferObj> shortChallengerListByPoints(ArrayList<ChallengerTransferObj> challengerList)
	{
		int j;
		boolean flag = true;   // set flag to true to begin first pass
		String temp;   //holding variable

		while ( flag )
		{
			flag= false;    //set flag to false awaiting a possible swap
			for( j=0;  j < challengerList.size() -1;  j++ )
			{
				int value1 = Integer.parseInt(challengerList.get(j).getSum());
				int value2 = Integer.parseInt(challengerList.get(j + 1).getSum());

				if ( value1 < value2 )   // change to > for ascending sort
				{
					temp = challengerList.get(j).getSum();//swap elements
					challengerList.get(j).setSum(challengerList.get(j + 1).getSum());
					challengerList.get(j + 1).setSum(temp);
					flag = true;              //shows a swap occurred  
				} 
			} 
		} 

		return challengerList;
	}


	/**
	 * This method set the challenger player points
	 * @param challengerObj
	 * @return
	 */
	private ChallengerTransferObj setChallengerPlayerPoints(ChallengerTransferObj challengerObj)
	{
		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "inside ChallengerTransferObj()");

		Random random = new Random();
		int points   = 0;

		if(highestScoreForPlayer - HIGHEST_POINT_RANGE > 0)
		{
			int diff = highestScoreForPlayer - HIGHEST_POINT_RANGE ;

			//random = new Random((highestScoreForPlayer + HIGHEST_POINT_RANGE) - diff);

			points = random.nextInt((highestScoreForPlayer + HIGHEST_POINT_RANGE) - diff);

			points = points + diff;
		}
		else
		{			
			//random = new Random(MAX_RANGE - INITIAL_RANGE);

			points = random.nextInt(MAX_RANGE - INITIAL_RANGE);

			points = points + INITIAL_RANGE;
		}

		//Log.e(TAG, " inside setChallengerPlayerPoints points " + points);
		challengerObj.setSum( points + "");

		if(CHOOSE_A_CHALLENGER_LIST)
			Log.e(TAG, "outside ChallengerTransferObj()");

		return challengerObj;
	}


	@Override
	public void onBackPressed() 
	{
		startActivity(new Intent(this,MainActivity.class));
		super.onBackPressed();
	}


	/**
	 * This class get highest score from server for login user
	 * @author Yashwant Singh
	 *
	 */
	class GetHighestScore extends AsyncTask<Void, Void, Void>
	{	
		private String userId;
		private String playerId;

		GetHighestScore(String userId , String playerId)
		{
			this.userId 	= userId;
			this.playerId 	= playerId;
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			if(CHOOSE_A_CHALLENGER_LIST)
				Log.e(TAG, "inside GetHighestScore doInBackground()");


			SingleFriendzyServerOperation serverObj = new SingleFriendzyServerOperation();
			highestScoreForPlayer = serverObj.getHighestScore(userId, playerId);

			if(CHOOSE_A_CHALLENGER_LIST)
				Log.e(TAG, "outside GetHighestScore doInBackground()");

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			if(CHOOSE_A_CHALLENGER_LIST)
				Log.e(TAG, "inside GetHighestScore onPostExecute()");

			createNewChellengerList();

			if(CHOOSE_A_CHALLENGER_LIST)
				Log.e(TAG, "outside GetHighestScore onPostExecute()");
			super.onPostExecute(result);
		}

	}
}
