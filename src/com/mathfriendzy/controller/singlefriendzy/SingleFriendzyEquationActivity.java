package com.mathfriendzy.controller.singlefriendzy;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ICommonUtils.SINGLE_FRIENDZY_EQUATION_SOLVE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geographyfriendzypaid.R;
import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.controller.learningcenter.SeeAnswerActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.friendzy.SaveTimePointsForFriendzyChallenge;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterTransferObj;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.MathResultTransferObj;
import com.mathfriendzy.model.learningcenter.MathScoreTransferObj;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.learningcenter.SeeAnswerTransferObj;
import com.mathfriendzy.model.learningcenter.triviaEquation.TriviaQuestion;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.singleFriendzy.ChallengerTransferObj;
import com.mathfriendzy.model.singleFriendzy.EquationFromServerTransferObj;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyImpl;
import com.mathfriendzy.model.singleFriendzy.SingleFriendzyServerOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.PlaySound;

public class SingleFriendzyEquationActivity extends AdBaseActivity implements OnClickListener
{
	private final String TAG = this.getClass().getSimpleName();
	private ChallengerTransferObj challengerDataObj 		= null;

	private TextView txtPlayer1Name							= null;
	private TextView txtPlayer2Name							= null;
	private TextView txtPlayer1Points						= null;
	private TextView txtPlayer2Points						= null;
	private TextView txtQueNo								= null;
	private TextView txtQuestion							= null;
	private TextView txtOption1								= null;
	private TextView txtOption2								= null;
	private TextView txtOption3								= null;
	private TextView txtOption4								= null;
	private TextView mfTitleHomeScreen						= null;
	private TextView txtTime								= null;

	private LinearLayout progressBarLayout1					= null;
	private LinearLayout progressBarLayout2					= null;
	private RelativeLayout layoutReady						= null;
	private RelativeLayout layoutShow						= null;

	private RelativeLayout ansLayout1						= null;
	private RelativeLayout ansLayout2						= null;
	private RelativeLayout ansLayout3						= null;
	private RelativeLayout ansLayout4						= null;
	private ImageView imgFlagPlayer1						= null;
	private ImageView imgFlagPlayer2						= null;
	private ImageView imgProgressBar						= null;
	private ImageView getReady								= null;

	private ArrayList<String> optionArray					= null;
	private LearningCenterTransferObj questionObj 			= null;

	private int questionNumber								= 0;
	private int id											= -1;
	private final int MAX_QUESTION							= 10;
	private CountDownTimer timer							= null;
	private long START_TIME									= 2*60*1000;
	private final long INTERVAL								= 1*1000;
	private static long playingTime							= 0;
	private String strPoint									= null;

	//Defining two integer variable for storing play start_time and end_time
	private int start_time									= 0;
	private int end_time									= 0;
	private static boolean timeFlag							= false;
	private long DISPLAY_TIME 								= 0;
	//Max and mininmum points for a correct answer
	private final int MIN_SCORE								= 225;
	private final int MAX_SCORE								= 250;
	private boolean flag_right								= false;

	private int pointsPlayer1 								= 0;
	private int pointsPlayer2 								= 0;
	private int pointsForEachQuestion 						= 0;

	private int rightAnsCounter  							= 0;
	private int numberOfCoins     							= 0;


	private float coinsPerPoint   							= 0.05f;

	private static boolean isClickOnBackPressed 			= true;
	public static SeeAnswerTransferObj seeAnswerDataObj 	= null;
	private String userAnswer 								= "";
	private int isCorrectAnsByUser 							= 0;

	private Date startTime 									= null;
	private Date endTime   									= null;
	private int totalTimeTaken								= 0;
	private int totalTime									= 0;

	private boolean isStopSecondPlayerProgress 				= false;
	private boolean isEquationFromServer 					= false;

	private final long START_TIME_FOR_GET_READY 			= 7 * 1000;
	private final int EQUAITON_TYPE 						= 11;
	private int equationId 									= 0;
	private int isWin			 							= 0;
	private int operationId     					        = 0;
	private int starForPlayer 								= 0;
	private String userId									= null;
	private String playerId									= null;
	private PlaySound	playSound							= null;
	private String gameType									= "Play";
	private int index_challenger							= 0;
	private int bonus										= 200;
	public static int completeLevel 						= 0;//level user play at current which is set in Single FriendzyMain

	private ArrayList<Integer> equationsIdList 									= null;
	private ArrayList<EquationFromServerTransferObj> equListForSecondPlayer 	= null;//hold the equation data from server
	private ArrayList<LearningCenterTransferObj> playEquationList 				= null;
	private ArrayList<String> userAnswerList 									= null;
	private ArrayList<MathScoreTransferObj> playMathlist 						= null;
	private boolean isTabSmall													= false;
	private boolean isCompeteLevel												= false;


	//Change for time issue
	private final int EXTRA_PLAY_TIME						= 30;
	private final int MIN_PLAY_TIME							= 90;
	private boolean isClick									= false;
	private Date holdingTime								= null;
	//ENd Change

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_single_friendzy_equation);
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		if(metrics.heightPixels >= CommonUtils.TAB_HEIGHT && metrics.widthPixels <= CommonUtils.TAB_WIDTH
				&& metrics.densityDpi <= CommonUtils.TAB_DENISITY)	{

			setContentView(R.layout.activity_single_friendzy_equation_low_tab);

		}

		isTabSmall = getResources().getBoolean(R.bool.isTabSmall);	

		playMathlist       = new ArrayList<MathScoreTransferObj>();
		seeAnswerDataObj 	= new SeeAnswerTransferObj();
		userAnswerList		= new ArrayList<String>();
		playEquationList	= new ArrayList<LearningCenterTransferObj>();
		questionObj			= new LearningCenterTransferObj();

		isClickOnBackPressed	= true;
		timeFlag				= false;

		getWidgetId();
		getReadyTimer();
		setPlayerDetail();

		getEquations();
		setWidgetListener();
		setWidgetText();
		setPoints();				

		startTime	= new Date();
		endTime		= new Date();		

	}//END onCreate method



	protected void getWidgetId()
	{
		mfTitleHomeScreen	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		txtOption1			= (TextView) findViewById(R.id.txtOption1);
		txtOption2			= (TextView) findViewById(R.id.txtOption2);
		txtOption3			= (TextView) findViewById(R.id.txtOption3);
		txtOption4			= (TextView) findViewById(R.id.txtOption4);
		txtQuestion			= (TextView) findViewById(R.id.txtQue);
		txtQueNo			= (TextView) findViewById(R.id.txtQueNo);
		txtTime				= (TextView) findViewById(R.id.txtTime);

		txtPlayer1Name		= (TextView) findViewById(R.id.txtPlayer1Name);
		txtPlayer1Points	= (TextView) findViewById(R.id.txtPlayer1Points);
		imgFlagPlayer1		= (ImageView) findViewById(R.id.imgFlagPlayer1);
		txtPlayer2Name		= (TextView) findViewById(R.id.txtPlayer2Name);
		txtPlayer2Points	= (TextView) findViewById(R.id.txtPlayer2Points);
		imgFlagPlayer2		= (ImageView) findViewById(R.id.imgFlagPlayer2);

		progressBarLayout1	= (LinearLayout) findViewById(R.id.progressBarLayout1);
		progressBarLayout2	= (LinearLayout) findViewById(R.id.progressBarLayout2);
		ansLayout1			= (RelativeLayout) findViewById(R.id.ansLayout1);
		ansLayout2			= (RelativeLayout) findViewById(R.id.ansLayout2);
		ansLayout3			= (RelativeLayout) findViewById(R.id.ansLayout3);
		ansLayout4			= (RelativeLayout) findViewById(R.id.ansLayout4);		

		layoutReady			= (RelativeLayout) findViewById(R.id.layoutReady);
		layoutShow			= (RelativeLayout) findViewById(R.id.layoutShow);
		getReady			= (ImageView) findViewById(R.id.getReady);
	}//END getWidgetId method




	/**
	 * This method getEquation from server or database
	 */
	private void getEquations()
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside getEquations()");

		if(challengerDataObj.getIsFakePlayer() != 1 && (challengerDataObj.getSum().length() > 0
				&& (!challengerDataObj.getSum().equals("0"))) && CommonUtils.isInternetConnectionAvailable(this))
		{	
			isEquationFromServer = true;
			new FindCompleteEquationsForPlayer(challengerDataObj.getUserId() , challengerDataObj.getPlayerId())
			.execute(null,null,null);
		}
		else
		{
			getEquationsFromLocalDatabase();
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside getEquations()");
	}



	/**
	 * use to select question list from local database
	 */
	private void getEquationsFromLocalDatabase()
	{
		TriviaQuestion triviaQuestion = new TriviaQuestion(this);
		triviaQuestion.openConn();
		playEquationList = triviaQuestion.getQuestionListForChallenger(completeLevel, userId, playerId);	
		if(playEquationList.size() > 10 && playEquationList.size() < 20)
		{
			triviaQuestion.deleteRecordFromTriviaQuestionsPlayed();
		}
		triviaQuestion.closeConn();

		craeteChallengerDataListFromDataBase();

	}


	/**
	 * This method create challenger data list from database
	 */
	private void craeteChallengerDataListFromDataBase()
	{
		int timeTaken = 0 ;	
		equListForSecondPlayer = new ArrayList<EquationFromServerTransferObj>();

		for( int i = 0 ; i < MAX_QUESTION ; i ++)
		{
			EquationFromServerTransferObj equationObj = new EquationFromServerTransferObj();

			timeTaken = calculateTimeForSecondPlayer();
			equationObj.setTimeTakenToAnswer(timeTaken);

			equationObj.setPoints(setPoints(timeTaken));

			int isAnsCorrect = getIsAnswerCorrectValue() ;
			equationObj.setIsAnswerCorrect(isAnsCorrect);
			equationObj.setOperationId(playEquationList.get(i).getOperationId());
			equationObj.setMathEquationId(playEquationList.get(i).getEquationId());
			equListForSecondPlayer.add(equationObj);

		}
	}

	/**
	 * This method calculate the time taken by other user when equations from database
	 * @return
	 */
	private int calculateTimeForSecondPlayer()
	{
		int calculatedTime = 0;//initially 0
		if(completeLevel >= 1 && completeLevel <= 10)
			calculatedTime = this.calculateTime(12 , 16);
		else if(completeLevel >= 11 && completeLevel <= 20)
			calculatedTime = this.calculateTime(10 , 14);
		else if(completeLevel >= 21 && completeLevel <= 30)
			calculatedTime = this.calculateTime(8 , 12);
		else if(completeLevel >= 31)
			calculatedTime = this.calculateTime(6 , 10);

		return calculatedTime ; 
	}

	/**
	 * This method calculate time for the second player
	 * @param min
	 * @param max
	 * @return
	 */
	private int calculateTime(int min , int max)
	{
		Random random = new Random();

		int timeTaken = random.nextInt(max - min + 1);

		return timeTaken + min;
	}


	/**
	 * This method set the points
	 */
	private int setPoints(int timeTaken)
	{
		int calculatedpoints;

		calculatedpoints = MAX_SCORE - (timeTaken * 5);

		if(calculatedpoints < MIN_SCORE)
			return MIN_SCORE;

		return calculatedpoints ; 
	}




	/**
	 * This method return the value of isAnser correct
	 */
	private int getIsAnswerCorrectValue()
	{
		int probebilityOfCorerctAnswer = 30 + 3 * (completeLevel - 1);

		Random random = new Random();
		int value = random.nextInt(100);
		value ++ ;

		if(value > probebilityOfCorerctAnswer)
			return 0 ;
		else
			return 1;
	}



	/**
	 * This method change the image on each seconds for get ready
	 */
	private void getReadyTimer()
	{
		new CountDownTimer(START_TIME_FOR_GET_READY, 1) 
		{
			@Override
			public void onTick(long millisUntilFinished) 
			{
				int value = (int) ((millisUntilFinished/1000) % 60) ;
				setBacKGroundForGetReadyImage(value);
			}

			@Override
			public void onFinish() 
			{
				if(isClickOnBackPressed)
				{
					layoutReady.setVisibility(View.GONE);
					layoutShow.setVisibility(View.VISIBLE);
					playSound	 = new PlaySound(); 
					playSound.playSoundForBackground(SingleFriendzyEquationActivity.this);

					timer = new MyCountDownTimer(START_TIME, INTERVAL);
					timer.start();

					if(playEquationList.size() != 0)
					{
						showNextEquation();
					}
					else
					{
						getEquationsFromLocalDatabase();
						showNextEquation();
					}
					setSecondPlayerProgress();
					//playsound.playSoundForSingleFriendzyEquationSolve(SingleFriendzyEquationSolve.this);
				}

			}
		}.start();
	}



	/**
	 * use to show single equation at a time
	 */
	private void showNextEquation()
	{
		isClick = false;
		
		setClickckableTrue();
		startTime  = new Date();
		questionObj = new LearningCenterTransferObj();
		questionObj = playEquationList.get(questionNumber);
		equationId	= questionObj.getEquationId();
		operationId	= questionObj.getOperationId();

		setOptionArray(questionObj);
		questionNumber++;
		visibleOptions();
		setWidgetText();	
		setOptionBackground();	
		TriviaQuestion db = new TriviaQuestion(this);
		db.openConn();		
		db.insertIntotriviaQuestionsPlayed(userId, playerId, questionObj.getEquationId());		
		db.closeConn();

	}


	/**
	 * This method set the second player progress
	 */
	private void setSecondPlayerProgress()
	{	
		if(equListForSecondPlayer != null)
		{
			int i = 0;

			for(i = 0; i < equListForSecondPlayer.size(); i++)
			{
				if(equListForSecondPlayer.get(i).getTimeTakenToAnswer() > 0 )
				{				
					break;
				}			
			}

			setsecondPlayerDataProgress(i, equListForSecondPlayer.get(i).getTimeTakenToAnswer());
		}
	}


	/**
	 * This method set the second player points data
	 * @param index
	 * @param timeTakenToAnswer
	 */
	private void setsecondPlayerDataProgress(final int index , double timeTakenToAnswer)
	{	
		if(!isStopSecondPlayerProgress)
		{
			long sleepTime = (long) (timeTakenToAnswer * 1000);

			Handler handlerSetResult = new Handler();
			handlerSetResult.postDelayed(new Runnable() 
			{
				@Override
				public void run() 
				{			
					imgProgressBar = new ImageView(getApplicationContext());

					if(equListForSecondPlayer.get(index).getIsAnswerCorrect() == 0)
					{
						if(isTabSmall)
						{
							imgProgressBar.setBackgroundResource(R.drawable.red_ipad);
						}
						else
						{
							imgProgressBar.setBackgroundResource(R.drawable.red);					
						}
					}
					else
					{
						pointsPlayer2 = pointsPlayer2 + equListForSecondPlayer.get(index).getPoints();

						txtPlayer2Points.setText(strPoint + " : " + pointsPlayer2);
						if(isTabSmall)
						{
							imgProgressBar.setBackgroundResource(R.drawable.green_ipad);
						}
						else
						{
							imgProgressBar.setBackgroundResource(R.drawable.green);					
						}
						//playsound.playSoundForRightForSingleFriendzyForChallenger(SingleFriendzyEquationSolve.this);
					}
					progressBarLayout2.addView(imgProgressBar);	

					equationIndex(index);

				}
			}, sleepTime);
		}

	}


	/**
	 * Get the data from the equation list for setting progress according to i index 
	 * @param index
	 */
	private void equationIndex(int index)
	{
		index_challenger = index + 1;

		if(index >= MAX_QUESTION - 1 && index >= equListForSecondPlayer.size() - 1 )
		{				
			isStopSecondPlayerProgress = true;
			setAnswerForAnswerScreen();

			if(isCompeteLevel)
			{
				Intent intent = new Intent(SingleFriendzyEquationActivity.this,
						CongratulationScreenForSingleFriendzy.class);
				intent.putExtra("points", pointsPlayer1);
				intent.putExtra("isWin", isWin);
				startActivity(intent);
			}
			else
			{
				startActivity(new Intent(SingleFriendzyEquationActivity.this,SingleFriendzyMain.class));
			}
		}
		else if(!isStopSecondPlayerProgress)
		{			
			if(equListForSecondPlayer.get(index_challenger).getTimeTakenToAnswer() > 0 )
			{
				setsecondPlayerDataProgress(index_challenger, equListForSecondPlayer.get(index_challenger).getTimeTakenToAnswer());
			}	
			else 
			{		
				if(isEquationFromServer)
					equationIndex(index_challenger);
			}
		}


	}


	private int getPlayer1ScoreForEachQuestion() 
	{
		end_time = (int) (new Date().getTime()/1000);

		if(start_time == 0)
		{
			return 0;
		}
		else
		{			
			pointsForEachQuestion = MAX_SCORE - (5 * (end_time - start_time));
			pointsPlayer1 = pointsPlayer1 + pointsForEachQuestion;

			if(pointsForEachQuestion < MIN_SCORE)
			{				
				pointsPlayer1 = pointsPlayer1 + MIN_SCORE - pointsForEachQuestion;
				pointsForEachQuestion = MIN_SCORE;
			}

			return pointsPlayer1;	
		}

	}

	/**
	 * sets point on UI
	 */
	private void setPoints()
	{				
		pointsPlayer1 = getPlayer1ScoreForEachQuestion();

		txtPlayer1Points.setText(strPoint+" : "+CommonUtils.setNumberString(pointsPlayer1+""));
		txtPlayer2Points.setText(strPoint+" : "+CommonUtils.setNumberString(pointsPlayer2+""));		
		start_time = (int) (new Date().getTime()/1000);

	}//END setPoints method

	/**
	 * use to setListener for widget
	 */
	private void setWidgetListener()
	{
		ansLayout1.setOnClickListener(this);
		ansLayout2.setOnClickListener(this);
		ansLayout3.setOnClickListener(this);
		ansLayout4.setOnClickListener(this);

	}//END setWidgetListner method

	/**
	 * disable listener when all questions completed.
	 */
	private void visibleOptions() 
	{
		ansLayout1.setVisibility(View.VISIBLE);
		ansLayout2.setVisibility(View.VISIBLE);
		ansLayout3.setVisibility(View.VISIBLE);
		ansLayout4.setVisibility(View.VISIBLE);
	}//END disableListner method


	private void setOptionBackground()
	{
		if(isTabSmall)
		{
			ansLayout1.setBackgroundResource(R.drawable.option1_ipad);
			ansLayout2.setBackgroundResource(R.drawable.option2_ipad);
			ansLayout3.setBackgroundResource(R.drawable.option3_ipad);
			ansLayout4.setBackgroundResource(R.drawable.option4_ipad);
		}
		/*else if(isTabLarge)
		{
			ansLayout1.setBackgroundResource(R.drawable.option1_ipad2);
			ansLayout2.setBackgroundResource(R.drawable.option2_ipad2);
			ansLayout3.setBackgroundResource(R.drawable.option3_ipad2);
			ansLayout4.setBackgroundResource(R.drawable.option4_ipad2);
		}*/
		else
		{
			ansLayout1.setBackgroundResource(R.drawable.option1);
			ansLayout2.setBackgroundResource(R.drawable.option2);
			ansLayout3.setBackgroundResource(R.drawable.option3);
			ansLayout4.setBackgroundResource(R.drawable.option4);
		}	

	}//END setOptionsBackground method


	private void setClickckableTrue()
	{
		ansLayout1.setEnabled(true);
		ansLayout2.setEnabled(true);
		ansLayout3.setEnabled(true);
		ansLayout4.setEnabled(true);
	}

	private void setClickckableFalse()
	{
		ansLayout1.setEnabled(false);
		ansLayout2.setEnabled(false);
		ansLayout3.setEnabled(false);
		ansLayout4.setEnabled(false);
	}


	/**
	 * Use to set options in a string array
	 * @param categories 
	 */
	private void setOptionArray(LearningCenterTransferObj categories) 
	{
		optionArray	= new ArrayList<String>();
		optionArray.add(categories.getAnswer());
		optionArray.add(categories.getOption1());
		optionArray.add(categories.getOption2());
		optionArray.add(categories.getOption3());		
	}//END setOptionArray method


	/**
	 * use to set Text on UI
	 * @param questionObj 
	 */
	private void setWidgetText() 
	{
		Translation translate = new Translation(this);
		translate.openConnection();	
		strPoint = (translate.getTranselationTextByTextIdentifier("lblPts"));

		if(questionObj.getOperationId() == 0)
		{
			mfTitleHomeScreen.setText(MathFriendzyHelper.getAppName(translate));
		}
		else
		{
			mfTitleHomeScreen.setText(translate.getTranselationTextByTextIdentifier(
					CommonUtils.setLearningTitle(questionObj.getOperationId())));	
			txtQueNo.setText(""+questionNumber);
			txtQuestion.setText(questionObj.getQuestion());
			id = new Random().nextInt(optionArray.size());
			txtOption1.setText(optionArray.get(id));
			optionArray.remove(id);

			id = new Random().nextInt(optionArray.size());
			txtOption2.setText(optionArray.get(id));
			optionArray.remove(id);

			id = new Random().nextInt(optionArray.size());
			txtOption3.setText(optionArray.get(id));
			optionArray.remove(id);

			id = new Random().nextInt(optionArray.size());
			txtOption4.setText(optionArray.get(id));
			optionArray.remove(id);
		}

		translate.closeConnection();

	}//END setWidgetId method



	/**
	 * This method set the Get Ready Image back ground
	 * @param value
	 */
	private void setBacKGroundForGetReadyImage(int value)
	{
		if(isTabSmall)
		{
			switch(value)
			{
			case 6 : 
				getReady.setBackgroundResource(R.drawable.mcg_get_ready_ipad);
				break;
			case 5 : 
				getReady.setBackgroundResource(R.drawable.mcg_5_ipad);
				break;
			case 4 : 
				getReady.setBackgroundResource(R.drawable.mcg_4_ipad);
				break;
			case 3 : 
				getReady.setBackgroundResource(R.drawable.mcg_3_ipad);
				break;
			case 2 : 
				getReady.setBackgroundResource(R.drawable.mcg_2_ipad);
				break;
			case 1 : 
				getReady.setBackgroundResource(R.drawable.mcg_1_ipad);
				break;
			case 0 : 
				getReady.setBackgroundResource(R.drawable.mcg_go_ipad);
				break;
			}
		}
		else
		{
			switch(value)
			{
			case 6 : 
				getReady.setBackgroundResource(R.drawable.mcg_get_ready);
				break;
			case 5 : 
				getReady.setBackgroundResource(R.drawable.mcg_5);
				break;
			case 4 : 
				getReady.setBackgroundResource(R.drawable.mcg_4);
				break;
			case 3 : 
				getReady.setBackgroundResource(R.drawable.mcg_3);
				break;
			case 2 : 
				getReady.setBackgroundResource(R.drawable.mcg_2);
				break;
			case 1 : 
				getReady.setBackgroundResource(R.drawable.mcg_1);
				break;
			case 0 : 
				getReady.setBackgroundResource(R.drawable.mcg_go);
				break;
			}
		}

	}

	@SuppressWarnings("deprecation")
	private void setPlayerDetail() 
	{
		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "inside setPlayerDetail()");

		challengerDataObj = ChooseChallengerAdapter.challengerDataObj;

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		userId		= sharedPreffPlayerInfo.getString("userId", "");
		playerId	= sharedPreffPlayerInfo.getString("playerId", "");
		//completeLevel = sharedPreffPlayerInfo.getInt("completeLevel", 0);

		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));

		txtPlayer1Name.setText(playerName+".");

		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo.getString("countryName", ""), this);

		try 
		{
			if(!(coutryIso.equals("-")))
				imgFlagPlayer1.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
						getString(R.string.countryImageFolder) +"/" 
						+ coutryIso + ".png"), null));
		} 
		catch (IOException e) 
		{
			Log.e(TAG, "Inside set player detail Error No Image Found");

		}

		if(challengerDataObj != null)
		{
			//set second user detail		
			txtPlayer2Name.setText(challengerDataObj.getFirst() + " " 
					+ challengerDataObj.getLast().charAt(0)+".");		
			try 
			{
				if(!(coutryIso.equals("-")))
					imgFlagPlayer2.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
							getString(R.string.countryImageFolder) +"/" 
							+ challengerDataObj.getCountryIso() + ".png"), null));
			} 
			catch (IOException e) 
			{
				Country country1 = new Country();
				try 
				{
					imgFlagPlayer2.setBackgroundDrawable(Drawable.createFromStream(getAssets().open(getResources().
							getString(R.string.countryImageFolder) +"/" 
							+ country1.getCountryIsoByCountryId(challengerDataObj.getCountryIso(), this) + ".png"), null));
				}
				catch (NotFoundException e1) 
				{
					Log.e(TAG, "Inside set player detail Error No Image Found");
				} 
				catch (IOException e1) 
				{
					Log.e(TAG, "Inside set player detail Error No Image Found");
				}

			}
		}

		if(SINGLE_FRIENDZY_EQUATION_SOLVE)
			Log.e(TAG, "outside setPlayerDetail()");
	}



	/**
	 * This class find equations from server
	 * @author Yashwant Singh
	 *
	 */
	class FindCompleteEquationsForPlayer extends AsyncTask<Void, Void, ArrayList<EquationFromServerTransferObj>>
	{
		private String userId;
		private String playerId;

		FindCompleteEquationsForPlayer(String userId , String playerId)
		{			
			this.userId 	= userId;
			this.playerId 	= playerId;
		}

		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
		}

		@Override
		protected ArrayList<EquationFromServerTransferObj> doInBackground(Void... params) 
		{		
			ArrayList<EquationFromServerTransferObj> equatiolListFromServer = new ArrayList<EquationFromServerTransferObj>();

			SingleFriendzyServerOperation serverObj = new SingleFriendzyServerOperation();
			equatiolListFromServer = serverObj.getEquations(userId, playerId);

			return equatiolListFromServer;
		}

		@Override
		protected void onPostExecute(ArrayList<EquationFromServerTransferObj> result) 
		{
			//Log.e(TAG, "inside FindCompleteEquationsForPlayer inside do in onPostExecute ");

			if(result != null)
			{
				equListForSecondPlayer = result;

				equationsIdList = new ArrayList<Integer>();

				for( int i = 0 ; i < equListForSecondPlayer.size() ; i ++ )
				{	
					equationsIdList.add(equListForSecondPlayer.get(i).getMathEquationId());
					//Log.e("", "equId "+equListForSecondPlayer.get(i).getMathEquationId());
				}

				TriviaQuestion db = new TriviaQuestion(getApplicationContext());
				db.openConn();
				playEquationList = db.getQuestionListForEquationId(equationsIdList);
				db.closeConn();
			}

			//Log.e(TAG, "outside FindCompleteEquationsForPlayer inside do in onPostExecute ");

			super.onPostExecute(result);
		}
	}//END FindCompleteEquationsForPlayer



	/**
	 * timer class for playing game
	 * @author Shilpi Mangal
	 *
	 */
	private class MyCountDownTimer extends CountDownTimer
	{
		public MyCountDownTimer(long startTime, long interval)
		{
			super(startTime, interval);
		}

		@Override
		public void onFinish() 
		{
			isClickOnBackPressed = false;
			//callOnBackPressed();

			setAnswerForAnswerScreen();

			if(isCompeteLevel)
			{
				Intent intent = new Intent(SingleFriendzyEquationActivity.this,
						CongratulationScreenForSingleFriendzy.class);
				intent.putExtra("points", pointsPlayer1);
				intent.putExtra("isWin", isWin);
				startActivity(intent);
			}
			else
			{
				startActivity(new Intent(SingleFriendzyEquationActivity.this,SingleFriendzyMain.class));
			}
		}

		@Override
		public void onTick(long millisUntilFinished) 
		{
			playingTime	= millisUntilFinished/1000;
			txtTime.setText(DateTimeOperation.setTimeInMinAndSec(playingTime));	
		}
	}//END MyCountDownTimer class

	@Override
	protected void onStop() 
	{
		if(playSound != null)
			playSound.stopPlayer();
		if(timer != null)
			timer.cancel();
		super.onStop();
	}

	@Override
	protected void onPause() 
	{	
		if(playSound != null)
			playSound.stopPlayer();
		timeFlag = true;
		if(timer != null)
			timer.cancel();
		super.onPause();
	}

	@Override
	protected void onRestart() 
	{			
		playSound = new PlaySound();
		playSound.playSoundForBackground(this);
		if(timeFlag)
		{
			START_TIME = playingTime * 1000;
			timer = new MyCountDownTimer(START_TIME, INTERVAL);
			timer.start();			
		}
		super.onRestart();
	}

	@Override
	public void onBackPressed() 
	{
		if(timer != null)
			timer.cancel();

		isClickOnBackPressed = false;
		isStopSecondPlayerProgress = true;//for stop the second player progress
		this.clickOnBackPressed();
		super.onBackPressed();
	}


	/**
	 * This method call when user click on back button
	 */
	private void clickOnBackPressed()
	{
		//numberOfCoins = (int) (pointsPlayer1 * coinsPerPoint);
		savePlayData();

		SharedPreferences sheredPreference = this.getSharedPreferences(LOGIN_SHARED_PREFF, 0);	
		if(!sheredPreference.getBoolean(IS_LOGIN, false))
		{	
			this.inserPlayDataIntodatabase(savePlayDataToDataBase());

			startActivity(new Intent(this,SingleFriendzyMain.class));
		}
		else
		{
			startActivity(new Intent(SingleFriendzyEquationActivity.this,SingleFriendzyMain.class));

			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				if(!isClickOnBackPressed)
				{
					this.insertIntoPlayerTotalPoints(savePlayDataToDataBase());
					new SaveTempPlayerScoreOnServer(savePlayDataToDataBase()).execute(null,null,null);
					new AddCoinAndPointsForLoginUser(getPlayerPoints(savePlayDataToDataBase())).execute(null,null,null);
				}
				else
				{
					this.insertIntoPlayerTotalPoints(savePlayDataToDataBase());
					new SaveTimePointsForFriendzyChallenge(totalTime, pointsPlayer1, SingleFriendzyEquationActivity.this)
					.execute(null, null, null);
				}
			}
			else
			{
				this.inserPlayDataIntodatabase(savePlayDataToDataBase());

				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier
						("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
		}
	}

	/**
	 * This method store data into database for Math_result
	 * @param mathResultObj
	 */
	private void inserPlayDataIntodatabase(MathResultTransferObj mathResultObj )
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		learningCenterObj.insertIntoMathResult(mathResultObj);
		learningCenterObj.closeConn();
		this.insertIntoPlayerTotalPoints(mathResultObj);
	}

	/**
	 * This method insert the data into total player points table
	 * @param mathResultObj
	 */
	private void insertIntoPlayerTotalPoints(MathResultTransferObj mathResultObj)
	{
		LearningCenterimpl learningCenterObj = new LearningCenterimpl(this);
		learningCenterObj.openConn();
		PlayerTotalPointsObj playerPoints = this.getPlayerPoints(mathResultObj);

		if(learningCenterObj.isPlayerTotalPointsExist(playerPoints.getPlayerId()))
		{
			int points = playerPoints.getTotalPoints();
			int coins  = playerPoints.getCoins();
			playerPoints.setTotalPoints(learningCenterObj.getDataFromPlayerTotalPoints(
					playerPoints.getPlayerId()).getTotalPoints()+ points);

			playerPoints.setCoins(learningCenterObj.getDataFromPlayerTotalPoints(playerPoints.getPlayerId()).getCoins()
					+ coins);

			learningCenterObj.deleteFromPlayerTotalPoints(playerPoints.getPlayerId());
		}

		learningCenterObj.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterObj.closeConn();
	}


	/**
	 * This method return the player points with its user id and player id and also coins
	 * @return
	 */
	private PlayerTotalPointsObj getPlayerPoints(MathResultTransferObj mathResultObj)
	{
		PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
		playerPoints.setUserId(mathResultObj.getUserId());
		playerPoints.setPlayerId(mathResultObj.getPlayerId());

		playerPoints.setTotalPoints(pointsPlayer1);

		numberOfCoins = (int) (pointsPlayer1 * 0.05);

		playerPoints.setCoins(numberOfCoins);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		playerPoints.setCompleteLevel(sharedPreffPlayerInfo.getInt("completeLevel", 0));//set complete level

		playerPoints.setPurchaseCoins(0);

		return playerPoints;
	}


	@Override
	public void onClick(View v)
	{
		playSound.playSoundForSelectingAnswer(this);
		setClickckableFalse();
		String layoutText	= null;
		imgProgressBar = new ImageView(this);
		ansLayout1.setVisibility(View.INVISIBLE);
		ansLayout2.setVisibility(View.INVISIBLE);
		ansLayout3.setVisibility(View.INVISIBLE);
		ansLayout4.setVisibility(View.INVISIBLE);
		v.setVisibility(View.VISIBLE);
		switch (v.getId()) 
		{
		case R.id.ansLayout1:
			if(isTabSmall)
			{
				ansLayout1.setBackgroundResource(R.drawable.option1right_ipad);		
			}
			/*else if(isTabLarge)
			{
				ansLayout1.setBackgroundResource(R.drawable.option1right_ipad2);		
			}*/
			else
			{
				ansLayout1.setBackgroundResource(R.drawable.option1right);		
			}			
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption1);
			break;

		case R.id.ansLayout2:
			if(isTabSmall)
			{
				ansLayout2.setBackgroundResource(R.drawable.option2right_ipad);
			}
			/*else if(isTabLarge)
			{
				ansLayout2.setBackgroundResource(R.drawable.option2right_ipad2);
			}*/
			else
			{
				ansLayout2.setBackgroundResource(R.drawable.option2right);
			}	
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption2);
			break;

		case R.id.ansLayout3:
			if(isTabSmall)
			{
				ansLayout3.setBackgroundResource(R.drawable.option3right_ipad);
			}
			/*else if(isTabLarge)
			{
				ansLayout3.setBackgroundResource(R.drawable.option3right_ipad2);
			}*/
			else
			{
				ansLayout3.setBackgroundResource(R.drawable.option3right);
			}	
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption3);
			break;

		case R.id.ansLayout4:
			if(isTabSmall)
			{
				ansLayout4.setBackgroundResource(R.drawable.option4right_ipad);
			}
			/*else if(isTabLarge)
			{
				ansLayout4.setBackgroundResource(R.drawable.option4right_ipad2);
			}*/
			else
			{
				ansLayout4.setBackgroundResource(R.drawable.option4right);
			}
			layoutText = MathFriendzyHelper.returnTextFromTextview(txtOption4);
			break;		
		}

		checkAnswer(layoutText, v);		
	}


	/**
	 * use to check answer and update UI with new Question
	 * @param layoutText refer to selected text for answer
	 */
	private void checkAnswer(String layoutText, final View v) 
	{	
		isClick = true;
		
		userAnswer = layoutText;

		if(layoutText.equals(questionObj.getAnswer()))
		{
			if(isTabSmall)
			{
				imgProgressBar.setBackgroundResource(R.drawable.green_ipad);
			}
			/*else if(isTabLarge)
			{
				imgProgressBar.setBackgroundResource(R.drawable.green_ipad2);
			}*/
			else
			{
				imgProgressBar.setBackgroundResource(R.drawable.green);
			}
			flag_right = true;
			setPoints();
			DISPLAY_TIME = 500;
		}
		else 
		{
			DISPLAY_TIME = 1000;
			flag_right = false;
			if(isTabSmall)
			{
				imgProgressBar.setBackgroundResource(R.drawable.red_ipad);
			}
			/*else if(isTabLarge)
			{
				imgProgressBar.setBackgroundResource(R.drawable.red_ipad2);
			}*/
			else
			{
				imgProgressBar.setBackgroundResource(R.drawable.red);
			}
		}
		progressBarLayout1.addView(imgProgressBar);

		//playSound.playSoundForAnimation(getApplicationContext());
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() 
			{
				if(!flag_right)
				{
					playSound.playSoundForWrong(getApplicationContext());
					if(isTabSmall)
					{
						v.setBackgroundResource(R.drawable.wrong_ipad);
					}
					/*else if(isTabLarge)
					{
						imgProgressBar.setBackgroundResource(R.drawable.red_ipad2);
					}*/
					else
					{
						v.setBackgroundResource(R.drawable.wrong);
					}

					isCorrectAnsByUser = 0;
				}
				else
				{
					playSound.playSoundForRight(getApplicationContext());
					rightAnsCounter ++ ;
					isCorrectAnsByUser = 1;					
				}

				//savePlayData();

				int right_bg_id;
				if(isTabSmall)
				{
					right_bg_id = R.drawable.right_ipad;
				}
				/*else if(isTabLarge)
				{
					imgProgressBar.setBackgroundResource(R.drawable.red_ipad2);
				}*/
				else
				{
					right_bg_id = R.drawable.right;
				}
				checkRightOption(right_bg_id);
			}

		}, DISPLAY_TIME);

		if(questionNumber == MAX_QUESTION)
		{
			playSound.playSoundForResultScreen(this);					
		}		

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() 
			{
				
				if(questionNumber < MAX_QUESTION && questionNumber < playEquationList.size())
				{
					savePlayData();
					showNextEquation();
				}
				else
				{
					setAnswerForAnswerScreen();					
					if(isCompeteLevel)
					{
						Intent intent = new Intent(SingleFriendzyEquationActivity.this,
								CongratulationScreenForSingleFriendzy.class);
						intent.putExtra("points", pointsPlayer1);
						intent.putExtra("isWin", isWin);
						startActivity(intent);
					}
					else
					{
						startActivity(new Intent(SingleFriendzyEquationActivity.this,SingleFriendzyMain.class));
					}
				}
			}

		}, 2000);

	}//END checkAnswer method


	
	/**
	 * This method add the start and end time to solve equation and all the information related to the equation
	 * to the arrayList
	 */		
	@SuppressWarnings("deprecation")
	private void savePlayData()
	{		
		int extraSec = 0;
		if(endTime == null){
			holdingTime = startTime;			
		}else{
			holdingTime = endTime;
		}

		endTime	 = new Date();		

		MathScoreTransferObj mathObj = new MathScoreTransferObj();
		mathObj.setMathOperationID(operationId);
		mathObj.setEquationId(questionObj.getEquationId());

		mathObj.setStartDateTimeStr(CommonUtils.formateDate(startTime));
		mathObj.setEndDateTimeStr(CommonUtils.formateDate(endTime));
		mathObj.setPoints(pointsForEachQuestion);


		/*Change for save time extra after stop clicking on answer*/

		//Log.e("", "holdingTime"+holdingTime);
		//Log.e("", "holdingTime"+endTime);

		if(totalTime < MIN_PLAY_TIME && !isClick 
				&& DateTimeOperation.isValidForChange(holdingTime, endTime)){

			endTime = startTime;
			String time = DateTimeOperation.getEndTimeAfterAddedExtraTime(CommonUtils.formateDate(endTime),
					EXTRA_PLAY_TIME);
			mathObj.setEndDateTimeStr(time);

			//Log.e("time", ""+time);

			extraSec = EXTRA_PLAY_TIME;

		}//End change



		int startTimeValue = startTime.getSeconds() ;
		int endTimeVlaue   = endTime.getSeconds();

		if( endTime.getSeconds() < startTime.getSeconds())
			endTimeVlaue = endTimeVlaue + 60;		

		totalTime = totalTime + (endTimeVlaue - startTimeValue) +extraSec;		
		totalTimeTaken = endTimeVlaue - startTimeValue;
		//Log.e("", "totalTime"+totalTime);

		if(userAnswer.length() > 0)
			mathObj.setAnswer(userAnswer);
		else
			mathObj.setAnswer(" ");

		mathObj.setAnswerCorrect(isCorrectAnsByUser);


		mathObj.setTimeTakenToAnswer(totalTimeTaken);
		mathObj.setEquation(questionObj);

		pointsForEachQuestion = 0;
		playMathlist.add(mathObj);

		userAnswerList.add(userAnswer);
	}
	/*@SuppressWarnings("deprecation")
	private void savePlayData()
	{		 
		endTime = new Date();

		MathScoreTransferObj mathObj = new MathScoreTransferObj();
		mathObj.setMathOperationID(operationId);
		mathObj.setEquationId(equationId);
		mathObj.setStartDateTimeStr(CommonUtils.formateDate(startTime));
		mathObj.setEndDateTimeStr(CommonUtils.formateDate(endTime));
		mathObj.setPoints(pointsForEachQuestion);

		if(userAnswer.length() > 0)
			mathObj.setAnswer(userAnswer);
		else
			mathObj.setAnswer(" ");

		mathObj.setAnswerCorrect(isCorrectAnsByUser);

		int startTimeValue = startTime.getSeconds() ;
		int endTimeVlaue   = endTime.getSeconds();

		if( endTime.getSeconds() < startTime.getSeconds())
			endTimeVlaue = endTimeVlaue + 60;

		totalTime = totalTime + (endTimeVlaue - startTimeValue);
		totalTimeTaken = endTimeVlaue - startTimeValue;		

		mathObj.setTimeTakenToAnswer(totalTimeTaken);
		//mathObj.setTimeTakenToAnswer(endTimeVlaue - startTimeValue);

		mathObj.setEquation(questionObj);

		pointsForEachQuestion = 0;
		playMathlist.add(mathObj);

		userAnswerList.add(userAnswer);
	}*/


	/**
	 * use to check which layout contains the right answer
	 * @param backgroungId
	 */
	private void checkRightOption(int backgroungId)
	{
		if(MathFriendzyHelper.returnTextFromTextview(txtOption1).equals(questionObj.getAnswer()))
		{
			ansLayout1.setVisibility(View.VISIBLE);
			ansLayout1.setBackgroundResource(backgroungId);
		}
		else if(MathFriendzyHelper.returnTextFromTextview(txtOption2).equals(questionObj.getAnswer()))
		{
			ansLayout2.setVisibility(View.VISIBLE);
			ansLayout2.setBackgroundResource(backgroungId);
		}
		else if(MathFriendzyHelper.returnTextFromTextview(txtOption3).equals(questionObj.getAnswer()))
		{
			ansLayout3.setVisibility(View.VISIBLE);
			ansLayout3.setBackgroundResource(backgroungId);
		}
		else if(MathFriendzyHelper.returnTextFromTextview(txtOption4).equals(questionObj.getAnswer()))
		{
			ansLayout4.setVisibility(View.VISIBLE);
			ansLayout4.setBackgroundResource(backgroungId);
		}
	}//END checkRightOption method


	/**
	 * use to setAnswer for Next Screen
	 */
	private void setAnswerForAnswerScreen()
	{		
		playSound.playSoundForResultScreen(getApplicationContext());
		SeeAnswerActivity.isCallFromActivity	= 2;

		savePlayData();

		//playsound.stopPlayer();		
		isStopSecondPlayerProgress = true;//for stop the second player progress	

		//check current level with the database stored level
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String userId = sharedPreffPlayerInfo.getString("userId", "") ; 
		String playerId = sharedPreffPlayerInfo.getString("playerId", "");
		int completeLevelForAnswerAcitiy = completeLevel;
		gameType = "Compete";
		if(completeLevel == sharedPreffPlayerInfo.getInt("completeLevel", 0))
		{
			isCompeteLevel = true;

			SharedPreferences.Editor editor = sharedPreffPlayerInfo.edit();			
			if(pointsPlayer1 > pointsPlayer2)
			{
				isWin = 1 ;
				completeLevel ++ ;
			}
			else
			{
				isWin = 0 ;
				if(completeLevel > 1)
					completeLevel -- ;
			}
			editor.putInt("completeLevel", completeLevel);
			editor.commit();
			insertOrUpdatePlayerEquationTable();
		}
		else
		{
			gameType = "Play";
			completeLevel = sharedPreffPlayerInfo.getInt("completeLevel", 0);
		}

		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);	
		if(sheredPreference.getBoolean(IS_LOGIN, false))
		{
			if(CommonUtils.isInternetConnectionAvailable(SingleFriendzyEquationActivity.this))
			{					
				new AddFriendzyForCompleteLevelForBonus(userId, playerId, completeLevel, starForPlayer, 
						CommonUtils.APP_ID).execute(null,null,null);

				MathResultTransferObj mathObj = savePlayDataToDataBase();
				mathObj.setProblems(mathObj.getProblems() + getEquationUnSolveXml());
				new SaveTempPlayerScoreOnServer(mathObj).execute(null,null,null);
			}
			else
			{
				LearningCenterimpl learningCenterObj = new LearningCenterimpl(SingleFriendzyEquationActivity.this);
				learningCenterObj.openConn();
				gameType = "Play";
				learningCenterObj.insertIntoMathResult(savePlayDataToDataBase());
				learningCenterObj.closeConn();
			}
		}
		else
		{
			LearningCenterimpl learningCenterObj = new LearningCenterimpl(SingleFriendzyEquationActivity.this);
			learningCenterObj.openConn();
			gameType = "Play";
			learningCenterObj.insertIntoMathResult(savePlayDataToDataBase());
			learningCenterObj.closeConn();
		}	

		updateStarAndLevel(userId , playerId);
		updatePlayerTotalPointsTable(playerId , userId);		

		seeAnswerDataObj.setPlayerName(sharedPreffPlayerInfo.getString("playerName", ""));
		Country country = new Country();
		String coutryIso = country.getCountryIsoByCountryName(sharedPreffPlayerInfo
				.getString("countryName", ""), SingleFriendzyEquationActivity.this);

		seeAnswerDataObj.setNoOfCorrectAnswer(rightAnsCounter);
		seeAnswerDataObj.setLevel(completeLevelForAnswerAcitiy);
		seeAnswerDataObj.setPoints(pointsPlayer1);
		seeAnswerDataObj.setCountryISO(coutryIso);
		seeAnswerDataObj.setEquationList(playEquationList);
		seeAnswerDataObj.setUserAnswerList(userAnswerList);

		isStopSecondPlayerProgress = true;

	}

	/**
	 * This method update star and level of the player
	 * @param userId
	 * @param playerId
	 */
	private void updateStarAndLevel(String userId , String playerId)
	{
		//Log.e(TAG, "inside updateStarAndLevel userId , player Id " + userId + " , " + playerId);

		SingleFriendzyImpl singleimpl = new SingleFriendzyImpl(SingleFriendzyEquationActivity.this);
		singleimpl.openConn();
		PlayerEquationLevelObj playrEquationObj = singleimpl.getEquaitonDataFromPlayerEquationTable
				(userId, playerId, EQUAITON_TYPE);
		singleimpl.closeConn();

		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(SingleFriendzyEquationActivity.this);
		learningCenterimpl.openConn();
		PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playrEquationObj.getPlayerId());

		int level = playrEquationObj.getLevel();

		if(level != 1 && level == completeLevel && ((level-1) % 10 == 0))
		{
			if(playrEquationObj.getStars() == 0)
			{
				starForPlayer = 1;

				SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
				singleFriendzy.openConn();

				singleFriendzy.updateEquaitonPlayerData(playrEquationObj.getUserId(), 
						playrEquationObj.getPlayerId(), EQUAITON_TYPE, playrEquationObj.getLevel(), starForPlayer);
				singleFriendzy.closeConn();

				playerPoints.setCoins(playerPoints.getCoins() + bonus);//add 2000 coins

				playerPoints.setPlayerId(playerId);
				playerPoints.setUserId(userId);

				learningCenterimpl.deleteFromPlayerTotalPoints(playrEquationObj.getPlayerId());

				learningCenterimpl.insertIntoPlayerTotalPoints(playerPoints);

				SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
				if(sheredPreference.getBoolean(IS_LOGIN, false))
				{
					if(CommonUtils.isInternetConnectionAvailable(SingleFriendzyEquationActivity.this))
					{
						new AddFriendzyForCompleteLevelForBonus(userId, playerId,
								completeLevel, starForPlayer, CommonUtils.APP_ID).execute(null,null,null);
					}
				}
			}
		}

		learningCenterimpl.closeConn();
	}



	/**
	 * Update player total points table
	 * @param playerId
	 */
	private void updatePlayerTotalPointsTable(String playerId , String userId)
	{
		numberOfCoins = (int) (pointsPlayer1 * coinsPerPoint);

		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(SingleFriendzyEquationActivity.this);
		learningCenterimpl.openConn();
		PlayerTotalPointsObj playerPoints =  learningCenterimpl.getDataFromPlayerTotalPoints(playerId);

		playerPoints.setTotalPoints(playerPoints.getTotalPoints() + pointsPlayer1);
		playerPoints.setCoins(playerPoints.getCoins() + numberOfCoins);
		playerPoints.setCompleteLevel(completeLevel);
		playerPoints.setUserId(userId);
		playerPoints.setPlayerId(playerId);

		learningCenterimpl.deleteFromPlayerTotalPoints(playerId);
		learningCenterimpl.insertIntoPlayerTotalPoints(playerPoints);

		learningCenterimpl.closeConn();

		SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
		if(sheredPreference.getBoolean(IS_LOGIN, false))
		{
			if(CommonUtils.isInternetConnectionAvailable(SingleFriendzyEquationActivity.this))
			{
				new AddCoinAndPointsForLoginUser(playerPoints).execute(null,null,null);
			}
		}
	}


	/**
	 * This method update table is record already exist pther wise insert the new  record
	 */
	private void insertOrUpdatePlayerEquationTable()
	{
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String userId = sharedPreffPlayerInfo.getString("userId", "") ; 
		String playerId = sharedPreffPlayerInfo.getString("playerId", "");

		SingleFriendzyImpl singleFriendzy = new SingleFriendzyImpl(this);
		singleFriendzy.openConn();
		if(singleFriendzy.isEquationPlayerExist( userId , playerId , EQUAITON_TYPE))
		{
			singleFriendzy.updateEquaitonPlayerData(userId, playerId, EQUAITON_TYPE, completeLevel, 0);
		}
		else
		{
			PlayerEquationLevelObj playerEquationObj = new PlayerEquationLevelObj();

			playerEquationObj.setUserId(userId);
			playerEquationObj.setPlayerId(playerId);
			playerEquationObj.setLevel(completeLevel);
			playerEquationObj.setStars(0);
			playerEquationObj.setEquationType(EQUAITON_TYPE);

			singleFriendzy.insertIntoEquationPlayerTable(playerEquationObj);
		}

		singleFriendzy.closeConn();
	}


	/**
	 * This method save play data to the database(Math_Result table)
	 */
	private MathResultTransferObj savePlayDataToDataBase()
	{
		MathResultTransferObj mathResultObj = new MathResultTransferObj();
		mathResultObj.setRoundId(0);
		mathResultObj.setIsFirstRound("0");
		mathResultObj.setGameType(gameType);
		mathResultObj.setIsWin(isWin);

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		if(sharedPreffPlayerInfo.getString("userId", "").equals("0"))
			mathResultObj.setUserId("0");
		else
			mathResultObj.setUserId(sharedPreffPlayerInfo.getString("userId", ""));

		if(sharedPreffPlayerInfo.getString("playerId", "").equals("0"))
			mathResultObj.setPlayerId("0");
		else
			mathResultObj.setPlayerId(sharedPreffPlayerInfo.getString("playerId", ""));

		mathResultObj.setChallengerId(challengerDataObj.getPlayerId());

		mathResultObj.setIsFakePlayer(0);
		mathResultObj.setProblems(this.getEquationSolveXml(playMathlist));

		mathResultObj.setTotalScore(pointsPlayer1);
		//numberOfCoins = (int) (pointsPlayer1 * coinsPerPoint);
		mathResultObj.setCoins(numberOfCoins);

		return mathResultObj;
	}

	/**
	 * This method convert the data for solving the equation into xml format
	 */
	private String getEquationSolveXml(ArrayList<MathScoreTransferObj> playMathlist)
	{		
		StringBuilder xml = new StringBuilder("");

		for( int i = 0 ;  i < playMathlist.size() ; i++)
		{
			xml.append("<equation>" +
					"<equationId>"+playMathlist.get(i).getEquationId()+"</equationId>"+
					"<start_date_time>"+playMathlist.get(i).getStartDateTimeStr()+"</start_date_time>"+
					"<end_date_time>"+playMathlist.get(i).getEndDateTimeStr()+"</end_date_time>"+
					"<time_taken_to_answer>"+playMathlist.get(i).getTimeTakenToAnswer()+"</time_taken_to_answer>"+
					"<math_operation_id>"+playMathlist.get(i).getMathOperationID()+"</math_operation_id>"+
					"<points>"+playMathlist.get(i).getPoints()+"</points>"+
					"<isAnswerCorrect>" + playMathlist.get(i).getAnswerCorrect() + "</isAnswerCorrect>"+
					"<user_answer>"+playMathlist.get(i).getAnswer()+"</user_answer>"+
					"</equation>");
		}

		return xml.toString();
	}

	/**
	 * This methdod make xml for unsove eqaurions
	 * @return
	 */
	private String getEquationUnSolveXml()
	{
		StringBuilder xml = new StringBuilder("");

		for( int i = playMathlist.size() ;  i < equListForSecondPlayer.size() ; i++)
		{
			int pointsForUnsolveEquation = (int) (equListForSecondPlayer.get(i).getTimeTakenToAnswer() 
					+ equListForSecondPlayer.get(i).getPoints());

			xml.append("<equation>" +
					"<equationId>"+equListForSecondPlayer.get(i).getMathEquationId()+"</equationId>"+
					"<start_date_time>"+CommonUtils.formateDate(endTime)+"</start_date_time>"+
					"<end_date_time>"+CommonUtils.formateDate(endTime)+"</end_date_time>"+
					"<time_taken_to_answer>"+equListForSecondPlayer.get(i).getTimeTakenToAnswer()+"</time_taken_to_answer>"+
					"<math_operation_id>"+equListForSecondPlayer.get(i).getOperationId() +"</math_operation_id>"+
					"<points>"+pointsForUnsolveEquation+"</points>"+
					"<isAnswerCorrect>" + 0 + "</isAnswerCorrect>"+
					"<user_answer>"+ "" +"</user_answer>"+
					"</equation>");
		}

		return xml.toString();
	}




	/**
	 * Save Temp Player result on server
	 * @author Yashwant Singh
	 *
	 */
	class SaveTempPlayerScoreOnServer extends AsyncTask<Void, Void, Void>
	{
		private MathResultTransferObj mathobj = null;

		SaveTempPlayerScoreOnServer(MathResultTransferObj mathObj)
		{			
			this.mathobj = mathObj;
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(SingleFriendzyEquationActivity.this);
			if(gameType.equals("Compete"))
			{
				register.savePlayerScoreOnServerForloginuserForSingleFriendzy(mathobj);
			}
			else
			{
				register.savePlayerScoreOnServerForloginuser(mathobj);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			super.onPostExecute(result);
			//new AddCoinAndPointsForLoginUser(getPlayerPoints(mathobj)).execute(null,null,null);
		}
	}

	/**
	 * This asyncTask Add points and coins on server
	 * @author Yashwant Singh
	 *
	 */
	class AddCoinAndPointsForLoginUser extends AsyncTask<Void, Void, Void> 
	{		
		private PlayerTotalPointsObj playerObj = null;

		AddCoinAndPointsForLoginUser(PlayerTotalPointsObj playerObj)
		{
			this.playerObj = playerObj;

			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(SingleFriendzyEquationActivity.this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints(playerObj.getPlayerId());
			learningCenterimpl.closeConn();	

			this.playerObj.setTotalPoints(playerObj.getTotalPoints());
			this.playerObj.setCoins(playerObj.getCoins());
		}

		@Override
		protected void onPreExecute() 
		{			
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Register register = new Register(SingleFriendzyEquationActivity.this);
			register.addPointsAndCoinsOnServerForloginUser(playerObj);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			new SaveTimePointsForFriendzyChallenge(totalTime, pointsPlayer1, SingleFriendzyEquationActivity.this)
			.execute(null, null, null);
			super.onPostExecute(result);
		}
	}


	/**
	 * This asyncktask update the level and stars on server for bonus
	 * @author Yashwant Singh
	 *
	 */
	class AddFriendzyForCompleteLevelForBonus extends AsyncTask<Void, Void, Void>
	{
		private String userId ;
		private String playerId;
		int level;
		int stars;
		String appId;

		AddFriendzyForCompleteLevelForBonus(String userId , String playerId , int level , int stars , String appId)
		{
			this.userId = userId;
			this.playerId = playerId;
			this.level = level;
			this.stars = stars;
			this.appId = appId;
		}

		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			SingleFriendzyServerOperation singleServerObj = new SingleFriendzyServerOperation();
			singleServerObj.addMathComplteLevelForBonus(userId, playerId, level, stars, appId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			super.onPostExecute(result);
		}
	}


}
