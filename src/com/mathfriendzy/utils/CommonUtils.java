package com.mathfriendzy.utils;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ICommonUtils.TEMP_PLAYER_OPERATION_FLAG;
import static com.mathfriendzy.utils.ITextIds.LBL_GEOGRAPHY;
import static com.mathfriendzy.utils.ITextIds.LBL_LANGUAGE;
import static com.mathfriendzy.utils.ITextIds.LBL_MATH;
import static com.mathfriendzy.utils.ITextIds.LBL_MEASUREMENT;
import static com.mathfriendzy.utils.ITextIds.LBL_MONEY;
import static com.mathfriendzy.utils.ITextIds.LBL_PHONICS;
import static com.mathfriendzy.utils.ITextIds.LBL_READING;
import static com.mathfriendzy.utils.ITextIds.LBL_SCIENCE;
import static com.mathfriendzy.utils.ITextIds.LBL_SOCIAL;
import static com.mathfriendzy.utils.ITextIds.LBL_VOCABULARY;
import static com.mathfriendzy.utils.ITextIds.LIKE_URL;
import static com.mathfriendzy.utils.ITextIds.RATE_URL;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.mathfriendzy.controller.main.SpalshScreen;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;

public class CommonUtils 
{
	private static final String TAG = "class CommonUtils";
	private static final int RESPONCE_CODE = 404;
	public static String LANGUAGE_CODE 	= "EN";
	public static String LANGUAGE_ID 	= "1";
	public static String APP_ID 		= "30";

	public static final int TAB_HEIGHT 		= 720;
	public static final int TAB_WIDTH 		= 480;
	public static final int TAB_DENISITY 	= 160;
	public static final int SCREEN_DENSITY	= 240;
	public static final int SCREEN_WIDTH	= 500;
	public static final boolean LOG_ON = false;
	/**
	 * This method check the frequncy date and compare it with current date if
	 * its diff is >= 10 days then ready to get new frequency from server
	 * @return - true for getting new frequency and false for not
	 */
	@SuppressLint("SimpleDateFormat")
	public static boolean isReadyToGetNewFrequesncyFromSerevr(Context context){
		SharedPreferences sharedPreff =
				context.getSharedPreferences(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
		if(sharedPreff.getInt(ICommonUtils.ADS_FREQUENCIES, 0) == 0){
			return true;
		}else{
			SimpleDateFormat sdf 	= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date lastFrequencyDate 	= null;
			try {
				lastFrequencyDate = sdf.parse(sharedPreff
						.getString(ICommonUtils.ADS_FREQUENCIES_DATE, ""));
				DateObj dateObj = CommonUtils.getDateTimeDiff(new Date() , lastFrequencyDate);
				if(dateObj.getDiffInDay() >= 1){
					return true;
				}else{
					return false;
				}
			} catch (ParseException e) {
				Log.e(TAG, "inside isReadyToGetNewFrequesncyFromSerevr " +
						"Error while parsing Date " + e.toString());
				return true;
			}
		}
	}

	/**
	 * This method return the diff in day
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static DateObj getDateTimeDiff(Date playDate , Date currentData)
	{
		DateObj dateObj = new DateObj();

		int diffInDays 		= 0 ;
		int diffInHours     = 0;
		int diffInMinuts    = 0;
		int diffInSeconds	= 0;

		if(playDate.compareTo(currentData) >= 0){
			diffInDays 		= (int)( (playDate.getTime()
					- currentData.getTime()) / (1000 * 60 * 60 * 24) );
			diffInHours 	= (int)( (playDate.getTime()
					- currentData.getTime()) / (1000 * 60 * 60 ) )
					- 24 * diffInDays;
			diffInMinuts 	= (int)( (playDate.getTime() - currentData.getTime()) / (1000  * 60 ) )
					- (diffInHours + 24 * diffInDays) * 60;
			diffInSeconds 	= (int)( (playDate.getTime() - currentData.getTime()) / (1000))
					- (diffInMinuts + (diffInHours + 24 * diffInDays) * 60) * 60;

			dateObj.setDiffInDay(diffInDays);
			dateObj.setDiffInHours(diffInHours);
			dateObj.setDiffInMin(diffInMinuts);
			dateObj.setDiffInSec(diffInSeconds);
		}else{
			dateObj.setDiffInDay(-1);
		}

		return dateObj;
	}

	public static String formateDate(Date date) {
		String displayDate = null;

		displayDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

		return displayDate;
	}



	@SuppressLint("SimpleDateFormat")
	public static boolean isTimeReachedToFrequencyTime(Context ctx) {
		SharedPreferences pref = ctx.getSharedPreferences(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);

		String date = pref.getString(ICommonUtils.ADS_FREQUENCIES_HOUSE_DATE, 
				formateDate(new Date()));
		//        	String now = CommonUtils.formateDate(new Date());
		SimpleDateFormat sdf 	= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date lastFrequencyDate 	= null;
		int time_interval = pref.getInt(ICommonUtils.ADS_timeIntervalFullAd, 0);

		try {
			lastFrequencyDate = sdf.parse(date);
			DateObj dateObj = getDateTimeDiff(new Date(), lastFrequencyDate);
			if(dateObj.getDiffInDay() < 0){
				pref.edit().putString(ICommonUtils.ADS_FREQUENCIES_HOUSE_DATE,
						CommonUtils.formateDate(new Date())).commit();
				//				return true;
			}
			Log.e("", "dateObj.getDiffInSec():"+dateObj.getDiffInSec());
			Log.e("", "time_interval:"+time_interval);
			if(dateObj.getDiffInSec()  >= time_interval){
				return true;
			}else{
				return false;
			}
		}catch (ParseException e) {
			Log.e(TAG, "inside isReadyToGetNewFrequesncyFromSerevr " +
					"Error while parsing Date " + e.toString());
			return true;
		}
	}



	/** This method read the data from url and return it into string form */
	public static String readFromURL(String strURL) 
	{

		StringBuffer finalString = new StringBuffer(); 

		try 
		{
			URL url = new URL(strURL);

			BufferedReader in = new BufferedReader(new
					InputStreamReader(url.openStream()), 8*1024);

			String str = ""; 
			while ((str = in.readLine()) != null) 
			{
				finalString.append(str); 
			} 
			in.close(); 
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Log.e(TAG, "Error while reading from url : " + e.toString()); 
		}

		return finalString.toString();

	}


	/**
	 * This method read the data from url and return it into string form 
	 * @param nameValuePairs
	 * @return
	 */
	public static String readFromURL(ArrayList<NameValuePair> nameValuePairs) 
	{
		InputStream inputStraem		= null;
		StringBuilder buffer 		= null;

		try 
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(COMPLETE_URL);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			inputStraem = entity.getContent();

			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStraem,"iso-8859-1"),8);
			buffer = new StringBuilder();

			String line = null;
			while ((line = reader.readLine()) != null) 
			{
				buffer.append(line);
			}
			inputStraem.close();
		} 
		catch (Exception e) 
		{
			Log.e("CommonUtils", "inside readFromUrl Error while reading from url ");
		}  
		//Log.e("entity", ""+buffer.toString());
		return buffer.toString();
	}


	/**
	 * This method return array object from bitmap object
	 * 
	 * @param bitmap
	 * @return
	 */
	public static byte[] getByteFromBitmap(Bitmap bitmap) {
		final int MAX_BYTE = 100;// for max byte
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, MAX_BYTE, stream);
		byte[] byteMap = stream.toByteArray();

		return byteMap;
	}

	/**
	 * This method return bitmap from byte array
	 * 
	 * @param byteImage
	 * @return
	 *//*
	public static Bitmap getBitmapFromByte(byte[] byteImage) {
		final int MIN_BYTE = 0;// for min byte
		Bitmap bmp = BitmapFactory.decodeByteArray(byteImage, MIN_BYTE,
				byteImage.length);
		return bmp;
	}*/

	/**
	 * Check for url is valid or not
	 * 
	 * @param urlString
	 * @return
	 * @throws IOException
	 */
	public static boolean isValidUrl(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection urlconnection = (HttpURLConnection) url
				.openConnection();

		if (urlconnection.getResponseCode() != RESPONCE_CODE)
			return true;
		else
			return false;
	}

	/**
	 * This method return the bitmap object from image url from server
	 * 
	 * @param urlString
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public static Bitmap getBitmap(String urlString)
			throws MalformedURLException, IOException {
		if (CommonUtils.isValidUrl(urlString)) {
			InputStream inputStream = (InputStream) new URL(urlString)
			.getContent();
			Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
			return bitmap;
		} else
			return null;
	}

	/**
	 * Generate Progress Dialog
	 * 
	 * @param context
	 * @return
	 */
	public static ProgressDialog getProgressDialog(Context context) {
		ProgressDialog pd = new ProgressDialog(context);
		pd.setCanceledOnTouchOutside(false);
		pd.setIndeterminate(false);
		pd.setMessage("Please wait");
		return pd;
	}

	/**
	 * @description Perform Validation for email.
	 * @param email
	 * @return isValid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		// String expression = "(?=.*[a-z])(?=.*\\d)+@(?=.*[a-z])(?=.*\\d)";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	/**
	 * @description Perform Validation for Password.
	 * @param password
	 * @return isValid
	 */
	public static boolean isPasswordValid(String password) {
		boolean isValid = false;

		String PASSWORD_PATTERN = "(?=.*[a-z])(?=.*\\d).{8,}";
		CharSequence inputStr = password;

		Pattern pattern = Pattern.compile(PASSWORD_PATTERN,
				Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	/**
	 * Check for email already exist or not
	 * 
	 * @param email
	 * @return
	 */
	public boolean isEmailAlreadyExist(String email) {
		String strURL = COMPLETE_URL + "action=checkEmailExistence&email="
				+ email;

		if (this.parseEmailResponceJson(CommonUtils.readFromURL(strURL))
				.equals("1"))
			return true;
		else
			return false;
	}

	/**
	 * Parse json for email existence
	 * 
	 * @param jsonString
	 * @return
	 */
	private String parseEmailResponceJson(String jsonString) {
		String result = null;
		try {
			JSONObject jObject = new JSONObject(jsonString);
			result = jObject.getString("response");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (TEMP_PLAYER_OPERATION_FLAG)
			Log.e("TempPlayerOperation", "outside getUserNameFromServer(");

		return result;
	}

	/**
	 * Check for Internet connection
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isInternetConnectionAvailable(Context context) {
		// Log.e("Common", "inside");
		ConnectivityManager connectivityMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connectivityMgr.getActiveNetworkInfo() != null
				&& connectivityMgr.getActiveNetworkInfo().isAvailable()
				&& connectivityMgr.getActiveNetworkInfo().isConnected())
			return true;
		else
			return false;
	}


	/**
	 * use to retrieve Operation title name using operationId
	 * @param operationId refer to operationId of Learning
	 * @return
	 */
	public static String setLearningTitle(int operationId)
	{	
		String title = "";

		switch(operationId)
		{
		case 1:
			return LBL_READING;
		case 2:
			return LBL_MATH;
		case 3:
			return LBL_LANGUAGE;
		case 4:
			return LBL_VOCABULARY;
		case 5:
			return LBL_MONEY;
		case 6:
			return LBL_PHONICS;
		case 7:
			return LBL_MEASUREMENT;
		case 8:
			return LBL_GEOGRAPHY;
		case 9:
			return LBL_SOCIAL;
		case 10:
			return LBL_SCIENCE;
		}

		return title;

	}


	/**
	 * Formate the integer to String
	 * ex - 2000 to 2,000
	 * @param str
	 * @return
	 */
	public static String setNumberString(String str)
	{
		try{
			double amount = Double.parseDouble(str);
			DecimalFormat formatter = new DecimalFormat("###,###,###,###");
			return formatter.format(amount);
		}
		catch(Exception e){
			return str;
		}
	}


	public static String saveBitmap(Bitmap bitmap, String dir, String baseName) 
	{
		try {
			File sdcard = Environment.getExternalStorageDirectory();
			File pictureDir = new File(sdcard, dir);
			pictureDir.mkdirs();
			File f = null;
			for (int i = 1; i < 200; ++i) {
				String name = baseName + i + ".png";
				f = new File(pictureDir, name);
				if (!f.exists()) {
					break;
				}
			}
			if (!f.exists()) 
			{				
				String name = f.getAbsolutePath();
				FileOutputStream fos = new FileOutputStream(name);
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
				fos.flush();
				fos.close();

				return name;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

		return null;
	}

	public static void showInternetDialog(Context context)
	{

		DialogGenerator dg = new DialogGenerator(context);
		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		dg.generateWarningDialog(transeletion
				.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
		transeletion.closeConnection();

	}

	/**
	 * This method formate the date
	 * @param date
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public static String formateDateIn24Hours(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
		//return SimpleDateFormat.getTimeInstance(SimpleDateFormat.MEDIUM, Locale.UK).format(date);
		return df.format(date);
	}


	/*public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight, Context context) 
	{
		final float densityMultiplier = context.getResources().getDisplayMetrics().density;        

		int h= (int) (200*densityMultiplier);
		int w= (int) (h * photo.getWidth()/((double) photo.getHeight()));

		photo=Bitmap.createScaledBitmap(photo, w, h, true);

		return photo;
	}*/


	public static void setSharedPreferences(Context context)
	{
		SharedPreferences sheredPreference = context.getSharedPreferences("SHARED_FLAG", 0);	
		SharedPreferences.Editor editor = sheredPreference.edit();
		editor.putBoolean("isGetLanguageFromServer", false);	
		editor.putBoolean("isTranselation", false);
		editor.putString("oldversion", SpalshScreen.newversion);
		editor.commit();
	}


	/*public static String getMessageForShareMail() 
	{	
		String text = null;	

		text =  "I just downloaded this awesome app and it is really helping me with my school work." +
				" It is so fun and exciting!" +
				" Now we can learn all our school subjects by competing with each other. Think you can beat me? "+
				" <a href=\""+ITextIds.RATE_URL+"\">"+"Click Here"+"</a>"+" "
				+" to visit the Google Play Store and download Reading Friendzy. " +
				"We can play against each other and see who wins."					
				+"<p><p><a href=\""+LIKE_URL+"\">" +"Like</a>"
				+ " us on the official Friendzy Apps Facebook fan page";

		return text;
	}*/

	public static String getMessageForShareMail() 
	{	
		String text = null;	

		text =  "I just downloaded this awesome app and it is really helping me with my school work." +
				" It is so fun and exciting!" +
				" Now we can learn all our school subjects by competing with each other. Think you can beat me? "+
				" <a href=\""+RATE_URL+"\">"+"Click Here"+"</a>"+" "
				+" to visit the Google Play Store and download "+ITextIds.LBL+" Friendzy. " +
				"We can play against each other and see who wins."					
				+"<p><p><a href=\""+LIKE_URL+"\">" +"Like</a>"
				+ " us on the official Friendzy Apps Facebook fan page";

		return text;
	}

	/**
	 * use to make message for inviting friends
	 * @return message
	 */
	public static String getFacebookShareMsg(Context context)
	{
		SharedPreferences sharedPreffPlayerInfo = context.getSharedPreferences(PLAYER_INFO, 0);
		String playerUserName = sharedPreffPlayerInfo.getString("userName", "");
		Translation translate = new Translation(context);
		translate.openConnection();
		String text = null;		

		text =  "Dear Friend: I just downloaded this awesome app and it is really helping me with my school work." +
				" It is so fun and exciting!" +
				" Now we can learn all our school subjects by competing with each other. Think you can beat me? "
				+ RATE_URL+" to visit the Google Play Store and download "+ITextIds.LBL+" Friendzy. " 
				+"Once you get it, click on \"Multi-Friendzy\" and \"Start New Friendzy\". Then search '"
				+playerUserName+"'. Then click on Go to start a Friendzy. Once you are done, I will get the same questions " +
				"as you and we'll see who wins.";

		translate.closeConnection();
		return text;
	}


	public static void publishFeedDialog(final String user, final Context context, boolean isFriend) 
	{		
		Bundle params = new Bundle();		
		params.putString("description",  getFacebookShareMsg(context));
		params.putString("picture", ITextIds.URL_ICON_IMG);
		params.putString("link", RATE_URL);
		if(isFriend)
			params.putString("to", user);		

		WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(context,
				Session.getActiveSession(),params))
				.setOnCompleteListener(new OnCompleteListener() {

					@Override
					public void onComplete(Bundle values,FacebookException error)
					{
						if (error == null) {
							// When the story is posted, echo the success
							// and the post Id.
							final String postId = values.getString("post_id");
							if (postId != null) 
							{								
								DialogGenerator generator = new DialogGenerator(context);
								Translation translate = new Translation(context);
								translate.openConnection();
								generator.generateWarningDialog(translate
										.getTranselationTextByTextIdentifier("alertMsgYourWallPostCompleted"));
								translate.closeConnection();

							} else {
								// User clicked the Cancel button
								Toast.makeText(context.getApplicationContext(), 
										"Publish cancelled", 
										Toast.LENGTH_SHORT).show();
							}
						} else if (error instanceof FacebookOperationCanceledException) {
							// User clicked the "x" button
							Toast.makeText(context.getApplicationContext(), 
									"Publish cancelled", 
									Toast.LENGTH_SHORT).show();
						} else {
							// Generic, ex: network error
							Toast.makeText(context.getApplicationContext(), 
									"Error posting story", 
									Toast.LENGTH_SHORT).show();
						}
					}

				})
				.build();
		feedDialog.show();			
	}

	/**
	 * This method return bitmap from byte array
	 * 
	 * @param byteImage
	 * @return
	 */
	public static Bitmap getBitmapFromByte(byte[] byteImage , Context context)
	{
		float cornerSize = 10.0f;
		if(MainActivity.isTab)
		{
			cornerSize = 14.0f;
		}

		final int MIN_BYTE = 0;// for min byte
		Bitmap bmp = BitmapFactory.decodeByteArray(byteImage, MIN_BYTE,
				byteImage.length);

		Bitmap output = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(),
				Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int borderSizePx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) 0.0,
				context.getResources().getDisplayMetrics());
		final int cornerSizePx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, cornerSize,
				context.getResources().getDisplayMetrics());
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bmp.getWidth(), bmp.getHeight());
		final RectF rectF = new RectF(rect);

		// prepare canvas for transfer
		paint.setAntiAlias(true);
		paint.setColor(0xFFFFFFFF);
		paint.setStyle(Paint.Style.FILL);
		canvas.drawARGB(0, 0, 0, 0);
		canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

		// draw bitmap
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bmp, rect, rect, paint);

		// draw border
		paint.setColor(Color.WHITE);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth((float) borderSizePx);
		canvas.drawRoundRect(rectF, cornerSizePx, cornerSizePx, paint);

		return output;
		//return bmp;
	}


	/**
	 * This method show the dialog after registeration added for new changes
	 */
	public static void showDialogAfterRegistrationSuccess(Context context){
		Translation transeletion = new Translation(context);
		transeletion.openConnection();
		DialogGenerator dg = new DialogGenerator(context);
		dg.generateDailogAfterRegistration(transeletion.
				getTranselationTextByTextIdentifier("lblTheLastStepOfRegistrationIsToConfirm"));
		transeletion.closeConnection();
	}

	/**
	 * This method check the application status
	 * @param context - current activity object
	 * @param userId  - userId
	 * @param itemId  - itemId
	 * @return - 1 for unlock status and 0 for unlock status
	 */
	public static int getApplicationStatus(Context context , String userId , int itemId){
		int appStatus = 0;
		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(context);
		learnignCenterImpl.openConn();
		appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
		learnignCenterImpl.closeConn();
		return appStatus;
	}

	public static void saveSharedPrefForIsAdDisable(Context context, int value){
		try{
			//save isDisable data for displaying ads
			//regUserObj.setIsAdsDisable(jObject.getInt("isAdsDisable"));
			SharedPreferences sharedPreff = context.getSharedPreferences
					(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
			SharedPreferences.Editor editor = sharedPreff.edit();	
			editor.putInt(ICommonUtils.ADS_IS_ADS_DISABLE, value);
			editor.commit();

		}catch(Exception e){
			Log.e("Login", "inside parseLoginEmailJson :" +
					" Error while getting isDisable " + e.toString());
		}
	}
	public static boolean isUserLogin(Context myContext) {
		return myContext.getSharedPreferences(LOGIN_SHARED_PREFF, 0)
				.getBoolean(IS_LOGIN, false);
	}

	public static void printLog(String TAG , String message){
		if(LOG_ON)
			Log.e(TAG, message);
	}
}
