package com.mathfriendzy.listener;

/**
 * Created by root on 24/6/15.
 */
public interface EditTextFocusChangeListener {
    void onFocusLost(boolean isValidString);
    void onFocusHas(boolean isValidString);
}
